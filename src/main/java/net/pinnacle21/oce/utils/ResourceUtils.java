package net.pinnacle21.oce.utils;

import com.google.common.base.Joiner;
import com.google.common.collect.FluentIterable;
import com.google.common.io.Files;
import net.bootstrap.config.ConfigUtils;
import net.bootstrap.core.callback.EngineCallback;
import net.bootstrap.core.integrity.ETagFile;
import net.bootstrap.core.utils.BootstrapFileUtils;
import net.bootstrap.utils.Libs;
import net.bootstrap.utils.S3Loader;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.CLIExceptionCode;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.model.StandardType;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datapackage.MetadataMapper;
import net.pinnacle21.oce.model.datasource.AbstractFileDS;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.datasource.FolderDS;
import net.pinnacle21.oce.model.input.Input;
import net.pinnacle21.oce.services.EngineCallbackBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static net.bootstrap.core.utils.aws.AwsUtils.getS3Path;
import static net.bootstrap.core.utils.hash.HashUtils.checkFileIntegrity;
import static net.pinnacle21.oce.utils.CliPreconditions.checkCollection;
import static net.pinnacle21.oce.utils.CliPreconditions.checkProvided;
import static net.pinnacle21.oce.utils.CliPreconditions.checkState;
import static net.pinnacle21.oce.utils.CliPreconditions.exists;
import static net.pinnacle21.oce.utils.DataSources.isDirectory;
import static net.pinnacle21.oce.utils.DataSources.makeDataSource;
import static net.pinnacle21.oce.utils.DataSources.toFile;

public final class ResourceUtils {
    private static final String DATA_FOLDER = "data";
    static final File TEMP_DIR;
    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceUtils.class);

    static {
        TEMP_DIR = Files.createTempDir();
        // register for deletion
        new FolderDS(TEMP_DIR, true);
    }

    private ResourceUtils() {
    }

    public static DataSource getConfigFile(DataSource rootFolder,
                                           StandardType.Standard standard,
                                           DataPackage dataPackage) {
        checkProvided(rootFolder,
            "Failed to determine config file name. Root folder is null");
        checkProvided(standard,
            "Failed to determine config file name. Standard is null");

        if (!DataSources.isDirectory(rootFolder)) {
            return configCheckAndDownload(rootFolder, standard.getType(), dataPackage);
        }

        // config is a folder need to get file name from data package
        LOGGER.info("Looking for config file in folder={}", rootFolder.getAbsolutePath());
        DataSource standardFile = DataSources.newFileOrFolder(rootFolder, standard.toName());

        LOGGER.info("Checking config file={}", standardFile.getAbsolutePath());

        return configCheckAndDownload(standardFile, standard.getType(), dataPackage);
    }

    private static DataSource configCheckAndDownload(DataSource ds,
                                                     StandardType standardType,
                                                     DataPackage dataPackage) {
        if (dataPackage == null || !dataPackage.getDownloadConfig().get()) {
            return ds;
        }

        File configFile = toFile(ds);
        if (configFile == null) {
            return ds;
        }
        File directory = configFile.getParentFile();
        if (directory == null || StringUtils.equalsIgnoreCase(directory.getName(), "custom")) {
            return ds;
        }

        try {
            downloadConfig(dataPackage, configFile);
            if (standardType == StandardType.DEFINE) {
                File parentFolder = configFile.getParentFile();
                File defineStandardConfig = new File(parentFolder, dataPackage.getStandard().get().toFileName());
                downloadConfig(dataPackage, defineStandardConfig);
            }
        } catch (CLIException cliEx) {
            throw cliEx;
        } catch (Exception e) {
            LOGGER.error("Failed to create config {}. Cannot download",
                ds.getAbsolutePath(), e);
        }
        return ds;
    }

    private static void downloadConfig(DataPackage dataPackage, File configFile) throws IOException {
        BootstrapFileUtils.forceMkdir(configFile);
        LOGGER.info("Downloading config {} from s3 for engine {}",
            configFile.getAbsolutePath(),
            dataPackage.getEngineVersion());
        
        EngineCallback callBack = EngineCallback
            .forEngine(EngineCallbackBuilder.builder().setCliModel(dataPackage));

        ConfigUtils.downloadConfig(configFile, callBack).await();

        if (!configFile.exists()) {
            throw new CLIException(
                SystemExceptions.DOWNLOAD_FAILED,
                String.format("Failed to download %s", configFile.getAbsolutePath())
            );
        }
    }

    public static MetadataResource getMetadataFolder(DataSource rootFolder,
                                                     MetadataMapper mapper,
                                                     DataPackage dataPackage) {
        return getMetadata(rootFolder, mapper, dataPackage);
    }

    public static MetadataResource getMetadataFolder(DataSource rootFolder,
                                                     MetadataMapper mapper,
                                                     Input<String> version,
                                                     EngineCallback callback,
                                                     boolean downloadConfig) {
        checkProvided(rootFolder,
            "Failed to determine terminology file name. Root folder is null");
        checkProvided(mapper,
            "Failed to determine file name. Metadata is null");

        String folderName = mapper.toFolderName();
        String fileName = mapper.toFileName();
        File rootFile = toFile(rootFolder);
        String configVersion = BootstrapFileUtils.getVersion(rootFile);
        if (Libs.getConfigVersion(callback.getEngineVersion()).getVersion().equals(configVersion) ||
            callback.getEngineVersion().equals(configVersion)) {
            rootFile = rootFile.getParentFile();
        }

        AbstractFileDS cDiscFolder = DataSources.newFileOrFolder(rootFile, DATA_FOLDER,
            folderName, fileName);

        DataSource terminologyDS;
        if (!version.isProvided()) {
            LOGGER.debug("Version is not provided for '{}'. Loading latest version.",
                mapper.getValue());
            File foundFile = getLatestVersion(cDiscFolder, mapper, downloadConfig);
            version = Input.asCalculated(foundFile.getName());
            terminologyDS = DataSources.newFileOrFolder(foundFile);
        } else {
            terminologyDS = DataSources.newFileOrFolder(cDiscFolder, version.get());
        }

        return new MetadataResource(version, terminologyDS, toPath(mapper), mapper);
    }

    private static MetadataResource getMetadata(DataSource rootFolder,
                                                MetadataMapper mapper,
                                                DataPackage dataPackage) {
        checkProvided(dataPackage,
            "Failed to determine terminology file name. Data package is null");

        MetadataResource resource = getMetadataFolder(rootFolder, mapper, mapper.getVersion(dataPackage),
            EngineCallback.forEngine(EngineCallbackBuilder
                .builder()
                .setCliModel(dataPackage)), dataPackage.getDownloadConfig().get());

        return validateMetadata(resource, SystemExceptions.EXISTS_CT_TERMINOLOGY,
            String.format("Cannot find metadata version=%s in folder=%s",
                resource.version.get(), resource.path), dataPackage);
    }

    private static File getLatestVersion(final DataSource folder,
                                         final MetadataMapper mapper,
                                         boolean downloadConfig) {
        checkState(isDirectory(folder), SystemExceptions.EXISTS_CT_TERMINOLOGY,
            String.format("Cannot find the latest version of the metadata. " +
                "Provided data source is not a folder. source=%s", folder.getAbsolutePath()));

        File[] versionFolders = Optional.ofNullable(toFile(folder).listFiles()).orElse(new File[0]);
        if (downloadConfig) {
            String prefix = toPath(mapper);
            LOGGER.info("Geting version list from s3 for {} ", prefix);
            List<String> versions = ConfigUtils.getDictionaryVersions(prefix);
            versionFolders = FluentIterable.from(versions)
                .transform(input -> new File(toFile(folder), input))
                .append(versionFolders)
                .toArray(File.class);
        }

        checkState(versionFolders.length > 0, SystemExceptions.EXISTS_CT_TERMINOLOGY,
            String.format("Cannot find control metadata in folder=%s", folder.getAbsolutePath()));

        // sort folder collection in desc order so the most recent terminology version comes first
        List<File> versions = Arrays.stream(versionFolders)
            .filter(DataSources::isDirectory)
            .sorted((v1, v2) -> {
                if (mapper.isNumeric()) {
                    return (int) (
                        (Double.valueOf(v2.getName()) - Double.valueOf(v1.getName())) * 1000
                    );
                }

                return v2.getName().compareTo(v1.getName());
            })
            .collect(Collectors.toList());

        checkCollection(versions, SystemExceptions.EXISTS_CT_TERMINOLOGY,
            String.format("Control metadata folder=%s does not have any version sub-folders",
                folder.getAbsolutePath()));

        LOGGER.info("Found latest version '{}", versions.get(0).getAbsolutePath());
        return versions.get(0);
    }

    private static MetadataResource validateMetadata(MetadataResource resource,
                                                     CLIExceptionCode errorCode,
                                                     String msg,
                                                     DataPackage dataPackage) {
        File file = toFile(resource.dataSource);
        checkProvided(file, errorCode, msg);

        if (resource.mapper.canDownload() &&
            dataPackage.getDownloadConfig().get()) {
            try {
                LOGGER.info("Trying to download dictionary {}", file.getAbsolutePath());
                ConfigUtils.downloadDictionary(file, getS3Path(resource.path, resource.version.get())).await();
                if (resource.version.isProvided()) {
                    // version was requested but not downloaded. report as error
                    exists(file, SystemExceptions.DOWNLOAD_FAILED, msg);
                }
            } catch (CLIException cliEx) {
                // re-throw CLI error
                throw cliEx;
            } catch (Exception e) {
                throw new CLIException(errorCode, msg, e);
            }

        } else {
            exists(file, errorCode, msg);
        }

        LOGGER.debug("Folder or file '{}' exists", file.getAbsolutePath());

        return resource;
    }

    private static String toPath(MetadataMapper mapper) {
        return Joiner.on('/').join(mapper.toFolderName(), mapper.toFileName());
    }

    public static void cleanup(boolean clean, List<DataSource> folders) {
        if (clean) {
            // cleanup all folders
            for (DataSource folder : folders) {
                try {
                    if (DataSources.exists(folder)) {
                        FileUtils.forceDelete(toFile(folder));
                        makeDataSource(folder);
                    }
                } catch (Exception e) {
                    LOGGER.error("Failed to clean folder {}", folder, e);
                }
            }
        }
    }

    public static final class MetadataResource {
        public final Input<String> version;
        public final DataSource dataSource;
        public final String path;
        final MetadataMapper mapper;

        public MetadataResource() {
            this.version = Input.emptyString();
            this.dataSource = null;
            this.path = null;
            this.mapper = null;
        }

        private MetadataResource(Input<String> version,
                                 DataSource dataSource,
                                 String path,
                                 MetadataMapper mapper) {
            this.version = version;
            this.dataSource = dataSource;
            this.path = path;
            this.mapper = mapper;
        }
    }
}
