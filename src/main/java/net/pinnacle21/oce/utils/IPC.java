package net.pinnacle21.oce.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.google.common.collect.ImmutableSet;
import net.pinnacle21.oce.Main;
import net.pinnacle21.oce.events.StateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

// IPC is done through STDOUT and STDERR so this
// class is really just to make it not seem odd
// that there's System output calls all over the place
public class IPC {
    private static final PrintStream IPC_STREAM = getIpcPrintStream();
    private static final Logger LOGGER = LoggerFactory.getLogger(IPC.class);
    final private static ImmutableSet<String> DATA_PACKAGE_FIELDS_TO_FILTER =
            ImmutableSet.of("reportSource", "output", "source", "engineFolder", "define", "standardFilter", "standard");
    final private static ImmutableSet<String> VALIDATOR_METRICS_FIELDS_TO_FILTER =
            ImmutableSet.of("options", "callback", "propertiesMap");
    final private static ImmutableSet<String> CONFIG_INFO_FIELDS_TO_FILTER =
            ImmutableSet.of("config");
    final private static ImmutableSet<String> DATA_SOURCE_INFO_FIELDS_TO_FILTER =
            ImmutableSet.of("dataSource", "remoteDataSource");
    final private static ImmutableSet<String> PARSE_RESULTS_FIELDS_TO_FILTER =
            ImmutableSet.of("config", "cliParameters");
    final private static ImmutableSet<String> CONVERTER_INFO_FIELDS_TO_FILTER =
            ImmutableSet.of("sources", "define", "config", "output");
    final private static ImmutableSet<String> DEFINE_CONVERSION_INFO_FIELDS_TO_FILTER =
            ImmutableSet.of("excelFile", "xmlFile");
    final private static ObjectWriter objWriter = new ObjectMapper()
            .setFilterProvider(
                    new SimpleFilterProvider()
                            .addFilter("dataPackageFilter",
                                    SimpleBeanPropertyFilter.serializeAllExcept(DATA_PACKAGE_FIELDS_TO_FILTER))
                            .addFilter("validationMetricsFilter",
                                    SimpleBeanPropertyFilter.serializeAllExcept(VALIDATOR_METRICS_FIELDS_TO_FILTER))
                            .addFilter("configInfoFilter",
                                    SimpleBeanPropertyFilter.serializeAllExcept(CONFIG_INFO_FIELDS_TO_FILTER))
                            .addFilter("dataSourceInfoFilter",
                                    SimpleBeanPropertyFilter.serializeAllExcept(DATA_SOURCE_INFO_FIELDS_TO_FILTER))
                            .addFilter("parseResultsFilter",
                                    SimpleBeanPropertyFilter.serializeAllExcept(PARSE_RESULTS_FIELDS_TO_FILTER))
                            .addFilter("converterInfoFilter",
                                    SimpleBeanPropertyFilter.serializeAllExcept(CONVERTER_INFO_FIELDS_TO_FILTER))
                            .addFilter("defineConversionInfoFilter",
                                    SimpleBeanPropertyFilter.serializeAllExcept(DEFINE_CONVERSION_INFO_FIELDS_TO_FILTER))
            )
            .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
            .configure(SerializationFeature.CLOSE_CLOSEABLE, true)
            .writer();

    public static void initialize() {
        // This method clears STDOUT so extra library chatter doesn't confuse the UI IPC
        // Because IPC_STREAM is static, it will be initialized before System.out is cleared.
        System.setOut(new PrintStream(new OutputStream() {
            public void write(int b) {
                //DO NOTHING
                // This is necessary because
                // net.pinnacle21.validator.settings.XmlConfigurationParser.java
                // uses System.out as it's own personal logger.
                // In it's defense, it has TODOs to fix that.
            }
        }));
    }

    private static synchronized PrintStream getIpcPrintStream() {
        try {
            return new PrintStream(System.out, true, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException ex) {
            send(ex);
            return System.out;
        }
    }

    public static void send(String message) {
        IPC_STREAM.println(message);
    }

    public static void send(Throwable err) {
        System.err.println(err.getMessage());
    }

    public static void send(StateEvent event) {
        try {
            send(objWriter.writeValueAsString(event));
        } catch (JsonProcessingException e) {
            send(e);
        }
    }
}

