package net.pinnacle21.oce.utils;

import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.datasource.DataSource;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


public final class ZipUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ZipUtils.class);

    private ZipUtils() {
    }

    static File zipDataSources(List<DataSource> sources, File tempDir) {
        File zipFile = new File(tempDir, "data.zip");

        LOGGER.info("Zipping data for upload into a file '{}'", zipFile.getAbsolutePath());

        try (OutputStream zipData = new FileOutputStream(zipFile);
             ZipOutputStream zipStream = new ZipOutputStream(zipData, Charset.forName("UTF-8"))) {

            int fileCnt = 0;
            for (DataSource ds : sources) {
                if (!DataSources.COMBINABLE.contains(ds.getSchema())) {
                    // combine data sources called for non-file source. This is not supported
                    throw new CLIException(ValidationExceptions.MISSING_DATA_SOURCE,
                        String.format("Cannot zip non-file data sources=%s",
                            sources));
                }
                if (DataSources.isDirectory(ds)) {
                    fileCnt += zipFolder(DataSources.toFile(ds), zipStream);
                } else {
                    if (zipFile(DataSources.toFile(ds), zipStream)) {
                        ++fileCnt;
                    }
                }
            }

            zipStream.finish();
            zipData.flush();

            if (fileCnt == 0) {
                throw new CLIException(SystemExceptions.FAILED_COPY_SOURCE,
                    String.format("Failed to zip data sources=%s to zipFile=%s. No files found",
                        sources, zipFile.getAbsolutePath()));
            }

            LOGGER.info("Zipping completed. Total number of files {}", fileCnt);

            return zipFile;
        } catch (IOException e) {
            throw new CLIException(SystemExceptions.FAILED_COPY_SOURCE,
                String.format("Failed to zip data sources=%s to zipFile=%s",
                    sources, zipFile.getAbsolutePath()), e);
        }
    }

    private static int zipFolder(File folder, ZipOutputStream zipStream) throws IOException {
        File[] files = folder.listFiles();
        if (files == null) {
            LOGGER.warn("Nothing to zip in the folder '{}'",
                folder.getAbsolutePath());

            return 0;
        }

        int cnt = 0;
        for (File file : files) {
            if (DataSources.isDirectory(file)) {
                cnt += zipFolder(file, zipStream);
            } else {
                if (zipFile(file, zipStream)) {
                    ++cnt;
                }
            }
        }

        return cnt;
    }

    private static boolean zipFile(File file, ZipOutputStream zipStream) throws IOException {
        if (!file.exists()) {
            return false;
        }

        try (FileInputStream fin = new FileInputStream(file)) {
            zipStream.putNextEntry(new ZipEntry(file.getName()));
            IOUtils.copy(fin, zipStream);
            zipStream.closeEntry();
            return true;
        }
    }
}
