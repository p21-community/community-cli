package net.pinnacle21.oce.utils;

import com.google.common.base.Strings;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.CLIExceptionCode;
import net.pinnacle21.oce.exceptions.DataExceptions;
import net.pinnacle21.oce.model.Valuable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Valuables {
    private static final Logger LOGGER = LoggerFactory.getLogger(Valuables.class);

    private Valuables() {
    }

    public static <T extends Valuable> T fromValue(T[] values,
                                                   String value) {
        return fromValue(values, value, DataExceptions.UNKNOWN_VALUE, null);
    }

    public static <T extends Valuable> T fromValue(T[] values,
                                                   String value,
                                                   CLIExceptionCode errCode) {
        return fromValue(values, value, errCode, null);
    }

    public static <T extends Valuable> T fromValue(T[] values,
                                                   String value,
                                                   T defaultValue) {
        return fromValue(values, value, null, defaultValue);
    }

    public static <T extends Valuable> T fromValue(T[] values,
                                                   String value,
                                                   CLIExceptionCode errCode,
                                                   T defaultValue) {
        String valuableName = "Item";
        String defaultName = defaultValue == null ? "unknown" : defaultValue.getValue();

        if (values != null) {
            for (T item : values) {
                if (item.isValue(Strings.nullToEmpty(value).trim())) {
                    return item;
                }
            }
            valuableName = values[0].getClass().getSimpleName();
        }


        if (errCode != null) {
            throw new CLIException(errCode, String.format("%s value='%s' is not found",
                valuableName, value));
        }

        LOGGER.warn("{} cannot be determined for value '{}'. Return '{}'",
            valuableName, value, defaultName);

        return defaultValue;
    }
}
