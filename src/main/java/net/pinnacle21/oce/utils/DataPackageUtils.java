package net.pinnacle21.oce.utils;

import net.pinnacle21.oce.model.datapackage.DataPackage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provide utilities for mapping data to DataPackage object
 */
public final class DataPackageUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataPackageUtils.class);

    private DataPackageUtils() {
    }

    /**
     * null safe source path
     *
     * @param dataPackage - data package to get source path
     * @return absolute source path
     */
    public static String getSourcePath(DataPackage dataPackage) {
        return dataPackage != null && dataPackage.getSource() != null
            ? dataPackage.getSource().getAbsolutePath()
            : "empty";
    }
}
