package net.pinnacle21.oce.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public final class EnterpriseUtils {
    private static final String TLSv1_2 = "TLSv1.2";
    private static final Logger LOGGER = LoggerFactory.getLogger(EnterpriseUtils.class);

    private EnterpriseUtils() {
    }

    public static boolean needToInstallTLS() {
        try {
            SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
            SSLSocket soc = (SSLSocket) factory.createSocket();
            String[] protocols = soc.getEnabledProtocols();
            boolean needToInstall = true;

            LOGGER.info("Enabled protocols:");
            for (String s : protocols) {
                LOGGER.info(s);
                if (TLSv1_2.equalsIgnoreCase(s)) {
                    needToInstall = false;
                }
            }

            return needToInstall;
        } catch (Exception e) {
            LOGGER.error("Failed to determine TLS protoclos. Enterprise systme may not work properly");
        }

        return true;
    }
}
