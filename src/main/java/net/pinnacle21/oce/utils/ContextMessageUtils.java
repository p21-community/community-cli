package net.pinnacle21.oce.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import net.bootstrap.core.model.ContextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

public final class ContextMessageUtils {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static Logger LOGGER = LoggerFactory.getLogger(ContextMessageUtils.class);

    private ContextMessageUtils() {
    }

    static ContextMessage toContextMessage(String message) {
        try {
            return OBJECT_MAPPER.readValue(message, ContextMessageProxy.class).message;
        } catch (IOException e) {
            LOGGER.error("Failed to read message '{}'", message, e);
            return null;
        }
    }

    @JsonDeserialize(using = ContextMessageDeseriualizer.class)
    private static class ContextMessageProxy {
        ContextMessage message;

        ContextMessageProxy(ContextMessage message) {
            this.message = message;
        }
    }

    private static class ContextMessageDeseriualizer extends JsonDeserializer<ContextMessageProxy> {
        @Override
        @SuppressWarnings("unchecked")
        public ContextMessageProxy deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            Map object = p.readValueAs(Map.class);

            return new ContextMessageProxy(new ContextMessage((String) object.get("name"),
                (Map<String, String>) object.get("properties")));
        }
    }

}
