package net.pinnacle21.oce.utils;

import com.google.common.base.Strings;
import com.google.common.io.Files;
import com.typesafe.config.Config;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.datasource.*;
import net.pinnacle21.oce.model.input.CliParameters;
import net.pinnacle21.oce.model.input.DataSourceInfo;
import net.pinnacle21.oce.model.input.Input;
import net.pinnacle21.oce.model.input.ParseResults;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

import static net.pinnacle21.oce.utils.CliPreconditions.checkProvided;
import static net.pinnacle21.oce.utils.CliPreconditions.checkState;
import static net.pinnacle21.oce.utils.DataSources.*;

public final class OutputUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(OutputUtils.class);

    private OutputUtils() {
    }

    private static DataSourceInfo.Builder getOutputFileBuilder(ParseResults results, Input<String> output, String defaultName) {
        DataSourceInfo outputFolder = getOutputFolder(results, output);
        if (output.isProvided() &&
            outputFolder.getDataSource() != DataSource.EMPTY &&
            !isDirectory(new File(output.get()))) {
            // return output file
            File outputFile = new File(output.get());
            return DataSourceInfo.builder()
                .setDataSource(DataSources.newFileOrFolder(outputFolder.getDataSource(),
                    outputFile.getName()))
                .setFormat(outputFolder.getFormat());
        }
        if (outputFolder.getDataSource() == DataSource.EMPTY ||
            (!output.isProvided() && Strings.isNullOrEmpty(defaultName))) {
            // cannot build output file since folder is not defined or no file name provided
            return DataSourceInfo.builder();
        }

        DataSource outputFileDs = DataSources.newFileOrFolder(outputFolder.getDataSource(),
            defaultName);

        LOGGER.info("Output file is set to '{}'", outputFileDs.getAbsolutePath());

        return DataSourceInfo.builder()
            .setDataSource(outputFileDs)
            .setFormat(outputFolder.getFormat());
    }

    private static DataSourceInfo getOutputFileOrFolder(ParseResults results, Input<String> output, String defaultName) {
        DataSourceInfo ds = getOutputFileBuilder(results, output, defaultName)
            .build();
        if (ds.isValid()) {
            return ds;
        }

        return getOutputFolder(results, output);
    }

    public static DataSourceInfo.Builder getOutputFileBuilder(ParseResults results, String defaultName) {
        return getOutputFileBuilder(results,
            results.getCliParameters().getOutput(), defaultName);
    }

    public static DataSourceInfo getOutputFileOrFolder(ParseResults results, String defaultName) {
        return getOutputFileOrFolder(results,
            results.getCliParameters().getOutput(), defaultName);
    }

    public static DataSourceInfo getReportFile(ParseResults results, String defaultName) {
        CliParameters cliParameters = results.getCliParameters();

        Input<String> reportName = cliParameters.getReport();
        if (reportName.isProvided() && "url".equalsIgnoreCase(reportName.get())) {
            // url is requested
            return new DataSourceInfo.Builder()
                .setFormat(FormatType.URL)
                .setDataSource(URLSource.EMPTY)
                .build();
        }

        DataSource reportDS = Schema.toDataSource(reportName, false);
        DataSource remoteReportDS = null;
        if (reportDS.getSchema().isRemote()) {
            remoteReportDS = reportDS;
            DataSource tempFile = DataSources.newFileOrFolder(ResourceUtils.TEMP_DIR,
                "report", FilenameUtils.getName(remoteReportDS.getAbsolutePath()));
            reportDS = new FileDS(tempFile.getAbsolutePath(), true);
        }

        DataSourceInfo sourceInfo = getOutputFileBuilder(results,
            Input.asString(reportDS.getAbsolutePath()), defaultName)
            .setFormat(FormatType.formatForOutput(cliParameters.getReport(),
                Input.asDefault(FormatType.EXCEL)))
            .build();

        checkProvided(sourceInfo.getDataSource(), ValidationExceptions.REQURED_REPORT_FILE);

        checkState(!isDirectory(sourceInfo.getDataSource()),
            ValidationExceptions.REQURED_REPORT_FILE,
            String.format("Report parameter is a folder. Cannot create report. source='%s; default='%s'",
                sourceInfo.getDataSource().getAbsolutePath(), defaultName));

        File rptFile = toFile(sourceInfo.getDataSource());
        FormatType fmtType = sourceInfo.getFormat().defaultIfNotProvided(FormatType.EXCEL);

        checkState(fmtType.isApplicable(rptFile),
            ValidationExceptions.INVALID_REPORT_FILE,
            String.format("Report file name has invalid extension. source='%s'; format=%s",
                sourceInfo.getDataSource().getAbsolutePath(),
                fmtType.getValue()));

        if (rptFile.exists()) {
            // check if file is opened. Will work for windows
            checkState(rptFile.renameTo(rptFile),
                SystemExceptions.OPENED_FILE,
                String.format("Report file '%s' appears to be open. Please close the file and restart the process",
                    rptFile.getAbsolutePath()));
        }
        return DataSourceInfo.builder()
            .setDataSource(sourceInfo.getDataSource())
            .setFormat(fmtType)
            .setCutoff(cliParameters.getReportCutoff())
            .setRemoteDataSource(remoteReportDS)
            .build();
    }

    private static DataSourceInfo getOutputFolder(ParseResults results, Input<String> output) {
        CliParameters cliParameters = results.getCliParameters();

        Config config = results.getConfig();
        File folder = null;

        if (output.isProvided()) {
            DataSource ds = Schema.toDataSource(output.get(), false);
            folder = toFile(ds);

            if (!isDirectory(folder) && folder != null) {
                folder = folder.getParentFile();
            }
        } else if (config.hasPath("cli.output")) {
            // no output provided by the user. check config first
            folder = new File(config.getString("cli.output"));
        }

        DataSource dsFolder;
        if (!isDirectory(folder)) {
            // last attempt to check local folders looking for the report subfolder
            dsFolder = lookupDir("reports", false);
            folder = toFile(dsFolder);
        } else {
            dsFolder = new FolderDS(folder, false);
        }

        if (folder != null && !folder.exists()) {
            try {
                Files.createParentDirs(folder);
            } catch (IOException e) {
                throw new CLIException(
                    SystemExceptions.FAILED_OUTPUT_DIR,
                    String.format(
                        "Failed to create folders for output=%s",
                        dsFolder.getAbsolutePath()
                    ),
                    e
                );
            }
        }

        return DataSourceInfo.builder()
            .setDataSource(dsFolder)
            .setFormat(FormatType.formatForOutput(output, cliParameters.getOutputFormat()))
            .build();
    }
}
