package net.pinnacle21.oce.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import joptsimple.OptionException;
import net.bootstrap.api.monitor.context.DynamicContext;
import net.pinnacle21.oce.events.*;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.CLIExceptionCode;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.logging.StringAppender;
import net.pinnacle21.oce.model.BaseModel;
import net.pinnacle21.oce.model.ProcessState;
import net.pinnacle21.oce.model.input.CliParameters;
import net.pinnacle21.oce.model.input.CommandLine;
import net.pinnacle21.oce.model.input.CommunityInput;
import net.pinnacle21.oce.model.request.Request;
import net.pinnacle21.oce.model.request.RequestType;
import net.pinnacle21.oce.model.response.*;
import net.pinnacle21.oce.services.RemoteAppender;
import net.pinnacle21.oce.starters.RequestStarter;
import net.pinnacle21.oce.starters.converter.ConverterStarter;
import net.pinnacle21.oce.starters.define.DefineStarter;
import net.pinnacle21.oce.starters.list.ListStarter;
import net.pinnacle21.oce.starters.miner.MinerStarter;
import net.pinnacle21.oce.starters.validator.ValidatorStarter;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.lang.Math.min;
import static net.pinnacle21.oce.utils.CliErrorUtils.report;
import static net.pinnacle21.oce.utils.CliErrorUtils.toCode;

public final class ProcessUtils {
    public static final String COMMUNITY_VERSION = StringUtils.defaultIfBlank(
        System.getProperty("pinnacle.software.version"),
        Resources.BUILD_VERSION
    );
    public static final String SOURCE_SYSTEM;
    public static final String CLIENT_ID;
    
    private static final String CCLI_CLIENT_ID = "ccli";
    private static final String GUI_CLIENT_ID = "gui";
    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessUtils.class);
    private static final Map<RequestType, RequestStarter> STARTER_MAP;
    private static final StringAppender STRING_APPENDER = new StringAppender();

    static {
        ImmutableMap.Builder<RequestType, RequestStarter> builder =
                ImmutableMap.<RequestType, RequestStarter>builder()
                        .put(RequestType.VALIDATOR, new ValidatorStarter())
                        .put(RequestType.DEFINE, new DefineStarter())
                        .put(RequestType.CONVERTER, new ConverterStarter())
                        .put(RequestType.LIST_CONFIG, new ListStarter())
                        .put(RequestType.MINER, new MinerStarter());

        STARTER_MAP = builder.build();

        CLIENT_ID = StringUtils.defaultIfBlank(System.getProperty("pinnacle.software.clientId"), CCLI_CLIENT_ID);
        String community = "Pinnacle 21 Community";
        SOURCE_SYSTEM = GUI_CLIENT_ID.equals(CLIENT_ID) ? community : community + " CLI";
    }

    private ProcessUtils() {
    }

    @SuppressWarnings("unchecked")
    public static Map<ResponseType, List<Response>> startFromJson(final String json) {
        LOGGER.info("received json '{}'", json);
        final Listener listener = new AbstractListener<StateEvent>() {
            @Override
            public void listen(StateEvent event) {
                if (event.getState().isCompleted()) {
                    setResponse(event);
                }
                IPC.send(event);
            }
        };
        try {
            JsonUtils jsonUtils = new JsonUtils(true);
            JsonNode jsonNode = new ObjectMapper().readTree(json);
            setEmail(jsonUtils.safeGetText(jsonNode, "email"));
            setUserAgent(jsonUtils.safeGetText(jsonNode, "userAgent"));
            CommunityInput input = new CommunityInput(json);
            RemoteAppender.setInput(input);
            Map<ResponseType, List<Response>> single = start(input, listener)
                    .subscribeOn(Schedulers.io())
                    .toBlocking()
                    .single();
        } catch (Exception ex) {
            // send exception response
            listener.listen(ExceptionEvent.Builder
                    .builder()
                    .setError(ex)
                    .build());
        }
        return null;
    }

    public static void startFromCommandLine(final String[] args) {
        try {
            final CommandLineReporter reporter = new CommandLineReporter();
            Observable<Map<ResponseType, List<Response>>> oResult = startFromCommandLine(
                    args,
                    new AbstractListener<StateEvent>() {
                        @Override
                        public void listen(StateEvent event) {
                            reporter.report(event);
                            setResponse(event);
                        }
                    }).observeOn(Schedulers.io());
            Map<ResponseType, List<Response>> single = oResult.toBlocking().single();
            exit(single);

        } catch (CLIException cliException) {
            exit(cliException);
        } catch (OptionException optEx) {
            exit(new CLIException(ValidationExceptions.INVALID_COMMAND_LINE_OPTION,
                    String.format("Failed to start process from command line=%s", Joiner.on(' ').join(args)),
                    optEx));
        } catch (Exception ex) {
            exit(new CLIException(String.format("Failed to process input=%s", Joiner.on(' ').join(args)), ex));
        }
    }

    private static Observable<Map<ResponseType, List<Response>>> startFromCommandLine(final String[] args, final Listener<StateEvent> listener) {

        if (isSas()) {
            setupSasEnv();
        }
        CommandLine commandLine = new CommandLine(args);
        RemoteAppender.setInput(commandLine);
        return start(commandLine, listener);
    }

    @SuppressWarnings("unchecked")
    private static Observable<Map<ResponseType, List<Response>>> start(CliParameters params, final Listener listener) {
        if (LOGGER.isDebugEnabled()) {
            RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
            List<String> jvmArgs = runtimeMXBean.getInputArguments();
            for (String arg : jvmArgs) {
                LOGGER.debug("jvm argument: {}", arg);
            }
        }
        return InputUtils.from(params)
                .flatMap((Func1<Request, Observable<Void>>) request -> start(request, listener))
                .onErrorResumeNext((Func1<Throwable, Observable<Void>>) throwable -> {
                    listener.listen(ExceptionEvent.Builder
                            .builder()
                            .setError(throwable)
                            .build());
                    return Observable.just(null);
                })
                .map(v -> listener.getResponse())
                .reduce(Maps.newHashMap(),
                        (responseMap, response) -> {
                            if (response == null) {
                                return responseMap;
                            }
                            List<Response> responses = responseMap.computeIfAbsent(response.getType(), k -> new ArrayList<>());
                            responses.add(response);
                            return responseMap;
                        });
    }

    private static Observable<Void> start(Request request, Listener<StateEvent> listener) {
        RequestStarter starter = STARTER_MAP.get(request.getType());
        if (starter == null) {
            return Observable.error(new CLIException(String.format("Failed to rout request to a starter. Request=%s cannot be mapped for type=%s",
                    request.toString(), request.getType().getValue())));
        }

        return starter.start(request, listener);
    }

    @SuppressWarnings("unused")
    public static void setUserAgent(String userAgent) {
        DynamicContext.getInstance().setUserAgent(userAgent);
    }

    @SuppressWarnings("unused")
    public static void setEmail(String email) {
        DynamicContext.getInstance().setEmail(email);
    }

    public static String toLabel(String origin, String destination) {
        if (Strings.isNullOrEmpty(destination)) {
            return origin;
        }
        if (Strings.isNullOrEmpty(origin)) {
            return destination;
        }

        if (origin.equalsIgnoreCase(destination)) {
            return destination;
        }

        return origin + "->" + destination;
    }

    public static <T extends BaseModel> ProcessEvent<T> buildStartEvent(T target, String label, long total) {
        return ProcessEvent.Builder.<T>builder()
                .setTarget(target)
                .setLabel(label)
                .setCurrent(0)
                .setState(ProcessState.STARTED)
                .setTotal(total)
                .build();
    }

    public static String getMessage() {
        return StringAppender.getMessage();
    }

    private static boolean isSas() {
        return Strings.nullToEmpty(System.getProperty("java.system.class.loader")).contains(".sas.") ||
                !Strings.nullToEmpty(System.getProperty("sas.app.class.path")).isEmpty();
    }

    static int exit() {
        if (STRING_APPENDER.isStarted()) {
            STRING_APPENDER.stop();
        }

        return StringAppender.getExitCode();
    }

    static void exit(int code) {
        if (STRING_APPENDER.isStarted()) {
            LOGGER.info("Process exit code: {}", code);
            StringAppender.setExitCode(code);
        } else {
            System.exit(code);
        }
    }

    static void exit(CLIException exception) {
        LOGGER.error(report(exception.getCode(), exception.getLocalizedMessage()),
                exception);
        exit(exception.getCode());
    }

    static void exit(CLIExceptionCode exception) {
        exit(toCode(exception.getCode()));
    }

    static void exit(Map<ResponseType, List<Response>> responseMap) {
        List<ValidatorResponse> validationResponses = ResponseType.VALIDATOR_RESPONSE.toResponses(responseMap);
        List<ConverterResponse> converterResponses = ResponseType.CONVERTER_RESPONSE.toResponses(responseMap);

        if (validatorExit(validationResponses)) {
            return;
        } else if (converterExit(converterResponses)) {
            return;
        } else {
            List<ExceptionResponse> exceptions = ResponseType.EXCEPTION.toResponses(responseMap);
            List<SuccessResponse> successes = ResponseType.SUCCESS.toResponses(responseMap);
            if (exceptions != null && !exceptions.isEmpty()) {
                exit(exceptions.get(0));
                return;
            }
            if (successes != null && !successes.isEmpty()) {
                int returnCode = 100;
                for (SuccessResponse success : successes) {
                    returnCode = min(returnCode, success.getExitCode());
                }
                exit(returnCode);
                return;
            }
        }
        exit(0);
    }

    private static boolean validatorExit(List<ValidatorResponse> validatorResponses) {
        if (validatorResponses != null && !validatorResponses.isEmpty()) {
            int returnCode = Integer.MAX_VALUE;

            for (ValidatorResponse response : validatorResponses) {
                returnCode = min(response.getSeverity(), returnCode);
            }
            exit(returnCode);
            return true;
        }

        return false;
    }

    private static boolean converterExit(List<ConverterResponse> converterResponses) {
        if (converterResponses != null && !converterResponses.isEmpty()) {
            int returnCode = 0;

            for (ConverterResponse response : converterResponses) {
                if (!Strings.isNullOrEmpty(response.getTruncation())) {
                    LOGGER.warn("Conversion results truncated. Info '{}'", response.getTruncation());
                    returnCode = 1;
                }
            }
            exit(returnCode);
            return true;
        }

        return false;
    }

    private static void setupSasEnv() {
        if (!STRING_APPENDER.isStarted()) {
            STRING_APPENDER.start();
        }

    }

    public static boolean isGui() {
        return GUI_CLIENT_ID.equals(CLIENT_ID);
    }

    public static boolean isCli() {
        return CCLI_CLIENT_ID.equals(CLIENT_ID);
    }
}
