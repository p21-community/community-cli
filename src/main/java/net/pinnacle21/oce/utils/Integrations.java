package net.pinnacle21.oce.utils;

import com.google.common.base.Predicate;
import com.google.common.io.Files;
import net.bootstrap.core.utils.BootstrapFileUtils;
import net.pinnacle21.oce.model.datasource.FileDS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import static net.bootstrap.core.utils.jar.JarUtils.isKeepArtifact;

public final class Integrations {
    private static final Logger LOGGER = LoggerFactory.getLogger(Integrations.class);
    private static final FileDS TEMP_DIR;

    static {
        File tempDir = new File(ResourceUtils.TEMP_DIR, "integrations");
        FileDS tempDS = new FileDS(tempDir, true);
        try {
            BootstrapFileUtils.forceMkdir(tempDir);
        } catch (Exception e) {
            LOGGER.debug("Failed to create integrations temp dir", e);
            tempDS = new FileDS(Files.createTempDir(), true);
        }
        TEMP_DIR = tempDS;
    }

    private Integrations() {
    }

    public static Predicate<String> cliJarPredicate(final String root, final String lib) {
        return new Predicate<String>() {
            @Override
            public boolean apply(String input) {
                return isKeepArtifact(root, lib, input);
            }
        };
    }
}
