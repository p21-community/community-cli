package net.pinnacle21.oce.utils;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigObject;
import com.typesafe.config.ConfigRenderOptions;
import net.bootstrap.core.utils.BootstrapFileUtils;
import net.bootstrap.core.utils.jar.JarUtils;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.CLIExceptionCode;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.StandardType;
import net.pinnacle21.oce.model.datasource.AbstractFileDS;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.datasource.FileDS;
import net.pinnacle21.oce.model.datasource.FolderDS;
import net.pinnacle21.oce.model.datasource.MultiSource;
import net.pinnacle21.oce.model.datasource.Schema;
import net.pinnacle21.oce.model.input.DataSourceInfo;
import net.pinnacle21.oce.model.input.Input;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static net.bootstrap.core.utils.jar.JarUtils.getFileLocation;
import static net.bootstrap.core.utils.jar.JarUtils.getJarFile;
import static net.bootstrap.core.utils.jar.JarUtils.isRunningJar;
import static net.bootstrap.core.utils.jar.URLClassLoaderUtils.unloadAll;
import static net.pinnacle21.oce.utils.CliPreconditions.checkProvided;
import static net.pinnacle21.oce.utils.CliPreconditions.checkState;
import static net.pinnacle21.oce.utils.Constants.LOG_FILE_PATH;

public final class DataSources {
    static final Set<Schema> COMBINABLE = EnumSet.of(Schema.ASPERA, Schema.FILE);
    private static final Logger LOGGER = LoggerFactory.getLogger(DataSources.class);
    private static final Predicate<File> ALL_FILES = Objects::nonNull;
    private static final Pattern CLEANUP_PATTERN = Pattern.compile("^\"|\"$|^'|'$");
    private static final String LOG_EXTENSION = ".log";

    static {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {

                // unload all jars
                unloadAll();
                System.gc();
                List<String> failedFiles = new ArrayList<>();
                File tempCliFile = getCliTempFile();
                if (tempCliFile.exists()) {
                    failedFiles.addAll(cleanupOldTemps(tempCliFile));
                }

                String sysTemp = System.getProperty("java.io.tmpdir");
                if (sysTemp.endsWith(File.separator)) {
                    sysTemp = FilenameUtils.getFullPathNoEndSeparator(sysTemp);
                }

                for (AbstractFileDS ds : AbstractFileDS.CLEANUP_SOURCES) {
                    // delete ds
                    try {
                        if (ds != null && ds.getFile() != null && ds.getFile().exists() &&
                            !ds.getAbsolutePath().equalsIgnoreCase(sysTemp)) {
                            FileUtils.forceDelete(ds.getFile());
                        }
                    } catch (Exception e) {
                        // ignore errors. continue with delete
                        LOGGER.debug("Failed to empty/delete temp file/folder {}",
                            ds.getFile().getAbsolutePath(), e);
                        System.gc();
                        try {
                            Thread.sleep(100);
                            FileUtils.forceDelete(ds.getFile());
                        } catch (Exception e1) {
                            failedFiles.add(ds.getAbsolutePath());
                        }
                    }

                    if (!failedFiles.isEmpty()) {
                        updateTempFile(tempCliFile, failedFiles);
                    }
                }
            }
        });
    }

    private DataSources() {
    }

    private static List<String> cleanupOldTemps(File tempCliFile) {
        Config tempConfig = ConfigFactory.parseFile(tempCliFile);
        List<String> stillFailedFiles = new ArrayList<>();

        if (tempConfig.hasPath("cli.failed.files")) {
            LOGGER.info("Loading CLI temp file and cleaning up old temp files and folders ... File {}",
                tempCliFile.getAbsolutePath());
            List<String> failedFiles = tempConfig.getStringList("cli.failed.files");
            for (String failedFile : failedFiles) {
                try {
                    FileUtils.forceDelete(new File(failedFile));
                } catch (IOException e) {
                    LOGGER.debug("Failed to delete old temp file {}", failedFile, e);
                    stillFailedFiles.add(failedFile);
                }
            }
        }

        return stillFailedFiles;
    }

    private static void updateTempFile(File tempCliFile, List<String> failedFiles) {
        if (failedFiles != null && !failedFiles.isEmpty()) {
            // write failed files to a temp file for later cleanup
            Map<String, Object> content = null;
            Config tempConfig = null;
            if (tempCliFile.exists()) {
                tempConfig = ConfigFactory.parseFile(tempCliFile);
                if (tempConfig.hasPath("cli")) {
                    ConfigObject configObject = tempConfig.getObject("cli");
                    content = configObject.unwrapped();
                    content.put("failed.files", failedFiles);
                }
            }
            if (content == null) {
                content = ImmutableMap.of("cli.failed.files", failedFiles);
                tempConfig = ConfigFactory.parseMap(content);
            }

            if (tempConfig != null) {
                String failedFilesToWrite = tempConfig.root().render(ConfigRenderOptions.concise());
                try {
                    LOGGER.info("Saving CLI temp file and record all temp files and folders that could not be deleted. File {} ...",
                        tempCliFile.getAbsolutePath());
                    FileUtils.writeStringToFile(tempCliFile, failedFilesToWrite, (String) null);
                } catch (IOException e) {
                    LOGGER.debug("write temp CLI file failed. File {}", tempCliFile.getAbsolutePath(),
                        e);
                }
            }
        }
    }

    private static File getCliTempFile() {
        File baseDir = new File(System.getProperty("java.io.tmpdir"), "p21");
        File cliDir = new File(baseDir, "cli");
        try {
            BootstrapFileUtils.forceMkdir(cliDir);
        } catch (IOException e) {
            return new File(System.getProperty("java.io.tmpdir"), "p21-client.tmp");
        }
        return new File(cliDir, "p21-client.tmp");
    }

    public static DataSource toDataSource(Input<String> ds,
                                          Input<Boolean> cleanup) {
        if (ds == null ||
            ds.isEmpty() ||
            cleanup == null ||
            cleanup.isEmpty()) {
            return DataSource.EMPTY;
        }

        return Schema.toDataSource(ds, cleanup.get());
    }

    public static DataSource toDataSource(File file,
                                          boolean cleanup) {
        if (file == null) {
            return DataSource.EMPTY;
        }

        if (isDirectory(file)) {
            return new FolderDS(file, cleanup);
        } else {
            return new FileDS(file, cleanup);
        }
    }

    public static DataSource toDataSource(List<String> inputSources,
                                          final DataSourceContext cxt) {
        List<DataSource> sources = inputSources.stream()
            .map(input -> Schema.toDataSource(input, cxt.cleanup))
            .filter(DataSources::isValid)
            .collect(Collectors.toList());

        if (sources.isEmpty()) {
            throw new CLIException(ValidationExceptions.INVALID_DATA_SOURCE,
                String.format("Invalid data source parameter=%s for type=%s",
                    Joiner.on(';').join(inputSources), cxt.type == null ? "unknown" : cxt.type.getValue()));
        }

        if (cxt.upload) {
            return combineDataSources(sources);
        }

        if (sources.size() == 1) {
            return sources.get(0);
        } else if (cxt.combine) {
            return combineDataSources(sources);
        } else {
            return new MultiSource(sources);
        }
    }

    private static String sourcePrefix(List<DataSource> sources) {
        Map<String, Integer> counter = Maps.newHashMap();
        String commonPath = null;
        int max = 0;
        for (DataSource ds : sources) {
            String key = StringUtils.defaultIfBlank(FilenameUtils.getPathNoEndSeparator(ds.getAbsolutePath()),
                "blank");
            Integer cnt = counter.get(key);
            if (cnt == null) {
                cnt = 0;
            }
            counter.put(key, ++cnt);
            if (cnt > max) {
                commonPath = key;
                max = cnt;
            }
        }
        if (commonPath == null) {
            return "";
        }
        String[] parts = FilenameUtils.separatorsToUnix(commonPath).split("/");
        if (parts.length > 4) {
            // keep only last 4 levels
            return Joiner.on(File.separator)
                .join(Arrays.stream(parts).skip(parts.length - 4).collect(Collectors.toList()));
        } else {
            return commonPath;
        }
    }

    static DataSource copyDataSource(DataSource source, String destName) {
        if (!isValid(source)) {
            return source;
        }

        File tempDir = new File(ResourceUtils.TEMP_DIR, sourcePrefix(Lists.newArrayList(source)) +
            File.separator + UUID.randomUUID().toString());
        File tempFile = new File(tempDir, destName);

        try {
            BootstrapFileUtils.forceMkdir(tempDir);
            File inFile = toFile(source);
            FileUtils.copyFile(inFile, tempFile, false);
            LOGGER.info("Copy file {} to {}", inFile.getAbsolutePath(),
                tempFile.getAbsoluteFile());
            return new FileDS(tempFile, true);
        } catch (IOException e) {
            LOGGER.warn("Failed to copy file {} to {}", source, tempFile.getAbsoluteFile());
            return source;
        }
    }

    private static DataSource combineDataSources(List<DataSource> sources) {
        final File tempDir = new File(ResourceUtils.TEMP_DIR, sourcePrefix(sources));

        try {
            BootstrapFileUtils.forceMkdir(tempDir);

            for (DataSource ds : sources) {
                if (!COMBINABLE.contains(ds.getSchema())) {
                    // combine data sources called for non-file source. This is not supported
                    throw new CLIException(ValidationExceptions.MISSING_DATA_SOURCE,
                        String.format("Cannot combine non-file data sources=%s",
                            sources));
                }
                final AbstractFileDS fileDS = (AbstractFileDS) ds;
                if (isDirectory(fileDS)) {
                    FileUtils.copyDirectory(fileDS.getFile(), tempDir, false);
                } else {
                    FileUtils.copyFileToDirectory(fileDS.getFile(), tempDir, false);
                }
            }
            return new FolderDS(tempDir, true);
        } catch (IOException e) {
            String msg = String.format("Failed to combine data sources=%s.", sources);
            try {
                FileUtils.forceDelete(tempDir);
            } catch (IOException e1) {
                msg += " Failed to cleanup temp dir. Error: " + e1.getMessage();
            }
            throw new CLIException(SystemExceptions.FAILED_COPY_SOURCE, msg, e);
        }
    }

    public static void validate(DataSource ds,
                                CLIExceptionCode errorCode,
                                String msg) {
        checkProvided(ds, errorCode, msg);
        checkState(isValid(ds), errorCode, msg);
        if (ds.getSchema() == Schema.FILE) {
            AbstractFileDS fileDS = (AbstractFileDS) ds;
            validate(fileDS.getFile(), errorCode, msg);
        }
    }

    static void validate(File file,
                         CLIExceptionCode errorCode,
                         String msg) {
        checkProvided(file, errorCode, msg);
        checkState(file.exists(), errorCode, msg);
    }

    public static List<File> toFiles(DataSource sources) {
        return toFiles(sources, ALL_FILES);
    }

    public static List<File> toFiles(DataSource sources, Predicate<File> predicate) {
        if (sources.getSchema() == Schema.MULTI_SOURCE) {
            return toFiles(((MultiSource) sources).getSources(), predicate);
        } else if (sources.getSchema() == Schema.FILE) {
            return toFiles(from(sources, predicate));
        }

        return ImmutableList.of();
    }

    public static File toFile(DataSource ds) {
        if (ds != null && ds.getSchema().isFile()) {
            return ((AbstractFileDS) ds).getFile();
        }

        return null;
    }

    public static File toFile(DataSourceInfo info) {
        if (info != null) {
            return toFile(info.getDataSource());
        }

        return null;
    }

    public static List<File> toFiles(Collection<DataSource> sources) {
        return toFiles(sources, ALL_FILES);
    }

    private static List<File> toFiles(Collection<DataSource> sources, Predicate<File> predicate) {
        List<File> allFiles = Lists.newArrayList();
        for (DataSource src : sources) {
            allFiles.addAll(toFiles(src, predicate, 0));
        }

        return allFiles;
    }

    private static List<File> toFiles(DataSource src, Predicate<File> predicate, int level) {
        List<File> result = Lists.newArrayList();

        if (src.getCount() == 1) {
            File srcFile = toFile(src);
            if (predicate.test(srcFile)) {
                result.add(srcFile);
            }
        } else if (level < 2) {
            List<DataSource> children = from(src, predicate);
            for (DataSource child : children) {
                result.addAll(toFiles(child, predicate, level + 1));
            }
        }

        return result;
    }

    public static AbstractFileDS newFileOrFolder(File file, String... children) {
        if (file == null) {
            return null;
        }

        if (isDirectory(file)) {
            return newFileOrFolder(new FolderDS(file, false), children);
        }

        return newFileOrFolder(new FileDS(file, false), children);
    }

    public static AbstractFileDS newFileOrFolder(DataSource ds, String... children) {
        File dsFile = toFile(ds);

        if (isDirectory(dsFile) &&
            children != null &&
            children.length > 0) {
            dsFile = newFile(dsFile, children);
        } else if (dsFile != null &&
            children != null &&
            children.length > 0) {
            // ds is a file - get parent
            dsFile = newFile(dsFile.getParentFile(), children);
        }

        if (isDirectory(dsFile)) {
            return new FolderDS(dsFile, ds.isCleanup());
        } else {
            return new FileDS(dsFile, ds.isCleanup());
        }
    }

    public static boolean makeDataSource(DataSource ds) {
        File file = toFile(ds);
        if (file == null) {
            return false;
        }

        try {
            if (file.exists()) {
                return true;
            }

            if (isDirectory(ds)) {
                BootstrapFileUtils.forceMkdir(file);
            } else {
                BootstrapFileUtils.forceMkdir(file.getParentFile());
            }
            return true;
        } catch (Exception e) {
            LOGGER.warn("Failed to create folder={}", file.getAbsolutePath(), e);
            return false;
        }
    }

    public static boolean isDirectory(DataSource ds) {
        return isDirectory(toFile(ds));
    }

    public static boolean isDirectory(Input<File> inFile) {
        return inFile.isProvided() &&
            isDirectory(inFile.get());
    }

    public static boolean isDirectory(File file) {
        if (file == null) {
            return false;
        }
        if (file.exists()) {
            return file.isDirectory();
        }

        String version = BootstrapFileUtils.getVersion(file);
        if (Strings.isNullOrEmpty(version)) {
            return Strings.isNullOrEmpty(FilenameUtils.getExtension(file.getName()));
        } else {
            return version.equals(file.getName());
        }
    }

    public static DataSource toFolder(DataSource ds) {
        if (isDirectory(ds)) {
            return ds;
        }

        File file = toFile(ds);
        if (file == null) {
            return DataSource.EMPTY;
        }

        return new FolderDS(file.getParentFile(), ds.isCleanup());
    }

    public static List<DataSource> from(DataSource source, Predicate<File> predicate) {
        predicate = Optional.ofNullable(predicate).orElse(ALL_FILES);

        if (source.getSchema() == Schema.MULTI_SOURCE) {
            MultiSource multiSource = (MultiSource) source;
            List<DataSource> sources = Lists.newArrayList();
            for (DataSource s : multiSource.getSources()) {
                sources.addAll(from(s, predicate));
            }

            return sources;
        }

        final File file = toFile(source);
        if (file == null) {
            return ImmutableList.of();
        } else if (!isDirectory(file)) {
            return predicate.test(file) ? ImmutableList.of(source) : ImmutableList.of();
        }

        File[] children = file.listFiles();
        if (children == null) {
            return ImmutableList.of();
        }

        return Arrays.stream(children)
            .filter(predicate)
            .map(input -> newFileOrFolder(input))
            .collect(Collectors.toList());
    }

    public static FormatType determineSourceFormatType(DataSource dataSource, boolean source) {
        if (dataSource.getSchema() == Schema.URL) {
            return FormatType.UNKNOWN_FORMAT;
        }

        // source format was not provided. Try to figure out from sourceData
        List<String> sourceExtensions = from(
            dataSource, input -> input != null && !isDirectory(input)
        )
            .stream()
            .map(input -> FilenameUtils.getExtension(input.getAbsolutePath()))
            .collect(Collectors.toList());

        Map<String, Integer> extCounter = Maps.newHashMap();
        int max = 0;
        String commonExt = null;
        for (String ext : sourceExtensions) {
            Integer cnt = extCounter.get(ext);
            if (cnt == null) {
                cnt = 0;
            }
            ++cnt;
            extCounter.put(ext, cnt);
            if (cnt > max) {
                max = cnt;
                commonExt = ext;
            }
        }
        // check for ties
        int maxCnt = 0;
        for (Map.Entry<String, Integer> entry : extCounter.entrySet()) {
            if (entry.getValue() == max) {
                ++maxCnt;
            }
        }

        if (source && extCounter.containsKey("xpt")) {
            LOGGER.info("Folder {} contains XPT files. Setting format to XPT by default", dataSource.getAbsolutePath());
            return FormatType.XPORT;
        }

        if (maxCnt > 1) {
            LOGGER.warn("Cannot determine source format type. More than one file extension found in the source '{}'",
                dataSource.getAbsolutePath());
            return FormatType.UNKNOWN_FORMAT;
        }

        FormatType fmt = FormatType.fromExt(commonExt);
        LOGGER.info("After looking at the dir {} CLI found these extensions [{}] and picked {}",
            dataSource.getAbsolutePath(), extCounter, commonExt);
        return fmt;
    }

    public static String cleanupPath(String path) {
        return CLEANUP_PATTERN.matcher(Strings.nullToEmpty(path).trim()).replaceAll("");
    }

    public static boolean exists(DataSource dataSource) {
        if (dataSource.getSchema() == Schema.MULTI_SOURCE) {
            // multi source check each DS
            MultiSource multiSource = (MultiSource) dataSource;
            for (DataSource child : multiSource.getSources()) {
                if (!exists(child)) {
                    return false;
                }
            }

            return true;
        }

        File file = toFile(dataSource);
        return file != null && file.exists();
    }

    public static boolean exists(DataSourceInfo dataSourceInfo) {
        return exists(dataSourceInfo.getDataSource());
    }

    public static DataSource lookupDir(String dir, boolean create) {
        // look for config folder around CLI
        File appPath = JarUtils.getAppPath(DataSource.class);
        // look for config folder in current dir
        DataSource configFolderFromApp = DataSources.newFileOrFolder(appPath, dir);
        if (DataSources.exists(configFolderFromApp)) {
            LOGGER.info("Found '{}' folder in local dir {}. Will use it",
                dir, configFolderFromApp.getAbsolutePath());
            return configFolderFromApp;
        }

        // look in parent dir
        DataSource configFolder = DataSources.newFileOrFolder(appPath.getParentFile(), dir);
        if (DataSources.exists(configFolder)) {
            LOGGER.info("Found '{}' folder in parent dir {}. Will use it",
                dir, configFolder.getAbsolutePath());
            return configFolder;
        }

        // give up
        LOGGER.warn("'{}' folder or file is not found.", dir);
        if (create) {
            LOGGER.info("Create folder {}", configFolderFromApp.getAbsolutePath());
            if (appPath.getName().equals("classes")) {
                /*
                    When developing, we want to download libs directly to 'target/lib' instead of
                    'target/classes/lib'
                 */
                DataSources.makeDataSource(configFolder);
                return configFolder;
            } else {
                DataSources.makeDataSource(configFolderFromApp);
                return configFolderFromApp;
            }
        }

        return DataSource.EMPTY;
    }

    public static boolean isValid(DataSource ds) {
        return ds != null &&
            ds.isValid() &&
            ds != DataSource.EMPTY;
    }

    public static String getLogFile() {
        String filePath = System.getProperty(LOG_FILE_PATH);
        if (StringUtils.isNotBlank(filePath)) {
            return filePath;
        }
        String fileName = getJarFile(DataSource.class);
        if (fileName != null && isRunningJar()) {
            DataSource logsDir = DataSources.lookupDir("logs", false);
            if (DataSources.isValid(logsDir)) {
                return DataSources.newFileOrFolder(logsDir, FilenameUtils.getBaseName(fileName) + LOG_EXTENSION)
                    .getAbsolutePath();
            }
            return fileName.replace(".jar", LOG_EXTENSION);
        } else {
            return new File(getFileLocation(DataSource.class), "client").getAbsolutePath();
        }
    }

    private static File newFile(File root, String... children) {
        if (children == null || children.length == 0) {
            return root;
        }

        int child = 0;
        File result = root;
        if (root == null) {
            result = new File(children[child++]);
        }

        for (; child < children.length; ++child) {
            if (!Strings.isNullOrEmpty(children[child])) {
                result = new File(result, children[child]);
            }
        }

        return result;
    }

    public static class DataSourceContext {
        private StandardType type;
        private boolean cleanup;
        private boolean upload;
        private boolean combine;

        private DataSourceContext() {
        }

        public static DataSourceContext context() {
            return new DataSourceContext();
        }

        DataSourceContext withType(StandardType type) {
            this.type = type;
            return this;
        }

        DataSourceContext withCleanup(boolean cleanup) {
            this.cleanup = cleanup;
            return this;
        }

        DataSourceContext withUpload(boolean upload) {
            this.upload = upload;
            return this;
        }

        DataSourceContext withCombine(boolean combine) {
            this.combine = combine;
            return this;
        }
    }
}
