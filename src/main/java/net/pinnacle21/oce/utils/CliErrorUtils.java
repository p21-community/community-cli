package net.pinnacle21.oce.utils;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.FluentIterable;
import com.google.common.primitives.UnsignedBytes;
import net.bootstrap.api.iq.IqException;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.CLIExceptionCode;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import rx.exceptions.CompositeException;

import javax.annotation.Nullable;
import java.io.IOException;

public final class CliErrorUtils {
    private static final Predicate<Throwable> PERMISSION_DENIED_CAUSE = new Predicate<Throwable>() {
        @Override
        public boolean apply(@Nullable Throwable input) {
            return input instanceof IOException &&
                StringUtils.containsIgnoreCase(input.getMessage(), "denied");
        }
    };

    private CliErrorUtils() {
    }

    public static CLIExceptionCode getErrorCode(Throwable ex) {
        if (ex == null) {
            return CLIExceptionCode.GENERAL_EXCEPTION;
        }

        if (ex instanceof CLIException) {
            return ((CLIException) ex).getCode();
        }

        if (ex instanceof IqException) {
            return SystemExceptions.FAILED_SYSTEM_IQ_CHECK;
        }

        FluentIterable<Throwable> causes = FluentIterable.from(ExceptionUtils.getThrowableList(ex));
        CLIExceptionCode errCode = causes.firstMatch(new Predicate<Throwable>() {
            @Override
            public boolean apply(Throwable input) {
                return input instanceof CLIException;
            }
        })
            .transform(new Function<Throwable, CLIExceptionCode>() {
                @Override
                public CLIExceptionCode apply(Throwable input) {
                    return ((CLIException) input).getCode();
                }
            }).or(CLIExceptionCode.GENERAL_EXCEPTION);

        if (errCode == CLIExceptionCode.GENERAL_EXCEPTION) {
            // check for access denied error
            errCode = causes.firstMatch(PERMISSION_DENIED_CAUSE)
                .transform(new Function<Throwable, CLIExceptionCode>() {
                    @Override
                    public CLIExceptionCode apply(Throwable input) {
                        return SystemExceptions.PERMISSION_DENIED;
                    }
                }).or(CLIExceptionCode.GENERAL_EXCEPTION);
        }

        return errCode;
    }

    public static CLIExceptionCode getProcessException(Throwable e) {
        CLIExceptionCode errCode = getErrorCode(e);
        return errCode == CLIExceptionCode.GENERAL_EXCEPTION ?
            SystemExceptions.FAILED_CORE : errCode;
    }

    public static CLIException propagate(Throwable e, CLIException defaultException) {
        if (e instanceof CLIException) {
            return (CLIException) e;
        }

        return defaultException;
    }

    public static String report(Throwable error) {
        if (error instanceof CLIException) {
            return Joiner.on(System.lineSeparator())
                .join(report((CLIException) error), ExceptionUtils.getStackTrace(error));
        } else {
            CLIExceptionCode errCode = getErrorCode(error);

            return Joiner.on(System.lineSeparator())
                .join(report(errCode, error.getLocalizedMessage()), ExceptionUtils.getStackTrace(error));
        }
    }

    public static String report(CLIException cliException) {
        return report(cliException.getCode(),
            cliException.getLocalizedMessage());
    }

    public static String report(CLIExceptionCode code, String message) {
        if (Strings.isNullOrEmpty(message)) {
            return String.format("%s::%s::Error", code.getRef(),
                code.getMessage());
        } else {
            return String.format("%s::%s::%s", code.getRef(),
                code.getMessage(), message);
        }
    }

    public static int toCode(byte code) {
        if (code > 0) {
            return UnsignedBytes.toInt((byte) (-1 * code));
        }

        return UnsignedBytes.toInt(code);
    }

    public static int toCode(String ref) {
        if (ref == null) {
            return 0;
        }

        String[] parts = ref.split("\\.");
        if (parts.length > 2) {
            int i2 = Integer.parseInt(parts[2]);
            return Integer.parseInt(String.format("%s%02d", parts[1], i2));
        }

        return Integer.parseInt(ref);
    }

    public static String getPermissionCause(Throwable cause) {
        return FluentIterable.from(ExceptionUtils.getThrowableList(cause))
            .firstMatch(PERMISSION_DENIED_CAUSE)
            .transform(new Function<Throwable, String>() {
                @Override
                public String apply(Throwable input) {
                    return input.getLocalizedMessage();
                }
            }).or("Permission denied error");
    }
}
