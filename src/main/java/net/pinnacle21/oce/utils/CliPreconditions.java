package net.pinnacle21.oce.utils;

import com.google.common.base.Strings;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.CLIExceptionCode;
import net.pinnacle21.oce.model.StandardType;
import net.pinnacle21.oce.model.input.Input;

import java.io.File;
import java.util.Collection;

public final class CliPreconditions {
    private CliPreconditions() {
    }

    public static <T> T checkProvided(T obj, String msg) {
        return checkProvided(obj, CLIExceptionCode.GENERAL_EXCEPTION, msg);
    }

    public static <T> T checkProvided(T obj, CLIExceptionCode errorCode) {
        return checkProvided(obj, errorCode, errorCode.getMessage());
    }

    public static <T> Input<T> checkProvided(Input<T> obj, CLIExceptionCode errorCode) {
        return checkProvided(obj, errorCode, errorCode.getMessage());
    }

    public static <T> Input<T> checkProvided(Input<T> obj, CLIExceptionCode errorCode, String msg) {
        if (obj == null || !obj.isProvided()) {
            throw new CLIException(errorCode, errorCode.getMessage());
        }

        return obj;
    }

    public static <T> T checkProvided(T obj, CLIExceptionCode errorCode, String msg) {
        if (obj == null) {
            throw new CLIException(errorCode, msg);
        }

        return obj;
    }

    public static String checkString(String obj, CLIExceptionCode errorCode) {
        return checkString(obj, errorCode, errorCode.getMessage());
    }

    public static String checkString(String obj, CLIExceptionCode errorCode, String msg) {
        if (Strings.isNullOrEmpty(obj)) {
            throw new CLIException(errorCode, msg);
        }

        return obj;
    }

    public static StandardType checkStandard(StandardType obj, String msg) {
        return checkStandard(obj, CLIExceptionCode.GENERAL_EXCEPTION, msg);
    }

    public static StandardType checkStandard(StandardType obj, CLIExceptionCode errorCode) {
        return checkProvided(obj, errorCode, errorCode.getMessage());
    }

    public static StandardType checkStandard(StandardType obj, CLIExceptionCode errorCode, String msg) {
        if (!StandardType.isValid(Input.asInput(obj))) {
            throw new CLIException(errorCode, msg);
        }

        return obj;
    }

    public static <T> Collection<T> checkCollection(Collection<T> coll, String msg) {
        return checkCollection(coll, CLIExceptionCode.GENERAL_EXCEPTION, msg);
    }

    public static <T> Collection<T> checkCollection(Collection<T> coll, CLIExceptionCode errorCode) {
        return checkCollection(coll, errorCode, errorCode.getMessage());
    }

    public static <T> Collection<T> checkCollection(Collection<T> coll, CLIExceptionCode errorCode, String msg) {
        checkProvided(coll, errorCode, msg);

        if (coll.isEmpty()) {
            throw new CLIException(errorCode, msg);
        }

        return coll;
    }

    public static void checkState(boolean state, String msg) {
        checkState(state, CLIExceptionCode.GENERAL_EXCEPTION, msg);
    }

    public static void checkState(boolean state, CLIExceptionCode errorCode) {
        checkState(state, errorCode, errorCode.getMessage());
    }

    public static void checkState(boolean state, CLIExceptionCode errorCode, String msg) {
        if (!state) {
            throw new CLIException(errorCode, msg);
        }
    }

    public static File exists(File file, CLIExceptionCode errorCode, String msg) {
        checkProvided(file, errorCode, msg);
        if (file.exists()) {
            return file;
        }

        throw new CLIException(errorCode, msg);
    }

    public String checkString(String obj, String msg) {
        return checkString(obj, CLIExceptionCode.GENERAL_EXCEPTION, msg);
    }
}
