package net.pinnacle21.oce.utils;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import joptsimple.BuiltinHelpFormatter;
import joptsimple.OptionDescriptor;
import net.pinnacle21.oce.exceptions.CLIExceptionCode;
import org.reflections.Reflections;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static net.pinnacle21.oce.utils.CliErrorUtils.toCode;

public class HelpPrinter extends BuiltinHelpFormatter {
    private boolean errorList = false;

    public HelpPrinter() {
        this(80, 2);
    }

    /**
     * Makes a formatter with a given overall row width and column separator width.
     *
     * @param desiredOverallWidth         how many characters wide to make the overall help display
     * @param desiredColumnSeparatorWidth how many characters wide to make the separation between option column and
     */
    private HelpPrinter(int desiredOverallWidth, int desiredColumnSeparatorWidth) {
        super(desiredOverallWidth, desiredColumnSeparatorWidth);
    }

    @Override
    protected void addRows(Collection<? extends OptionDescriptor> options) {
        errorList = false;
        super.addRows(options);

        // create error report
        Reflections reflect = new Reflections("net.pinnacle21.oce");
        List<? extends OptionDescriptor> errors = reflect.getSubTypesOf(CLIExceptionCode.class)
            .stream()
            .flatMap(clazz -> {
                CLIExceptionCode[] values = clazz.getEnumConstants();
                if (values != null) {
                    return Arrays.stream(values)
                        .map(CLIExceptionOption::new);
                }
                return Lists.newArrayList(
                    new CLIExceptionNonOption(CLIExceptionCode.GENERAL_EXCEPTION),
                    new CLIExceptionOption(CLIExceptionCode.GENERAL_EXCEPTION)
                ).stream();
            })
            .filter(Objects::nonNull)
            .collect(Collectors.toList());

        errorList = true;
        List<OptionDescriptor> errOpts = Lists.newArrayList(errors);
        errOpts.sort((o1, o2) -> {
            CLIExceptionOption exO1 = (CLIExceptionOption) o1;
            CLIExceptionOption exO2 = (CLIExceptionOption) o2;
            return toCode(exO1.getErrorCode().getRef()) - toCode(exO2.getErrorCode().getRef());
        });
        super.addRows(errOpts);
    }

    @Override
    protected void addHeaders(Collection<? extends OptionDescriptor> options) {
        if (errorList) {
            addOptionRow(message("option.divider"), message("description.divider"));
            addOptionRow("Code", "Message");
            addOptionRow(message("option.divider"), message("description.divider"));
        } else {
            super.addHeaders(options);
        }
    }

    class CLIExceptionOption implements OptionDescriptor {
        private final CLIExceptionCode errorCode;

        CLIExceptionOption(CLIExceptionCode errorCode) {
            this.errorCode = errorCode;
        }

        CLIExceptionCode getErrorCode() {
            return errorCode;
        }

        @Override
        public List<String> options() {
            return Lists.newArrayList(errorCode.getRef());
        }

        @Override
        public String description() {
            return errorCode.getMessage();
        }

        @Override
        public List<?> defaultValues() {
            return ImmutableList.of();
        }

        @Override
        public boolean isRequired() {
            return false;
        }

        @Override
        public boolean acceptsArguments() {
            return true;
        }

        @Override
        public boolean requiresArgument() {
            return false;
        }

        @Override
        public String argumentDescription() {
            return String.valueOf(toCode(errorCode.getCode()));
        }

        @Override
        public String argumentTypeIndicator() {
            return "Error code";
        }

        @Override
        public boolean representsNonOptions() {
            return false;
        }
    }

    class CLIExceptionNonOption extends CLIExceptionOption {
        CLIExceptionNonOption(CLIExceptionCode errorCode) {
            super(errorCode);
        }

        @Override
        public String argumentTypeIndicator() {
            return "";
        }

        @Override
        public String argumentDescription() {
            return "";
        }


        @Override
        public boolean representsNonOptions() {
            return true;
        }

        @Override
        public String description() {
            return "Pinnacle CLI arguments";
        }
    }

}
