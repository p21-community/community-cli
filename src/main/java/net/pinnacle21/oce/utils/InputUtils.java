package net.pinnacle21.oce.utils;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.typesafe.config.Config;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.ReportType;
import net.pinnacle21.oce.model.StandardFamily;
import net.pinnacle21.oce.model.StandardType;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.datasource.FileDS;
import net.pinnacle21.oce.model.datasource.FolderDS;
import net.pinnacle21.oce.model.datasource.Schema;
import net.pinnacle21.oce.model.input.*;
import net.pinnacle21.oce.model.request.Request;
import net.pinnacle21.oce.model.request.RequestType;
import net.pinnacle21.oce.model.request.StandardRequestBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;

import javax.annotation.Nullable;
import java.io.File;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import static net.pinnacle21.oce.utils.DataSources.*;
import static net.pinnacle21.oce.utils.ProcessUtils.exit;

public final class InputUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(InputUtils.class);
    private static final Set<ReportType> MINER_REPORTS = EnumSet.of(ReportType.OUTCOMES, ReportType.SITES);
    private InputUtils() {
    }

    public static Observable<? extends Request> from(CliParameters cliParameters) {
        if (cliParameters.help()) {
            exit(0);
        }

        ParseResults resultArgs = ParseResults.parseArgs(cliParameters);
        return Observable.just(determine(resultArgs)
            .toRequest(resultArgs));
    }

    static RequestType determine(ParseResults args) {
        CliParameters arguments = args.getCliParameters();

        if (arguments.listConfiguration().get()) {
            return RequestType.LIST_CONFIG;
        }

        boolean hasReport = arguments.getReport().isProvided();
        boolean hasOutput = arguments.getOutput().isProvided();
        if (!hasReport && !hasOutput) {
            throw new CLIException(
                ValidationExceptions.REQUIRED_OUTPUT,
                String.format(
                    "CLI needs output or report parameter defined for processing. None is found in args=%s",
                    args.getInput()
                )
            );
        }

        if (MINER_REPORTS.contains(arguments.getReportType().get())) {
            return RequestType.MINER;
        }

        // look for source
        if (!hasSource(arguments)) {
            throw new CLIException(
                ValidationExceptions.REQURED_DATASOURCE,
                String.format(
                    "CLI needs data source(s) for processing. Data source is not found in args=%s",
                    args.getInput()
                )
            );
        }

        if (hasSource(StandardFamily.ADaM, arguments) ||
            hasSource(StandardFamily.SDTM, arguments) ||
            hasSource(StandardFamily.SEND, arguments) ||
            (arguments.getDefineFile().isProvided() && !hasSource(null, arguments))) {
            // switch to CONVERTER if output parameter is provided.
            // Otherwise switch to VALIDATOR.
            return hasReport ? RequestType.VALIDATOR : RequestType.CONVERTER;
        } else if (hasSource(null, arguments)) {
            List<String> files = arguments.getSources().get();
            if (files.size() > 1) {
                // more than one file requested -> switch to data converter
                return RequestType.CONVERTER;
            }
            // this could be data conversion or define spec conversion request
            File source = new File(files.get(0));
            DataSources.validate(
                source,
                ValidationExceptions.INVALID_DATA_SOURCE,
                String.format(
                    "Data source file or directory does not exists. source=%s",
                    source.getAbsolutePath()
                )
            );

            Input<FormatType> type = arguments.getSourceFormat();
            if (!type.isProvided()) {
                try {
                    type = Input.asCalculated(
                        FormatType.fromValue(Files.getFileExtension(source.getName()))
                    );
                } catch (Exception e) {
                    type = Input.asInput(FormatType.UNKNOWN_FORMAT);
                }
            }

            if (source.isFile()
                && (type.get() == FormatType.DEFINE_SPEC || type.get() == FormatType.EXCEL)) {
                // source is Excel file -> switch to define.xml converter
                return RequestType.DEFINE;
            } else {
                // switch to data converter
                return RequestType.CONVERTER;
            }
        }

        throw new CLIException(
            SystemExceptions.FAILED_REQUEST_TYPE,
            String.format("Failed to determine request type form args=%s", args.getInput())
        );
    }

    public static DataSourceInfo getDefineSourceFile(ParseResults results) {
        CliParameters arguments = results.getCliParameters();

        Input<File> defineFile = arguments.getDefineFile();
        if (!defineFile.isProvided() || isDirectory(defineFile)) {
            return DataSourceInfo.builder().build();
        }

        return DataSourceInfo.builder()
            .setDataSource(new FileDS(defineFile.get(), false))
            .setFormat(FormatType.DEFINE_XML)
            .build();
    }

    public static ConfigInfo getConfigInfo(ParseResults results, DataPackage dataPackage) {
        return getConfigInfo(results, dataPackage, false);
    }

    public static ConfigInfo getConfigInfo(ParseResults results, DataPackage dataPackage, boolean create) {
        ConfigInfo.Builder builder = ConfigInfo.Builder.builder()
            .setDataPackage(dataPackage);

        CliParameters cliParameters = results.getCliParameters();
        Input<File> commandLineFile = cliParameters.getProcessConfig();

        if (commandLineFile.isProvided()) {
            // we got config file on command line with full path.
            return builder
                .setConfig(toDataSource(commandLineFile.get(), false))
                .build();
        }

        Config config = results.getConfig();
        if (config.hasPath("cli.config")) {
            File path = new File(config.getString("cli.config"));
            if (path.exists() && path.isDirectory()) {
                LOGGER.info("Using config in folder={}",
                    path.getAbsolutePath());
                return builder
                    .setConfig(new FolderDS(path, false))
                    .build();
            }

            if (path.exists()) {
                // cli config provides config for validator
                LOGGER.info("Using config file {} from CLI configuration", path.getAbsolutePath());
                return builder.setConfig(new FileDS(path, false)).build();
            }
        }

        DataSource configDir = lookupDir("config", create);
        if (configDir == DataSource.EMPTY) {
            // try configs
            configDir = lookupDir("configs", create);
        }

        return builder.setConfig(configDir).build();
    }

    private static DataSource getDefineFile(Input<File> defineFile, Input<Boolean> cleanup) {
        if (!defineFile.isProvided()) {
            return DataSource.EMPTY;
        }

        if (isDirectory(defineFile)) {
            defineFile = Input.asInput(new File(defineFile.get().getAbsolutePath(), "define.xml"));
        }

        if (!defineFile.get().exists()) {
            throw new CLIException(
                SystemExceptions.EXISTS_DEFINE_FILE,
                String.format("Cannot find define file=%s", defineFile.get().getAbsolutePath())
            );
        }

        return new FileDS(defineFile.get(), cleanup.get());
    }

    public static StandardRequestBuilder resolve(StandardRequestBuilder stdBuilder) {
        if (stdBuilder == null || stdBuilder.getParseResults() == null) {
            throw new CLIException("Malformed standard request builder");
        }
        ParseResults results = stdBuilder.getParseResults();
        resolveParams(stdBuilder.getDataPackageBuilder(), results);

        return stdBuilder;
    }

    private static void resolveParams(DataPackage.Builder bld, ParseResults parseRes) {
        if (bld == null) {
            bld = DataPackage.Builder.builder();
        }

        CliParameters cliParameters = parseRes.getCliParameters();
        Config config = parseRes.getConfig();

        Input<File> engineFolder = cliParameters.getEngineFolder();
        if (!engineFolder.isProvided() &&
            config.hasPath(Constants.ENGINE_FOLDER)) {
            engineFolder = Input.asInput(new File(config.getString(Constants.ENGINE_FOLDER)));
        }

        bld.setEngineFolder(engineFolder);
    }

    public static StandardRequestBuilder resolve(StandardRequestBuilder builder,
                                                 ParseResults parseResults) {
        CliParameters arguments = parseResults.getCliParameters();
        if (arguments.listConfiguration().get()) {
            // list configuration. no data source
            return (StandardRequestBuilder) builder.setParseResults(parseResults);
        }

        final Input<Boolean> cleanup = arguments.getSourceCleanup();
        Input<StandardType> standardType = arguments.getStandard();

        Input<FormatType> sourceFormatType = arguments.getSourceFormat();
        DataSource dataSource = null;
        Input<Boolean> upload = Input.or(arguments.isUpload(),
            isUploadSource(arguments));

        DataSource defineSource = getDefineFile(arguments.getDefineFile(),
            arguments.getSourceCleanup());
        ImmutableList.Builder<String> sources = ImmutableList.builder();

        DataSources.DataSourceContext cxt = DataSources.DataSourceContext.context()
            .withUpload(upload.get())
            .withCleanup(cleanup.get());

        if (hasSource(StandardFamily.SDTM, arguments) &&
            !hasSource(StandardFamily.ADaM, arguments)) {
            standardType = standardType.withCalculated(StandardType.SDTMIG);

            addDefine(defineSource, sources);

            dataSource = toDataSource(sources
                .addAll(arguments.getSources(standardType.get().getFamily()).get())
                .build(), cxt.withType(standardType.get()));
        } else if (hasSource(StandardFamily.ADaM, arguments)) {
            // adam validation
            standardType = standardType.withCalculated(StandardType.ADaMIG);
            sources.addAll(arguments.getSources(standardType.get().getFamily()).get());

            addDefine(defineSource, sources);

            if (hasSource(StandardFamily.SDTM, arguments)) {
                // SDTM also provided. Need to combine with ADaM.
                /*
                 * TIP: When validating ADaM data don't forget to include AE, DM, and EX domains
                 * for traceability checks between ADaM and SDTM data.
                 */
                sources.addAll(arguments.getSources(StandardFamily.SDTM).get());
            }
            dataSource = toDataSource(sources.build(), cxt.withType(standardType.get()));
        } else if (hasSource(StandardFamily.SEND, arguments)) {
            standardType = standardType.withCalculated(StandardType.SENDIG);
            addDefine(defineSource, sources);
            dataSource = toDataSource(sources
                .addAll(arguments.getSources(standardType.get().getFamily()).get())
                .build(), cxt.withType(standardType.get()));
        } else if (arguments.getDefineFile().isProvided() &&
            !hasSource(null, arguments)) {
            // only define is provided then run define validation
            dataSource = getDefineFile(arguments.getDefineFile(), cleanup);
            sourceFormatType = Input.asCalculated(FormatType.DEFINE_XML);
        } else if (hasSource(null, arguments)) {
            dataSource = toDataSource(arguments.getSources().get(),
                cxt.withType(standardType.get()).withUpload(false));
        }

        if (dataSource == null) {
            throw new CLIException(ValidationExceptions.INVALID_DATA_SOURCE,
                String.format("Data source is not provided for validator. args=%s",
                    parseResults.getInput()));
        }

        DelimitedInfo sourceDelimited = arguments.getDelimitedInfo();

        Input<StandardFamily> standardFamily = Input.asDefault(StandardFamily.UNDEFINED);
        if (!standardType.isEmpty()) {
            standardFamily = Input.asCalculated(standardType.get().getFamily());
        }

        return (StandardRequestBuilder) builder
            .setDataSource(dataSource)
            .setSourceFormat(sourceFormatType)
            .setDelimiter(sourceDelimited.getDelimiter())
            .setQualifier(sourceDelimited.getQualifier())
            .setStandardType(standardType)
            .setStandardFamily(standardFamily)
            .setParseResults(parseResults);
    }

    private static void addDefine(DataSource defineSource, ImmutableList.Builder<String> sources) {
        if (isValid(defineSource) && COMBINABLE.contains(defineSource.getSchema())) {
            sources.add(defineSource.getAbsolutePath());
        }
    }

    static boolean hasSource(StandardFamily family, CliParameters params) {
        return params != null && !params.getSources(family).get().isEmpty();
    }

    static boolean hasSource(CliParameters params) {
        for (StandardFamily family : StandardFamily.values()) {
            if (family != StandardFamily.UNDEFINED &&
                hasSource(family, params)) {
                return true;
            }
        }

        return hasSource(null, params);
    }

    private static Input<Boolean> isUploadSource(CliParameters params) {
        List<Schema> schemas = Lists.newArrayList();
        Function<String, Schema> transformFunc = new Function<String, Schema>() {
            @Nullable
            @Override
            public Schema apply(@Nullable String path) {
                return Schema.fromPath(path);
            }
        };

        for (StandardFamily family : StandardFamily.values()) {
            if (family != StandardFamily.UNDEFINED && hasSource(family, params)) {
                schemas.addAll(FluentIterable
                    .from(params.getSources(family).get())
                    .transform(transformFunc)
                    .toList());
            }
        }

        if (hasSource(null, params)) {
            schemas.addAll(FluentIterable
                .from(params.getSources().get())
                .transform(transformFunc)
                .toList());
        }

        for (Schema schema : schemas) {
            if (schema.isUpload()) {
                return Input.asBool(true);
            }
        }

        return Input.asBool(false);
    }
}

