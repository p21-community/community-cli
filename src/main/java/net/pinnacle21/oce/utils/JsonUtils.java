package net.pinnacle21.oce.utils;

import com.fasterxml.jackson.databind.JsonNode;

public class JsonUtils {
    private boolean shouldUseEmptyStringForNull = false;

    JsonUtils() {
    }

    JsonUtils(boolean shouldUseEmptyStringForNull) {
        this.shouldUseEmptyStringForNull = shouldUseEmptyStringForNull;
    }

    public void useEmptyStringForNull(boolean shouldUseEmptyStringForNull) {
        this.shouldUseEmptyStringForNull = shouldUseEmptyStringForNull;
    }

    public String safeGetText(JsonNode json, String field) {
        if (json == null || field == null) {
            return shouldUseEmptyStringForNull ? "" : null;
        }
        JsonNode fieldNode = json.get(field);
        if (fieldNode != null) {
            String fieldText = fieldNode.asText();
            if (fieldText != null) {
                return fieldText;
            }
        }
        return shouldUseEmptyStringForNull ? "" : null;
    }
}
