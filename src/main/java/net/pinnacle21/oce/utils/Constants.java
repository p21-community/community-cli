package net.pinnacle21.oce.utils;

public final class Constants {
    public static final String API_KEY = "api.key";
    public static final String WEB_HOST = "web.host";
    public static final String CREATE_ALWAYS = "create.always";
    public static final String ENABLE_BC = "enableBC";

    public static final String S3_ROOT = "s3.root";

    public static final String THREAD_COUNT = "engine.threadcount";
    public static final String ENGINE_VERSION = "engine.version";
    public static final String ENGINE_FOLDER = "engine.folder";
    public static final String GENERATE_RULE_METRICS = "engine.generateRuleMetrics";
    public static final String USE_ENHANCED_SAS = "engine.useEnhancedSas";
    public static final String AUTO_DISPLAY_DOMAIN_KEYS = "engine.autoDisplayDomainKeys";

    public static final boolean DEFAULT_GENERATE_RULE_METRICS = true;
    public static final boolean DEFAULT_USE_ENHANCED_SAS = true;
    public static final boolean DEFAULT_AUTO_DISPLAY_DOMAIN_KEYS = true;

    public static final Integer THREAD_COUNT_DEFAULT = 3;
    public static final String COMMUNITY_MODE = "COMMUNITY_MODE";
    public static final String COMMUNITY_MODE_URL = "http://localhost:9000";
    public static final int DEFAULT_CUTOFF = 1000;

    public static final String DEFAULT_PROJECT = "enterprise_cli";
    public static final String DEFAULT_STUDY = "enterprise_cli";
    public static final String DEFAULT_GROUP = "enterprise_cli";
    public static final String CONF_FILE = "pinnacle21.conf";

    public static final String LOG_FILE_PATH = "cli.log.file";
    public static final String LOG_LEVEL_PROPERTY = "cli.log.level";

    public static final String DOWNLOAD_CONFIG = "download.config";

    public static final String SYSTEM_ONLINE = "system.online";

    private Constants() {
    }
}
