package net.pinnacle21.oce.model.support;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import net.pinnacle21.oce.model.community.GenerateType;

import java.io.IOException;

public class GenerateTypeDeserializer extends JsonDeserializer<GenerateType> {
    @Override
    public GenerateType deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        return GenerateType.fromValue(parser.getValueAsString());
    }
}
