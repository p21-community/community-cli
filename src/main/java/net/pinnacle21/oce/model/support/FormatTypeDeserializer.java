package net.pinnacle21.oce.model.support;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import net.pinnacle21.oce.model.FormatType;

import java.io.IOException;

public class FormatTypeDeserializer extends JsonDeserializer<FormatType> {
    @Override
    public FormatType deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        return FormatType.fromValue(parser.getValueAsString());
    }
}
