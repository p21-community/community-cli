package net.pinnacle21.oce.model.support;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Maps;
import net.pinnacle21.oce.model.Dictionaries;
import net.pinnacle21.oce.model.Terminologies;
import net.pinnacle21.oce.model.Valuable;
import net.pinnacle21.oce.utils.Valuables;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

public class TerminologiesDeserializer extends JsonDeserializer<Map<Valuable, String>> {
    @Override
    public Map<Valuable, String> deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        JsonNode terminologies = parser.readValueAsTree();
        Iterator<Map.Entry<String, JsonNode>> fields = terminologies.fields();
        Map<Valuable, String> resultMap = Maps.newHashMap();
        if (fields == null) {
            return resultMap;
        }

        while (fields.hasNext()) {
            Map.Entry<String, JsonNode> entry = fields.next();
            Valuable key = Valuables.fromValue(Terminologies.values(), entry.getKey(), null, null);
            if (key == null) {
                key = Valuables.fromValue(Dictionaries.values(), entry.getKey(), null, null);
            }

            if (key != null) {
                resultMap.put(key, entry.getValue().textValue());
            }
        }

        return resultMap;
    }
}
