package net.pinnacle21.oce.model.support;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;
import net.pinnacle21.oce.exceptions.DataExceptions;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.datasource.Schema;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.List;

import static net.pinnacle21.oce.utils.CliPreconditions.checkProvided;

public class DataSourcesDeserializer extends JsonDeserializer<List<DataSource>> {
    @Override
    public List<DataSource> deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        ArrayNode ds = parser.readValueAsTree();
        return ImmutableList.copyOf(Iterators.transform(ds.elements(), new Function<JsonNode, DataSource>() {
            @Nullable
            @Override
            public DataSource apply(@Nullable JsonNode item) {
                checkProvided(item, DataExceptions.PARSE_DATA_SOURCE,
                    "Failed to deserialize data source. Cannot be null");

                return Schema.toDataSource(item.get("absolutePath").asText(),
                    item.get("cleanup").asBoolean());
            }
        }));

    }
}
