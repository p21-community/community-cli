package net.pinnacle21.oce.model.support;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import net.pinnacle21.oce.model.community.Action;

import java.io.IOException;

public class ActionDeserializer extends JsonDeserializer<Action> {
    @Override
    public Action deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        return Action.fromValue(parser.getValueAsString());
    }
}
