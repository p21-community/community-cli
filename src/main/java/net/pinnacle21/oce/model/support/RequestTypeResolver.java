package net.pinnacle21.oce.model.support;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DatabindContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;
import net.pinnacle21.oce.model.request.Request;
import net.pinnacle21.oce.model.request.RequestType;

import java.io.IOException;

public class RequestTypeResolver implements TypeIdResolver {
    private JavaType type;

    @Override
    public void init(JavaType type) {
        this.type = type;
    }

    @Override
    public String idFromValue(Object value) {
        return idFromValueAndType(value, value.getClass());
    }

    @Override
    public String idFromValueAndType(Object value, Class<?> suggestedType) {
        if (value instanceof Request) {
            return ((Request) value).getType().name();
        }

        throw new IllegalArgumentException("Value is not a Request object");
    }

    @Override
    public String idFromBaseType() {
        return RequestType.fromClass(this.type.getClass()).getValue();
    }

    @Override
    public JavaType typeFromId(DatabindContext cxt, String id) throws IOException {
        RequestType requestType = RequestType.fromValue(id);

        return cxt.constructSpecializedType(this.type,
            requestType.getRequestClass());
    }

    @Override
    public String getDescForKnownTypeIds() {
        return RequestType.report();
    }

    @Override
    public JsonTypeInfo.Id getMechanism() {
        return JsonTypeInfo.Id.CUSTOM;
    }
}
