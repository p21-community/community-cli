package net.pinnacle21.oce.model.support;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import net.pinnacle21.oce.model.community.Action;

import java.io.IOException;

public class ActionSerializer extends JsonSerializer<Action> {

    @Override
    public void serialize(Action value, JsonGenerator gen,
                          SerializerProvider serializers) throws IOException, JsonProcessingException {
        gen.writeString(value.getValue());
    }
}
