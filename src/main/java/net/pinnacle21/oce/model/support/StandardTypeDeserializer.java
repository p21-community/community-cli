package net.pinnacle21.oce.model.support;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import net.pinnacle21.oce.model.StandardType;

import java.io.IOException;

public class StandardTypeDeserializer extends JsonDeserializer<StandardType> {
    @Override
    public StandardType deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        return StandardType.fromValue(parser.getValueAsString());
    }
}
