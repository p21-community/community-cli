package net.pinnacle21.oce.model.support;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DatabindContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;
import net.pinnacle21.oce.model.response.Response;
import net.pinnacle21.oce.model.response.ResponseType;

import java.io.IOException;

public class ResponseTypeResolver implements TypeIdResolver {
    private JavaType type;

    @Override
    public void init(JavaType type) {
        this.type = type;
    }

    @Override
    public String idFromValue(Object value) {
        return idFromValueAndType(value, value.getClass());
    }

    @Override
    public String idFromValueAndType(Object value, Class<?> suggestedType) {
        if (value instanceof Response) {
            return ((Response) value).getType().name();
        }

        throw new IllegalArgumentException("Value is not a Response object");
    }

    @Override
    public String idFromBaseType() {
        return ResponseType.fromClass(this.type.getClass()).getValue();
    }

    @Override
    public JavaType typeFromId(DatabindContext cxt, String id) throws IOException {
        ResponseType responseType = ResponseType.fromValue(id);

        return cxt.constructSpecializedType(this.type,
            responseType.getResponseClass());
    }

    @Override
    public String getDescForKnownTypeIds() {
        return ResponseType.report();
    }

    @Override
    public JsonTypeInfo.Id getMechanism() {
        return JsonTypeInfo.Id.CUSTOM;
    }
}
