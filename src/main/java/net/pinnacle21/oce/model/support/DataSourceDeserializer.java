package net.pinnacle21.oce.model.support;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.datasource.Schema;

import java.io.IOException;

public class DataSourceDeserializer extends JsonDeserializer<DataSource> {
    @Override
    public DataSource deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        ObjectNode ds = parser.readValueAsTree();
        return Schema.toDataSource(ds.get("absolutePath").asText(),
            ds.get("cleanup").asBoolean());
    }
}
