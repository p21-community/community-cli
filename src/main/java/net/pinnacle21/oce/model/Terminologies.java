package net.pinnacle21.oce.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import net.bootstrap.api.validator.ValidatorOptions;
import net.bootstrap.core.model.CoreNames;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.DataExceptions;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datapackage.DataPackageGetter;
import net.pinnacle21.oce.model.datapackage.DataPackageSetter;
import net.pinnacle21.oce.model.datapackage.MetadataMapper;
import net.pinnacle21.oce.model.input.Input;
import net.pinnacle21.oce.utils.Valuables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

public enum Terminologies implements MetadataMapper {
    ADaM_CT("ADaM-CT", new DataPackageGetter() {
        @Override
        public Input<String> get(DataPackage dataPackage) {
            return dataPackage.getAdamCtVersion();
        }
    }, new DataPackageSetter() {
        @Override
        public DataPackage.Builder set(Input<String> value, DataPackage.Builder builder) {
            return builder.setAdamCtVersion(value);
        }
    }, EnumSet.of(StandardFamily.ADaM),
        String.format(CoreNames.CDISC_NAME, StandardFamily.ADaM.getValue())),
    SDTM_CT("SDTM-CT", new DataPackageGetter() {
        @Override
        public Input<String> get(DataPackage dataPackage) {
            return dataPackage.getSdtmCtVersion();
        }
    }, new DataPackageSetter() {
        @Override
        public DataPackage.Builder set(Input<String> value, DataPackage.Builder builder) {
            return builder.setSdtmCtVersion(value);
        }
    }, EnumSet.of(StandardFamily.ADaM, StandardFamily.SDTM),
        String.format(CoreNames.CDISC_NAME, StandardFamily.SDTM.getValue())),
    SEND_CT("SEND-CT", new DataPackageGetter() {
        @Override
        public Input<String> get(DataPackage dataPackage) {
            return dataPackage.getSendCtVersion();
        }
    }, new DataPackageSetter() {
        @Override
        public DataPackage.Builder set(Input<String> value, DataPackage.Builder builder) {
            return builder.setSendCtVersion(value);
        }
    }, EnumSet.of(StandardFamily.SEND),
        String.format(CoreNames.CDISC_NAME, StandardFamily.SEND.getValue()));

    private static final Logger LOGGER = LoggerFactory.getLogger(Terminologies.class);
    private static final Pattern CT_REMOVAL = Pattern.compile("-CT");

    private final String value;
    private final DataPackageGetter versionGetter;
    private final DataPackageSetter versionSetter;
    private final Set<StandardFamily> standards;
    private final Set<String> allValues = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

    Terminologies(String value,
                  DataPackageGetter versionGetter,
                  DataPackageSetter versionSetter,
                  Set<StandardFamily> standards,
                  String... values) {
        this.value = value;
        this.versionGetter = versionGetter;
        this.versionSetter = versionSetter;
        this.standards = standards;
        if (values != null && values.length > 0) {
            this.allValues.addAll(ImmutableList.copyOf(values));
        }
    }

    public static Terminologies fromValue(String value) {
        return Valuables.fromValue(values(), value,
            DataExceptions.NOTFOUND_TERMINOLOGY);
    }

    public static Terminologies fromStandardFamily(StandardFamily family) {
        if (!StandardFamily.isValid(family)) {
            LOGGER.warn("Standard family {} is null or invalid. Terminology cannot be determined.", family);
            return null;
        }

        for (Terminologies terminology : values()) {
            StandardFamily stdFamily = terminology.toStandardFamily();
            if (stdFamily == family) {
                return terminology;
            }
        }

        LOGGER.warn("No terminologies found for standard family {}", family);
        return null;
    }

    private static String stripCT(String value) {
        return CT_REMOVAL.matcher(value).replaceAll("");
    }

    public static MetadataMapper toMapper(String value) {
        try {
            return fromValue(value);
        } catch (CLIException ex) {
            if (ex.getCode() == DataExceptions.NOTFOUND_TERMINOLOGY) {
                return null;
            }
            throw ex;
        }
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public boolean isValue(String value) {
        return this.getValue().equalsIgnoreCase(value) ||
            this.name().equalsIgnoreCase(value) ||
            this.allValues.contains(value);
    }

    public StandardFamily toStandardFamily() {
        return StandardFamily.fromValue(stripCT(this.value));
    }

    @Override
    public Input<String> getVersion(DataPackage dataPackage) {
        return this.versionGetter.get(dataPackage);
    }

    @Override
    public DataPackage.Builder setVersion(Input<String> version, DataPackage.Builder builder) {
        return this.versionSetter.set(version, builder);
    }

    @Override
    public Set<String> convertFrom() {
        return ImmutableSet.of();
    }

    @Override
    public boolean canDownload() {
        return true;
    }

    @Override
    public boolean isNumeric() {
        return false;
    }

    /**
     * Remove -CT  postfix to match terminology folder name. For example SEND-CT to SEND
     *
     * @return terminology folder name
     */
    @Override
    public String toFolderName() {
        return "CDISC";
    }

    @Override
    public String toFileName() {
        return stripCT(this.value);
    }

    public boolean containStandard(StandardFamily family) {
        return standards.contains(family);
    }
}
