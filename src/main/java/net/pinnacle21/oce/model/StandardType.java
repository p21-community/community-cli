package net.pinnacle21.oce.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import joptsimple.ValueConverter;
import net.bootstrap.core.events.EventListener;
import net.bootstrap.core.model.ConfigEvent;
import net.bootstrap.core.utils.config.Parser;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.DataExceptions;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datapackage.MetadataMapper;
import net.pinnacle21.oce.model.input.Input;
import net.pinnacle21.oce.utils.DynaEnum;
import net.pinnacle21.oce.utils.Valuables;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

import static net.bootstrap.core.utils.config.Parser.UNKNOWN_VALUE;
import static net.pinnacle21.oce.utils.CliPreconditions.checkProvided;

public class StandardType extends DynaEnum<StandardType> implements MetadataMapper {
    public static final StandardType SDTMIG = new StandardType(StandardFamily.SDTM,
        "SDTM-IG", "SDTMIG", "SDTM");
    public static final StandardType SDTMIG_MD = new StandardType(StandardFamily.SDTM,
        "SDTMIG-MD", "SDTMIG_MD", "SDTM_MD", "SDTM MD");
    public static final StandardType SDTMIG_AP = new StandardType(StandardFamily.SDTM,
        "SDTMIG-AP", "SDTMIG_AP", "SDTM-AP", "SDTM AP");
    public static final StandardType SDTMIG_PGx = new StandardType(StandardFamily.SDTM,
        "SDTMIG-PGx", "SDTMIG_PGx", "SDTM-PGx", "SDTM PGx");
    public static final StandardType SENDIG = new StandardType(StandardFamily.SEND,
        "SEND-IG", "SENDIG", "SEND");
    public static final StandardType ADaMIG = new StandardType(StandardFamily.ADaM,
        "ADaM-IG", "ADaMIG", "ADaM", "ADaM IG");
    public static final StandardType DEFINE = new StandardType(StandardFamily.DEFINE,
        "Define", "DEFINE", "Define.xml");
    public static final StandardType UNDEFINED = new StandardType(StandardFamily.UNDEFINED,
        "Undefined", "UNDEFINED");
    private static final Logger LOGGER = LoggerFactory.getLogger(StandardType.class);
    private final StandardFamily family;
    private final String value;
    private final Set<String> allValues = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
    private final Pattern IG_PATTERN = Pattern.compile("IG");
    private final Pattern DASH_PATTERN = Pattern.compile("-");

    private StandardType(StandardFamily family, String... values) {
        super(values[1]);

        this.family = family;
        this.value = values[0];
        this.allValues.addAll(ImmutableList.copyOf(values));
    }

    public static Standard fromString(final String release) {
        class StdListener implements EventListener<ConfigEvent> {
            private Standard standard;

            @Override
            public void pushEvent(ConfigEvent event) {
                if (event.isUnknown()) {
                    standard = new Standard(
                        StandardType.UNDEFINED, event.getVersion(), RuleFilter.ALL
                    );
                } else {
                    StandardType type = StandardType.UNDEFINED;

                    try {
                        type = StandardType.fromValue(event.getStandard());
                        if (type.getFamily() == StandardFamily.UNDEFINED) {
                            standard = new Standard(
                                StandardType.fromValue(release), UNKNOWN_VALUE, RuleFilter.ALL
                            );
                            return;
                        }
                    } catch (Exception e) {
                        LOGGER.error("Failed to get standard type '{}'", event.getStandard(), e);
                    }

                    standard = new Standard(
                        type, event.getVersion(), RuleFilter.fromValue(event.getFilter())
                    );
                }
            }
        }
        ;

        StdListener listener = new StdListener();
        Parser.parse(release, listener);


        return listener.standard;
    }

    public static StandardType fromValue(String value) {
        if (StringUtils.isBlank(value)) {
            throw new CLIException(DataExceptions.UNKNOWN_STANDARD, "Standard cannot be blank");
        }

        try {
            return Valuables.fromValue((StandardType[]) values(), value,
                DataExceptions.UNKNOWN_STANDARD);
        } catch (CLIException cliEx) {
            if (cliEx.getCode() == DataExceptions.UNKNOWN_STANDARD) {
                LOGGER.info("Adding new custom standard type {}", value);
                StandardFamily family = StandardFamily.fromValue(value);
                return new StandardType(family, value, value);
            }
            throw cliEx;
        }
    }

    public static DynaEnum<? extends DynaEnum<?>>[] values() {
        return values(StandardType.class);
    }

    public static ValueConverter<StandardType> converter() {
        return new ValueConverter<StandardType>() {
            @Override
            public StandardType convert(String value) {
                return StandardType.fromValue(Strings.nullToEmpty(value).trim());
            }

            @Override
            public Class<? extends StandardType> valueType() {
                return StandardType.class;
            }

            @Override
            public String valuePattern() {
                return null;
            }
        };
    }

    public static boolean isValid(Input<StandardType> standardType) {
        return standardType.get() != null &&
            standardType.get() != StandardType.UNDEFINED;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public boolean isValue(String value) {
        return this.allValues.contains(value) ||
            this.name().equalsIgnoreCase(value);
    }

    public StandardFamily getFamily() {
        return this.family;
    }

    @Override
    public boolean canDownload() {
        return true;
    }

    /**
     * transform standard type value to file name basis by removing IG and repleacing dash '-' with space
     * for exampl SDTMIG-MD should become SDTM MD
     * For Define append .xml to the value
     *
     * @return file name base
     */
    @Override
    public String toFileName() {
        String fileName = this.value.trim();
        if (getFamily() == StandardFamily.DEFINE) {
            fileName += ".xml";
        }

        return fileName;
    }

    @Override
    public Input<String> getVersion(DataPackage dataPackage) {
        return dataPackage.getStandardVersion();
    }

    @Override
    public DataPackage.Builder setVersion(Input<String> version, DataPackage.Builder builder) {
        return builder.setStandardVersion(version);
    }

    @Override
    public Set<String> convertFrom() {
        return ImmutableSet.of();
    }

    @Override
    public boolean isNumeric() {
        return false;
    }

    @Override
    public String toFolderName() {
        return "";
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("value", value)
            .add("name", name())
            .toString();
    }

    public static class Standard {
        private final StandardType type;
        private final String version;
        private final RuleFilter filter;

        public Standard(StandardType type, String version, RuleFilter filter) {
            this.type = type;
            this.version = version;
            this.filter = filter;
        }

        public StandardType getType() {
            return type;
        }

        public String getVersion() {
            return version;
        }

        public RuleFilter getFilter() {
            return filter;
        }

        public String toName() {
            checkProvided(type,
                "Failed to determine standard file name from data package. Standard Type cannot be null");

            StringBuilder fileNameBuilder = new StringBuilder(type.toFileName());
            if (Strings.emptyToNull(this.version) != null &&
                !UNKNOWN_VALUE.equalsIgnoreCase(this.version)) {
                fileNameBuilder.append(' ').append(this.version);
            }

            if (this.filter != null &&
                this.filter != RuleFilter.ALL) {
                fileNameBuilder.append(" (").append(this.filter.getValue()).append(')');
            }

            return fileNameBuilder.append(".xml").toString();
        }
    }
}
