package net.pinnacle21.oce.model.datasource;

import com.google.common.base.MoreObjects;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Folder data source
 */
public class StdSource extends AbstractDataSource {
    private final StandardInputOutput sourceType;

    public StdSource(StandardInputOutput sourceType) {
        this.sourceType = sourceType;
    }

    @Override
    public boolean isValid() {
        return this.sourceType != null;
    }

    @Override
    public String getAbsolutePath() {
        return this.sourceType.getValue();
    }

    @Override
    public Schema getSchema() {
        return Schema.STD;
    }

    @Override
    public boolean isCleanup() {
        // never cleanup enterprise data
        return false;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                System.out.write(b);
            }
        };
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new InputStream() {
            @Override
            public int read() throws IOException {
                return System.in.read();
            }
        };
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
            .add("standard type", this.sourceType.getValue())
            .add("schema", getSchema())
            .toString();
    }
}
