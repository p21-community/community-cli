package net.pinnacle21.oce.model.datasource;

import com.google.common.base.MoreObjects;
import org.apache.commons.io.input.NullInputStream;
import org.apache.commons.io.output.NullOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Data source interfacr
 */
public interface DataSource {
    static DataSource EMPTY = new Empty();

    /**
     * validates data source
     *
     * @return
     */
    boolean isValid();

    /**
     * data source absolute path
     *
     * @return
     */
    String getAbsolutePath();

    /**
     * get datasource schema
     *
     * @return
     */
    Schema getSchema();

    /**
     * data source should be cleaned up on exit
     *
     * @return
     */
    boolean isCleanup();

    int getCount();

    OutputStream getOutputStream() throws IOException;

    InputStream getInputStream() throws IOException;

    class Empty implements DataSource {
        @Override
        public boolean isValid() {
            return true;
        }

        @Override
        public String getAbsolutePath() {
            return "";
        }

        @Override
        public Schema getSchema() {
            return Schema.EMPTY;
        }

        @Override
        public boolean isCleanup() {
            return false;
        }

        @Override
        public int getCount() {
            return 0;
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            return new NullOutputStream();
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return new NullInputStream(0);
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(getClass())
                .add("absolutePath", "EMPTY")
                .toString();
        }
    }
}
