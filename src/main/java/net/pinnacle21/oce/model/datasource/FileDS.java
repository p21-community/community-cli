package net.pinnacle21.oce.model.datasource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * Folder data source
 */
public class FileDS extends AbstractFileDS {
    private final Logger LOGGER = LoggerFactory.getLogger(FileDS.class);

    public FileDS(String fileName, boolean cleanup) {
        super(fileName, cleanup);
    }

    public FileDS(File file, boolean cleanup) {
        super(file, cleanup);
    }

    @Override
    public boolean isValid() {
        return this.file != null && this.file.isFile();
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return new FileOutputStream(getFile());
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new FileInputStream(getFile());
    }
}
