package net.pinnacle21.oce.model.datasource;


import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import org.apache.commons.io.output.NullOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import static net.pinnacle21.oce.utils.CliPreconditions.checkCollection;

public class MultiSource implements DataSource {
    private final String path;
    private Collection<DataSource> sources;

    public MultiSource(String path) {
        this(path, null);
    }

    public MultiSource(Collection<DataSource> sources) {
        this("sources", sources);
    }

    public MultiSource(String path, Collection<DataSource> sources) {
        int location = path.indexOf(Schema.SCHEMA_DELIM);
        if (location < 1) {
            path = String.format("%s%s%s",
                Schema.MULTI_SOURCE.getSchema(), Schema.SCHEMA_DELIM, path);
        }

        this.path = path;
        this.sources = sources;
    }

    @Override
    public boolean isValid() {
        return !Strings.isNullOrEmpty(this.path)
            && sources != null && !sources.isEmpty();
    }

    @Override
    public String getAbsolutePath() {
        return this.path;
    }

    @Override
    public Schema getSchema() {
        return Schema.MULTI_SOURCE;
    }

    @Override
    public boolean isCleanup() {
        return false;
    }

    @Override
    public int getCount() {
        return sources != null ? sources.size() : 0;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return new NullOutputStream();
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new InputStream() {
            private LinkedList<InputStream> streams = new LinkedList<>();
            private Iterator<DataSource> current = sources.iterator();

            @Override
            public int read() throws IOException {
                if (streams.size() == 0 && current.hasNext()) {
                    streams.add(current.next().getInputStream());
                }
                int by = streams.getLast().read();
                if (by == -1 && current.hasNext()) {
                    // end of current source
                    streams.add(current.next().getInputStream());
                    by = streams.getLast().read();
                }

                return by;
            }

            @Override
            public void close() throws IOException {
                for (InputStream s : streams) {
                    s.close();
                }
                super.close();
            }
        };
    }

    public String getPath() {
        return path;
    }

    public Collection<DataSource> getSources() {
        return sources;
    }

    public void setSources(Collection<DataSource> sources) {
        this.sources = checkCollection(sources, ValidationExceptions.MISSING_DATA_SOURCE,
            "Sources cannot be null or empty in multi-source");
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
            .add("absolutePath", this.getAbsolutePath())
            .add("schema", this.getSchema())
            .add("count", this.getCount())
            .add("sources", Joiner.on('\n').join(this.sources))
            .toString();
    }
}
