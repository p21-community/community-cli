package net.pinnacle21.oce.model.datasource;

import java.io.File;

import static net.pinnacle21.oce.utils.DataSources.isDirectory;

/**
 * Folder data source
 */
public class FolderDS extends AbstractFileDS {
    FolderDS(String folderName, boolean cleanup) {
        super(folderName, cleanup);
    }

    public FolderDS(File file, boolean cleanup) {
        super(file, cleanup);
    }

    @Override
    public boolean isValid() {
        return this.file != null && isDirectory(this.file);
    }

    @Override
    public int getCount() {
        if (isValid()) {
            File[] files = this.file.listFiles();
            return files == null ? 0 : files.length;
        }
        return 0;
    }
}
