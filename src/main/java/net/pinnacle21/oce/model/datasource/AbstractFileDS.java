package net.pinnacle21.oce.model.datasource;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Sets;

import java.io.File;
import java.util.Set;

public abstract class AbstractFileDS extends AbstractDataSource {
    public static Set<AbstractFileDS> CLEANUP_SOURCES = Sets.newConcurrentHashSet();

    protected final File file;
    private final boolean cleanup;

    AbstractFileDS(String fileOrFolder, boolean cleanup) {
        this.file = Schema.toFile(fileOrFolder);
        this.cleanup = cleanup;
        register(this, cleanup);
    }

    AbstractFileDS(File file, boolean cleanup) {
        this.file = file;
        this.cleanup = cleanup;
        register(this, cleanup);
    }

    private static void register(AbstractFileDS ds, boolean cleanup) {
        if (ds != null && cleanup) {
            CLEANUP_SOURCES.add(ds);
        }
    }

    public File getFile() {
        return file;
    }

    @Override
    public String getAbsolutePath() {
        return this.file == null ? "" : this.file.getAbsolutePath();
    }

    @Override
    public Schema getSchema() {
        return Schema.FILE;
    }

    @Override
    public boolean isCleanup() {
        return cleanup;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
            .add("path", getAbsolutePath())
            .add("schema", getSchema())
            .add("cleanup", isCleanup())
            .add("valid", isValid())
            .toString();
    }
}
