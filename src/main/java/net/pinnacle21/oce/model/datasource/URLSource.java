package net.pinnacle21.oce.model.datasource;

import joptsimple.internal.Strings;

/**
 * Folder data source
 */
public class URLSource extends AbstractDataSource {
    private final String url;

    public URLSource(String url) {
        this.url = url;
    }

    @Override
    public boolean isValid() {
        return !Strings.isNullOrEmpty(this.url);
    }

    @Override
    public String getAbsolutePath() {
        return this.url;
    }

    @Override
    public Schema getSchema() {
        return Schema.URL;
    }

    @Override
    public boolean isCleanup() {
        return false;
    }
}
