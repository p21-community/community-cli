package net.pinnacle21.oce.model.datasource;

import net.pinnacle21.oce.exceptions.DataExceptions;
import net.pinnacle21.oce.model.Valuable;
import net.pinnacle21.oce.utils.Valuables;

public enum StandardInputOutput implements Valuable {
    IN, OUT;

    public static StandardInputOutput fromValue(String value) {
        return Valuables.fromValue(values(), value,
            DataExceptions.UNKNOWN_VALUE);
    }

    @Override
    public String getValue() {
        return this.name();
    }

    @Override
    public boolean isValue(String value) {
        return this.name().equalsIgnoreCase(value);
    }
}
