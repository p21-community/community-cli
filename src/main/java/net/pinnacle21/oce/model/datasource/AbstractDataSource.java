package net.pinnacle21.oce.model.datasource;


import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.apache.commons.io.input.NullInputStream;
import org.apache.commons.io.output.NullOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class AbstractDataSource implements DataSource {
    @Override
    public int hashCode() {
        if (getAbsolutePath() == null && getSchema() == null) {
            return 0;
        }

        if (getAbsolutePath() == null && getSchema() != null) {
            return getSchema().hashCode();
        }

        if (getAbsolutePath() != null && getSchema() == null) {
            return getAbsolutePath().hashCode();
        }

        return Objects.hashCode(getAbsolutePath(), getSchema());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof DataSource)) {
            return false;
        }

        DataSource lhs = (DataSource) obj;

        return getAbsolutePath() != null &&
            getAbsolutePath().equals(lhs.getAbsolutePath()) &&
            getSchema() == lhs.getSchema();
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return new NullOutputStream();
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new NullInputStream(0);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
            .add("absolutePath", this.getAbsolutePath())
            .add("schema", this.getSchema())
            .add("count", this.getCount())
            .toString();
    }

}
