package net.pinnacle21.oce.model.datasource;

import com.google.common.base.Strings;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.input.Input;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Set;
import java.util.TreeSet;

import static net.pinnacle21.oce.utils.DataSources.cleanupPath;
import static net.pinnacle21.oce.utils.DataSources.isDirectory;

/**
 * Schema definition enum
 */
public enum Schema {
    FILE("file", false),
    REMOTE("remote", true),
    ASPERA("aspera", false, true),
    ENTERPRISE("ent", true),
    MULTI_SOURCE("multi", false),
    SDD4("sdd4", true),
    LSAF("lsaf", true),
    STD("std", false, false),
    URL("url", true, false, "http", "https"),
    BROWSER("browser", false, false),
    EMPTY("empty", false);

    public static final String SCHEMA_DELIM = "://";
    private static final Logger LOG = LoggerFactory.getLogger(Schema.class);
    private static final Set<Schema> ENTERPRISE_SCHEMA = EnumSet.of(ASPERA, SDD4, LSAF);
    private static final Set<Schema> FILE_SCHEMA = EnumSet.of(ASPERA, FILE);
    private final String schema;
    private final boolean remote;
    private final boolean upload;
    private final Set<String> values = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

    Schema(String schema, boolean remote) {
        this(schema, remote, false);
    }

    Schema(String schema, boolean remote, boolean upload, String... values) {
        this.schema = schema;
        this.remote = remote;
        this.upload = upload;
        if (ArrayUtils.isNotEmpty(values)) {
            this.values.addAll(Arrays.asList(values));
        }

        this.values.add(schema);
    }

    public static Schema fromPath(String path) {
        if (Strings.isNullOrEmpty(path)) {
            throw new IllegalArgumentException("Cannot get schema from null or empty path");
        }
        path = cleanupPath(path);

        int location = path.indexOf(SCHEMA_DELIM);
        if (location < 1) {
            if (isStd(path)) {
                return STD;
            }

            return FILE;
        }

        String inSchema = path.substring(0, location).toLowerCase();
        return fromValue(inSchema);
    }

    private static boolean isStd(String path) {
        try {
            StandardInputOutput.fromValue(path);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    static Schema fromValue(String strSchema) {
        if (Strings.isNullOrEmpty(strSchema)) {
            return FILE;
        }

        for (Schema schema : values()) {
            if (schema.values.contains(strSchema)) {
                return schema;
            }
        }

        LOG.info("Schema '{}' is unknown. Return file as default", strSchema);
        return FILE;
    }

    static File toFile(String path) {
        if (Strings.isNullOrEmpty(path)) {
            throw new CLIException(ValidationExceptions.MISSING_DATA_SOURCE,
                "Cannot get file from null or empty path");
        }
        path = cleanupPath(path);

        int location = path.indexOf(SCHEMA_DELIM);
        if (location < 1) {
            return new File(path);
        }

        Schema schema = fromPath(path);
        if (schema == Schema.FILE ||
            schema == Schema.ASPERA) {
            return new File(path.substring(location + SCHEMA_DELIM.length()));
        } else {
            return null;
        }
    }

    static StandardInputOutput toStd(String path) {
        if (Strings.isNullOrEmpty(path)) {
            throw new CLIException(ValidationExceptions.MISSING_DATA_SOURCE,
                "Cannot get standard in/out from null or empty path");
        }
        path = cleanupPath(path);

        int location = path.indexOf(SCHEMA_DELIM);
        if (location < 1) {
            return StandardInputOutput.fromValue(path);
        }

        if (fromPath(path) == Schema.STD) {
            return StandardInputOutput.fromValue(path.substring(location + SCHEMA_DELIM.length()));
        } else {
            return null;
        }
    }

    public static String removeSchema(String path) {
        int location = path.indexOf(SCHEMA_DELIM);
        if (location < 1) {
            return path;
        }

        return path.substring(location + SCHEMA_DELIM.length());
    }

    public static DataSource toDataSource(String path, boolean cleanup) {
        return toDataSource(Input.asString(path), cleanup);
    }

    public static DataSource toDataSource(Input<String> path, boolean cleanup) {
        if (!path.isProvided()) {
            return DataSource.EMPTY;
        }

        Input<String> orgPath = path;
        path = Input.asString(cleanupPath(path.get()));

        Schema schema = fromPath(path.get());
        if (schema == Schema.URL) {
            // URL keep org path
            path = orgPath;
        }
        if (schema == Schema.FILE ||
            schema == Schema.ASPERA) {
            File f = toFile(path.get());
            if (f == null) {
                throw new CLIException(ValidationExceptions.MISSING_DATA_SOURCE,
                    String.format("Cannot get file path=%s", path));
            }
            if (isDirectory(f)) {
                return new FolderDS(f, cleanup);
            } else {
                return new FileDS(f, cleanup);
            }
        } else if (schema == Schema.STD) {
            return new StdSource(toStd(path.get()));
        } else if (schema == Schema.MULTI_SOURCE) {
            return new MultiSource(path.get());
        } else if (schema == Schema.EMPTY) {
            return DataSource.EMPTY;
        } else if (schema == Schema.URL) {
            return new URLSource(path.get());
        } else {
            return null;
        }
    }

    public String getSchema() {
        return this.schema;
    }

    public boolean isRemote() {
        return remote;
    }

    public boolean isEnterprise() {
        return ENTERPRISE_SCHEMA.contains(this);
    }

    public boolean isUpload() {
        return upload;
    }

    public boolean isFile() {
        return FILE_SCHEMA.contains(this);
    }
}
