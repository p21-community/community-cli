package net.pinnacle21.oce.model.response;

import com.google.common.base.MoreObjects;

public class ConverterResponse extends AbstractOutputResponse {
    private final String truncation;

    public ConverterResponse(String outputPath, String truncation) {
        super(outputPath);
        this.truncation = truncation;
    }

    public String getTruncation() {
        return truncation;
    }

    @Override
    public ResponseType getType() {
        return ResponseType.CONVERTER_RESPONSE;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("outputPath", getOutputPath())
            .add("truncation", truncation)
            .toString();
    }
}
