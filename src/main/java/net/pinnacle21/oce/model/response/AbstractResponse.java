package net.pinnacle21.oce.model.response;

public abstract class AbstractResponse implements Response {
    private String exception;

    @Override
    public String getException() {
        return exception;
    }

    @Override
    public void setException(String exception) {
        this.exception = exception;
    }
}
