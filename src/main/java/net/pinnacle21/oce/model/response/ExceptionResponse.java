package net.pinnacle21.oce.model.response;

import com.google.common.base.MoreObjects;
import net.pinnacle21.oce.exceptions.CLIExceptionCode;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.utils.CliErrorUtils;

public class ExceptionResponse extends AbstractResponse implements CLIExceptionCode {
    private static final String EMPTY = "";

    private byte code = CLIExceptionCode.GENERAL_EXCEPTION.getCode();
    private String message = CLIExceptionCode.GENERAL_EXCEPTION.getMessage();
    private String ref = CLIExceptionCode.GENERAL_EXCEPTION.getRef();
    private Throwable cause;

    public ExceptionResponse() {
    }

    public ExceptionResponse(String exception) {
        this.message = exception;
        super.setException(exception);
    }

    public ExceptionResponse(Throwable cause) {
        this(CliErrorUtils.getErrorCode(cause), cause);
    }

    public ExceptionResponse(CLIExceptionCode cliCode, Throwable cause) {
        this.cause = cause;
        this.code = cliCode.getCode();
        this.message = cliCode.getMessage();
        this.ref = cliCode.getRef();
        if (this.cause != null) {
            String msg = this.cause.getLocalizedMessage();
            if (cliCode == SystemExceptions.PERMISSION_DENIED) {
                msg = String.format("Permission error: %s::%s",
                    CliErrorUtils.getPermissionCause(this.cause), msg);
            }
            this.setException(CliErrorUtils.report(cliCode, msg));
        } else {
            this.setException(CliErrorUtils.report(cliCode, null));
        }
    }

    public ExceptionResponse(CLIExceptionCode cliCode) {
        this.code = cliCode.getCode();
        this.message = cliCode.getMessage();
        this.ref = cliCode.getRef();
    }

    public ExceptionResponse(String exception, Throwable cause) {
        this.message = exception;
        this.cause = cause;
        if (cause != null) {
            this.setException(cause.getMessage());
        }
    }

    @Override
    public ResponseType getType() {
        return ResponseType.EXCEPTION;
    }

    @Override
    public byte getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getRef() {
        return ref;
    }

    public Throwable getCause() {
        return cause;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("code", code)
            .add("message", message)
            .add("ref", ref)
            .add("cause", cause)
            .add("type", getType())
            .add("exception", getException())
            .toString();
    }
}
