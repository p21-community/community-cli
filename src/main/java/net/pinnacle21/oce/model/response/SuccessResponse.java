package net.pinnacle21.oce.model.response;

public class SuccessResponse extends AbstractOutputResponse {
    private final int exitCode;

    public SuccessResponse() {
        this(0, null);
    }

    public SuccessResponse(int exitCode, String outputPath) {
        super(outputPath);
        this.exitCode = exitCode;
    }

    public int getExitCode() {
        return exitCode;
    }

    @Override
    public ResponseType getType() {
        return ResponseType.SUCCESS;
    }

    @Override
    public String getException() {
        return null;
    }
}
