package net.pinnacle21.oce.model.response;

import com.google.common.base.MoreObjects;
import org.apache.commons.lang3.StringUtils;

public class ValidatorResponse extends AbstractOutputResponse {
    private int severity;
    private String reportUrl;

    public ValidatorResponse(String outputPath) {
        super(outputPath);
    }

    public ValidatorResponse(int severity, String outputPath) {
        super(outputPath);
        this.severity = severity;
    }

    public ValidatorResponse(int severity, String outputPath, String reportUrl) {
        super(outputPath);
        this.severity = severity;
        this.reportUrl = reportUrl;
    }

    public int getSeverity() {
        return severity;
    }

    public String getReportUrl() {
        return reportUrl;
    }

    @Override
    public ResponseType getType() {
        return ResponseType.VALIDATOR_RESPONSE;
    }

    @Override
    public String toString() {
        MoreObjects.ToStringHelper helper = MoreObjects.toStringHelper(getClass())
            .add("severity", this.severity)
            .add("outputPath", getOutputPath());
        if (StringUtils.isNotBlank(getReportUrl())) {
            helper.add("url", getReportUrl());
        }

        return helper.toString();
    }
}
