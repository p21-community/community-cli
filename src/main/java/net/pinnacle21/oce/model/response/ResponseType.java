package net.pinnacle21.oce.model.response;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public enum ResponseType {
    VALIDATOR_RESPONSE("validator", ValidatorResponse.class),
    CONVERTER_RESPONSE("converter", ConverterResponse.class),
    SUCCESS("success", SuccessResponse.class),
    EXCEPTION("exception", ExceptionResponse.class);

    private final String value;
    private final Class<? extends Response> responseClass;

    ResponseType(String value, Class<? extends Response> responseClass) {
        this.value = value;
        this.responseClass = responseClass;
    }

    public static ResponseType fromClass(Class<?> aClass) {
        for (ResponseType type : ResponseType.values()) {
            if (type.getResponseClass() == aClass) {
                return type;
            }
        }

        throw new IllegalArgumentException(String.format("Response class %s is not supported",
            aClass.getName()));
    }

    public static ResponseType fromValue(String value) {
        for (ResponseType type : ResponseType.values()) {
            if (type.getValue().equalsIgnoreCase(value) ||
                type.name().equalsIgnoreCase(value)) {
                return type;
            }
        }

        throw new IllegalArgumentException(String.format("Response type %s is not supported", value));
    }

    public static String report() {
        return Joiner.on(",")
            .join(
                Arrays.stream(values())
                    .map(requestType -> requestType.getResponseClass().getSimpleName())
                    .collect(Collectors.toList())
            );
    }

    public String getValue() {
        return value;
    }

    public Class<? extends Response> getResponseClass() {
        return this.responseClass;
    }

    @SuppressWarnings("unchecked")
    public <T extends Response> List<T> toResponses(Map<ResponseType, List<Response>> map) {
        return Optional.ofNullable(map.get(this)).orElse(ImmutableList.of()).stream()
            .map(input -> (T) getResponseClass().cast(input))
            .collect(Collectors.toList());
    }
}
