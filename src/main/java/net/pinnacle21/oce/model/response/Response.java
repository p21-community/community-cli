package net.pinnacle21.oce.model.response;

public interface Response {
    ResponseType getType();

    String getException();

    void setException(String exception);
}
