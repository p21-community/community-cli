package net.pinnacle21.oce.model.response;

public abstract class AbstractOutputResponse extends AbstractResponse {
    private final String outputPath;

    protected AbstractOutputResponse(String outputPath) {
        this.outputPath = outputPath;
    }

    public String getOutputPath() {
        return outputPath;
    }
}
