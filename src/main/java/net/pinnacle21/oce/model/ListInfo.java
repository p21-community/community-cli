package net.pinnacle21.oce.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.support.DataSourceDeserializer;

@JsonDeserialize(builder = ListInfo.Builder.class)
public class ListInfo extends InstallInfo {
    private final DataSource output;
    private final boolean loggedIn;

    private ListInfo(Builder builder) {
        super(InstallInfo.Builder.builder()
            .setDataPackage(builder.dataPackage)
            .setConfig(builder.config)
            .setClean(builder.clean));
        this.output = builder.output;
        this.loggedIn = builder.loggedIn;
    }

    public DataSource getOutput() {
        return output;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class Builder extends BaseModel.Builder<Builder, ListInfo> {
        private DataPackage dataPackage;
        private DataSource config;
        private boolean clean;
        private boolean loggedIn;

        @JsonDeserialize(using = DataSourceDeserializer.class)
        private DataSource output;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public static Builder from(ListInfo otherInfo) {
            return builder()
                .setDataPackage(otherInfo.getDataPackage())
                .setConfig(otherInfo.getConfig())
                .setOutput(otherInfo.getOutput());
        }

        @Override
        protected Builder getThis() {
            return this;
        }

        public Builder setDataPackage(DataPackage dataPackage) {
            this.dataPackage = dataPackage;
            return getThis();
        }

        public Builder setConfig(DataSource config) {
            this.config = config;
            return getThis();
        }

        public Builder setClean(boolean clean) {
            this.clean = clean;
            return getThis();
        }

        public Builder setOutput(DataSource output) {
            this.output = output;
            return getThis();
        }

        public Builder setLoggedIn(boolean isLoggedIn) {
            this.loggedIn = isLoggedIn;
            return getThis();
        }

        @Override
        public ListInfo build() {
            return new ListInfo(this);
        }
    }
}
