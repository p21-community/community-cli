package net.pinnacle21.oce.model;

/**
 * Base model for service data
 */

public abstract class BaseModel<T extends BaseModel> implements CliModel {
    public static abstract class Builder<B extends BaseModel.Builder, T extends BaseModel> {
        protected abstract B getThis();

        public abstract T build();
    }
}
