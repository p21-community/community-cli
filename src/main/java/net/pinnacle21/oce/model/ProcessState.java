package net.pinnacle21.oce.model;

import net.pinnacle21.oce.events.State;

public enum ProcessState implements State<ProcessState> {
    STARTED,
    ABORTED(true),
    TASK_PROCESSING,
    TASK_SUBPROCESSING,
    COMPLETED(true),
    EXCEPTION(true),
    TASK_COMPLETED;

    private final boolean completed;

    ProcessState() {
        this(false);
    }

    ProcessState(boolean completed) {
        this.completed = completed;
    }

    @Override
    public ProcessState getState() {
        return this;
    }

    @Override
    public boolean isCompleted() {
        return completed;
    }
}
