package net.pinnacle21.oce.model;

import com.google.common.base.Strings;
import joptsimple.ValueConverter;
import net.pinnacle21.oce.utils.Valuables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum RuleFilter implements Valuable {
    PMDA, FDA, ALL;

    private static final Logger LOGGER = LoggerFactory.getLogger(RuleFilter.class);

    public static RuleFilter fromValue(String value) {
        return Valuables.fromValue(values(), value, ALL);
    }

    public static ValueConverter<RuleFilter> converter() {
        return new ValueConverter<RuleFilter>() {
            @Override
            public RuleFilter convert(String value) {
                return RuleFilter.fromValue(Strings.nullToEmpty(value).trim());
            }

            @Override
            public Class<? extends RuleFilter> valueType() {
                return RuleFilter.class;
            }

            @Override
            public String valuePattern() {
                return null;
            }
        };
    }

    @Override
    public String getValue() {
        return this.name();
    }

    @Override
    public boolean isValue(String value) {
        return this.getValue().equalsIgnoreCase(value);
    }
}
