package net.pinnacle21.oce.model.request;

import net.pinnacle21.oce.model.datapackage.LocalDataPackage;

import java.util.List;

public class ValidatorRequest implements Request {
    private List<LocalDataPackage> packages;

    ValidatorRequest(List<LocalDataPackage> packages) {
        this.packages = packages;
    }

    public List<LocalDataPackage> getPackages() {
        return packages;
    }

    @Override
    public RequestType getType() {
        return RequestType.VALIDATOR;
    }
}
