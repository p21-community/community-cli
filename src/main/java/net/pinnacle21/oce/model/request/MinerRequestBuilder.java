package net.pinnacle21.oce.model.request;

import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.input.CliParameters;
import net.pinnacle21.oce.model.input.DataSourceInfo;
import net.pinnacle21.oce.utils.DataSources;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.pinnacle21.oce.utils.CliPreconditions.checkState;
import static net.pinnacle21.oce.utils.OutputUtils.getOutputFileOrFolder;

public class MinerRequestBuilder extends StandardRequestBuilder<MinerRequestBuilder, MinerRequest> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MinerRequestBuilder.class);

    private MinerRequestBuilder() {
    }

    public static MinerRequestBuilder builder() {
        return new MinerRequestBuilder();
    }

    @Override
    protected MinerRequestBuilder getThis() {
        return this;
    }

    @Override
    public MinerRequest build() {
        LOGGER.info("Build miner request");

        DataPackage.Builder builder = getDataPackageBuilder();
        DataPackage minerPackage = builder.build();
        CliParameters parameters = getParseResults().getCliParameters();
        DataSourceInfo output = getOutputFileOrFolder(this.getParseResults(), StringUtils.EMPTY);

        checkState(output.getDataSource() != DataSource.EMPTY,
            ValidationExceptions.REQUIRED_OUTPUT,
            String.format("Output is required for data miner. input=%s",
                this.getParseResults().getInput()));

        DataSources.makeDataSource(output.getDataSource());

        return new MinerRequest(
            minerPackage,
            parameters.getReportType().get(),
            parameters.getProcessConfig().get(),
            output.getDataSource()
        );
    }
}
