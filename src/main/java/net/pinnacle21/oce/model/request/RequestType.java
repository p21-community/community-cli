package net.pinnacle21.oce.model.request;

import com.google.common.base.Joiner;
import net.pinnacle21.oce.exceptions.DataExceptions;
import net.pinnacle21.oce.model.Valuable;
import net.pinnacle21.oce.model.input.ParseResults;
import net.pinnacle21.oce.utils.InputUtils;
import net.pinnacle21.oce.utils.Valuables;

import java.util.Arrays;
import java.util.stream.Collectors;

public enum RequestType implements Valuable {
    DEFINE("define", DefineRequest.class,
        (RequestFactory<DefineRequest>) argResults -> {
            return ((DefineRequestBuilder) InputUtils
                .resolve(InputUtils.resolve(DefineRequestBuilder.builder(), argResults)))
                .build();
        }),
    VALIDATOR("validator", ValidatorRequest.class,
        (RequestFactory<ValidatorRequest>) argResults -> {
            return ((ValidatorRequestBuilder) InputUtils
                .resolve(InputUtils.resolve(ValidatorRequestBuilder.builder(), argResults)))
                .build();
        }),
    CONVERTER("converter", ConverterRequest.class,
        (RequestFactory<ConverterRequest>) argResults -> {
            return ((ConverterRequestBuilder) InputUtils
                .resolve(InputUtils.resolve(ConverterRequestBuilder.builder(), argResults)))
                .build();
        }),
    MINER("miner", MinerRequest.class,
        (RequestFactory<MinerRequest>) argResults -> {
            return ((MinerRequestBuilder) InputUtils
                .resolve(InputUtils.resolve(MinerRequestBuilder.builder(), argResults)))
                .build();
        }),
    LIST_CONFIG("list", ListConfigRequest.class,
        (RequestFactory<ListConfigRequest>) argResults -> {
            return ((ListConfigRequestBuilder) InputUtils
                .resolve(InputUtils.resolve(ListConfigRequestBuilder.builder(), argResults)))
                .build();
        });

    private final String value;
    private final Class<? extends Request> requestClass;
    private final RequestFactory<? extends Request> requestFactory;

    RequestType(String value,
                Class<? extends Request> requestClass,
                RequestFactory<? extends Request> requestFactory) {
        this.value = value;
        this.requestClass = requestClass;
        this.requestFactory = requestFactory;
    }

    public static RequestType fromValue(String value) {
        return Valuables.fromValue(values(), value,
            DataExceptions.UNKNOWN_REQUEST_TYPE);
    }

    public static RequestType fromClass(Class<?> aClass) {
        for (RequestType type : RequestType.values()) {
            if (type.getRequestClass() == aClass) {
                return type;
            }
        }

        throw new IllegalArgumentException(String.format("Request class %s is not supported",
            aClass.getName()));
    }

    public static String report() {
        return Joiner.on(",")
            .join(Arrays.stream(values())
                .map(requestType -> requestType.getRequestClass().getSimpleName())
                .collect(Collectors.toList())
            );
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public boolean isValue(String value) {
        return this.getValue().equalsIgnoreCase(value) ||
            this.name().equalsIgnoreCase(value);
    }

    public Class<? extends Request> getRequestClass() {
        return this.requestClass;
    }

    public Request toRequest(ParseResults argResults) {
        return this.requestFactory == null ? null : this.requestFactory.create(argResults);
    }

    interface RequestFactory<T extends Request> {
        T create(ParseResults argResults);
    }
}
