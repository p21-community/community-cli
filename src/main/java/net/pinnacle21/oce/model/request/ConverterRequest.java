package net.pinnacle21.oce.model.request;

import net.pinnacle21.oce.model.ConverterInfo;

public class ConverterRequest implements Request {
    private ConverterInfo info;

    ConverterRequest(ConverterInfo info) {
        this.info = info;
    }

    public ConverterInfo getInfo() {
        return info;
    }

    public void setInfo(ConverterInfo info) {
        this.info = info;
    }

    @Override
    public RequestType getType() {
        return RequestType.CONVERTER;
    }
}
