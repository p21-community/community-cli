package net.pinnacle21.oce.model.request;

import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.input.ParseResults;
import net.pinnacle21.oce.utils.CliPreconditions;

public abstract class AbstractRequestBuilder<T extends AbstractRequestBuilder, R extends Request> {
    private ParseResults parseResults;

    protected abstract T getThis();

    public abstract R build();

    public ParseResults getParseResults() {
        return parseResults;
    }

    public T setParseResults(ParseResults parseResults) {
        this.parseResults = CliPreconditions.checkProvided(parseResults,
            ValidationExceptions.MISSING_ARG_PARSE_RESULTS);
        return getThis();
    }
}
