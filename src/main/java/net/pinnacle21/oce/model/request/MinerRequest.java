package net.pinnacle21.oce.model.request;

import net.pinnacle21.oce.model.CliModel;
import net.pinnacle21.oce.model.ReportType;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datasource.DataSource;

import java.io.File;

public class MinerRequest implements CliModel, Request {
    private final DataSource url;
    private final ReportType reportType;
    private final File categories;
    private final DataSource output;
    private final DataPackage dataPackage;

    MinerRequest(DataPackage dataPackage,
                 ReportType reportType,
                 File categories,
                 DataSource output) {
        this.url = dataPackage.getSource();
        this.reportType = reportType;
        this.categories = categories;
        this.output = output;
        this.dataPackage = dataPackage;
    }

    public DataSource getUrl() {
        return url;
    }

    public ReportType getReportType() {
        return reportType;
    }

    public File getCategories() {
        return categories;
    }

    public DataSource getOutput() {
        return output;
    }

    @Override
    public RequestType getType() {
        return RequestType.MINER;
    }

    @Override
    public String getEngineVersion() {
        return this.dataPackage.getEngineVersion();
    }

    @Override
    public File getEngineFolder() {
        return this.dataPackage.getEngineFolder();
    }
}
