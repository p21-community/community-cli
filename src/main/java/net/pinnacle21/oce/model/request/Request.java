package net.pinnacle21.oce.model.request;

public interface Request {
    RequestType getType();
}
