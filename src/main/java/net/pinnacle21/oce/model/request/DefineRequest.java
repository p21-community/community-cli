package net.pinnacle21.oce.model.request;

import net.pinnacle21.oce.model.DefineConversionInfo;

import java.util.List;

public class DefineRequest implements Request {
    private List<DefineConversionInfo> infos;

    DefineRequest(List<DefineConversionInfo> infos) {
        this.infos = infos;
    }

    @Override
    public RequestType getType() {
        return RequestType.DEFINE;
    }

    public List<DefineConversionInfo> getInfos() {
        return infos;
    }

    public void setInfos(List<DefineConversionInfo> infos) {
        this.infos = infos;
    }
}
