package net.pinnacle21.oce.model.request;

import com.typesafe.config.Config;
import net.bootstrap.core.model.EngineDescriptor;
import net.bootstrap.engine.EngineUtils;
import net.bootstrap.utils.Libs;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.*;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.datasource.Schema;
import net.pinnacle21.oce.model.input.CliParameters;
import net.pinnacle21.oce.model.input.ConfigInfo;
import net.pinnacle21.oce.model.input.Input;
import net.pinnacle21.oce.utils.CliPreconditions;
import net.pinnacle21.oce.utils.DataSources;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.functions.Func1;

import java.io.File;
import java.util.EnumSet;
import java.util.Set;

import static net.pinnacle21.oce.utils.DataSources.determineSourceFormatType;
import static net.pinnacle21.oce.utils.DataSources.isDirectory;

public abstract class StandardRequestBuilder<T extends StandardRequestBuilder, R extends Request> extends AbstractRequestBuilder<T, R> {
    private static final Logger LOGGER = LoggerFactory.getLogger(StandardRequestBuilder.class);
    protected final DataPackage.Builder builder = DataPackage.Builder.builder();
    private DataSource dataSource;
    private Input<FormatType> sourceFormat;
    private Input<String> delimiter;
    private Input<String> qualifier;
    private Input<StandardFamily> standardFamily = Input.asDefault(StandardFamily.UNDEFINED);
    private Input<StandardType> standardType = Input.asDefault(StandardType.UNDEFINED);

    private static EngineDescriptor getEngineDescriptorFromVersionAndFilter(String version, RuleFilter filter) {
        EngineDescriptor descriptor = EngineUtils.getEngineDescriptorByFromName(version);
        if (StringUtils.isNotBlank(descriptor.getVersion())) {
            return descriptor;
        }

        // try version and filter
        if (filter != null && filter != RuleFilter.ALL) {
            // combine filter and version
            descriptor = EngineUtils.getEngineDescriptorByFromName(filter.getValue() + " " + version);
        }

        return descriptor;
    }

    void validate() {
        if (this.standardFamily.get() != StandardFamily.DEFINE) {
            // validate input and standard
            CliPreconditions.checkState(this.standardFamily.get() == this.standardType.get().getFamily(),
                ValidationExceptions.INVALID_DATA_SOURCE,
                String.format("Data source family=%s is invalid for standard=%s",
                    this.standardFamily, this.standardType));
        }

        if (this.sourceFormat.get() == FormatType.DELIMITED) {
            // has to have delimiter and qualifier
            CliPreconditions.checkState(this.delimiter.isProvided(),
                ValidationExceptions.REQUIRED_DELIMITER,
                String.format("Source=%s requires delimiter for format=%s",
                    dataSource.getAbsolutePath(), this.sourceFormat.get().getValue()));

            CliPreconditions.checkState(this.qualifier.isProvided(),
                ValidationExceptions.REQUIRED_QUALIFIER,
                String.format("Source=%s requires text qualifier for format=%s",
                    dataSource.getAbsolutePath(), this.sourceFormat.get().getValue()));
        }
    }

    public DataPackage.Builder getDataPackageBuilder() {
        if (dataSource.getSchema() == Schema.URL) {
            this.sourceFormat = Input.asDefault(FormatType.UNKNOWN_FORMAT);
        } else if (this.sourceFormat.get() == FormatType.UNKNOWN_FORMAT) {
            this.sourceFormat = Input.asCalculated(determineSourceFormatType(this.dataSource, true));
            if (this.sourceFormat.get() == FormatType.UNKNOWN_FORMAT) {
                this.sourceFormat = Input.asCalculated(FormatType.XPORT);
                LOGGER.info("Source format cannot be determined. Defaulting to '{}'", this.sourceFormat.get().getValue());
            } else {
                LOGGER.info("Source format is {} as '{}'", this.sourceFormat.describe(),
                    this.sourceFormat.get().getValue());
            }
        } else {
            LOGGER.info("Source format is {} as '{}'",
                this.sourceFormat.describe(),
                this.sourceFormat.get().getValue());
        }

        Pair<Input<RuleFilter>, Input<String>> resolvedFilterAndEngine = resolveEngineVersionAndFilter();
        return setupMetadata()
            .setSource(this.dataSource)
            .setSourceFormatType(this.sourceFormat)
            .setDownloadConfig(this.getParseResults().getCliParameters().downloadConfig())
            .setEngineVersion(resolvedFilterAndEngine.getRight())
            .setStandardFilter(resolvedFilterAndEngine.getLeft())
            .setSourceDelimiter(
                this.delimiter.withCalculated(this.sourceFormat.get().getDefaultDelimiter())
            )
            .setSourceQualifier(
                this.qualifier.withCalculated(this.sourceFormat.get().getDefaultQualifier())
            );
    }

    Pair<Input<RuleFilter>, Input<String>> resolveEngineVersionAndFilter() {
        CliParameters parameters = this.getParseResults().getCliParameters();
        Config p21Config = this.getParseResults().getConfig();

        Input<String> engineVersion = this.getParseResults().getEngineVersion();
        Input<RuleFilter> paramFilter = parameters.getRuleFilter();
        Input<File> config = parameters.getProcessConfig();
        Input<String> resolvedEngineVersion;

        if (!paramFilter.isProvided() && p21Config.hasPath("cli.filter")) {
            String configFilterName = p21Config.getString("cli.filter");
            LOGGER.info("setting standard.filter from config value={}",
                configFilterName);
            paramFilter = Input.asInput(RuleFilter.fromValue(configFilterName), RuleFilter.ALL);
        }

        Input<RuleFilter> resolvedFilter = paramFilter;

        if (engineVersion == null || !engineVersion.isProvided()) {
            // processing community request
            if (config.isProvided() && !isDirectory(config.get())) {
                // parse config file name
                try {
                    ConfigInfo info = ConfigInfo.Builder.builder()
                        .setConfig(DataSources.toDataSource(config.get(), false))
                        .build();
                    LOGGER.info(
                        "Extracted filter {} from config name {}",
                        resolvedFilter.get().getValue(),
                        FilenameUtils.getBaseName(info.getConfig().getAbsolutePath())
                    );
                } catch (Exception ignore) {
                    LOGGER.info(
                        "Custom config file name {}. Cannot interpret version/agency info",
                        config.get().getName()
                    );
                }
            }
            resolvedEngineVersion = Input.asCalculated(Libs.getEngineVersion(null).getEngine());
        } else {
            // find engine descriptor for value
            EngineDescriptor descriptor = getEngineDescriptorFromVersionAndFilter(
                engineVersion.get(), paramFilter.get()
            );
            if (StringUtils.isNotBlank(descriptor.getVersion())) {
                resolvedEngineVersion = Input.asInput(
                    Libs.getEngineVersion(descriptor.getVersion()).getEngine()
                );
                // check filter parameter
                if (paramFilter.isProvided()) {
                    CliPreconditions.checkState(
                        paramFilter.get() == RuleFilter.fromValue(descriptor.getAgency()),
                        ValidationExceptions.INVALID_ENGINE_VERSION,
                        String.format(
                            "Engine version %s is not compatible with filter %s",
                            engineVersion.get(),
                            paramFilter.get().getValue()
                        )
                    );
                } else {
                    // set filter from engine descriptor
                    resolvedFilter = Input.asCalculated(RuleFilter.fromValue(descriptor.getAgency()));
                    LOGGER.info("Determine filter {} from engine {}", resolvedFilter.get().getValue(),
                        descriptor.getName());
                }
            } else {
                // just use version as is and hope for the best
                Libs.Agency agency = Libs.Agency.fromString(engineVersion.get());
                if (agency != null && !resolvedFilter.isProvided()) {
                    resolvedFilter = Input.asCalculated(RuleFilter.fromValue(agency.name()));
                    LOGGER.info("Determine filter {} from engine {}", resolvedFilter.get().getValue(),
                        engineVersion.get());
                }
                resolvedEngineVersion = Input.asInput(
                    Libs.getEngineVersion(engineVersion.get()).getEngine()
                );
            }
        }

        LOGGER.info("Determined engine version {}", resolvedEngineVersion.get());

        return ImmutablePair.of(resolvedFilter, resolvedEngineVersion);
    }

    private DataPackage.Builder setupMetadata() {
        final CliParameters cliParameters = this.getParseResults().getCliParameters();

        final Set<Terminologies> providedTerminologies = EnumSet.noneOf(Terminologies.class);
        Observable<DataPackage.Builder> oTerms = Observable.from(Terminologies.values())
            .map(new Func1<Terminologies, DataPackage.Builder>() {
                @Override
                public DataPackage.Builder call(Terminologies terminology) {
                    Input<String> inputTerm = cliParameters.getTerminology(terminology);
                    if (inputTerm.isProvided()) {
                        providedTerminologies.add(terminology);
                    }
                    terminology.setVersion(inputTerm, builder);
                    return builder;
                }
            });

        Observable<DataPackage.Builder> oDics = Observable.from(Dictionaries.values())
            .map(new Func1<Dictionaries, DataPackage.Builder>() {
                @Override
                public DataPackage.Builder call(Dictionaries dictionary) {
                    dictionary.setVersion(cliParameters.getDictionary(dictionary),
                        builder);
                    return builder;
                }
            });

        Observable.concat(oTerms, oDics)
            .toList().toBlocking().single();

        if (this.standardType.isEmpty()) {
            if (providedTerminologies.contains(Terminologies.ADaM_CT)) {
                this.standardType = Input.asCalculated(StandardType.ADaMIG);
            } else if (providedTerminologies.contains(Terminologies.SDTM_CT)) {
                this.standardType = Input.asCalculated(StandardType.SDTMIG);
            } else if (providedTerminologies.contains(Terminologies.SEND_CT)) {
                this.standardType = Input.asCalculated(StandardType.SENDIG);
            }
        }

        if (this.standardFamily.isEmpty()) {
            this.standardFamily = Input.asCalculated(this.standardType.get().getFamily());
        }

        return builder
            .setStandard(this.standardType)
            .setStandardVersion(cliParameters.getStandardVersion());
    }

    public T setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        return getThis();
    }

    public T setStandardFamily(Input<StandardFamily> standardFamily) {
        this.standardFamily = standardFamily;
        return getThis();
    }

    public T setStandardType(Input<StandardType> standardType) {
        this.standardType = standardType;
        return getThis();
    }

    public T setSourceFormat(Input<FormatType> sourceFormat) {
        this.sourceFormat = sourceFormat;
        return getThis();
    }

    public T setDelimiter(Input<String> delimiter) {
        this.delimiter = delimiter;
        return getThis();
    }

    public T setQualifier(Input<String> qualifier) {
        this.qualifier = qualifier;
        return getThis();
    }
}
