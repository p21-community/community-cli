package net.pinnacle21.oce.model.request;

import com.google.common.collect.Lists;
import com.typesafe.config.Config;
import net.bootstrap.api.monitor.context.DynamicContext;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.StandardFamily;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datapackage.LocalDataPackage;
import net.pinnacle21.oce.model.datasource.StandardInputOutput;
import net.pinnacle21.oce.model.datasource.StdSource;
import net.pinnacle21.oce.model.input.*;
import net.pinnacle21.oce.utils.Constants;
import net.pinnacle21.oce.utils.DataSources;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.pinnacle21.oce.utils.CliPreconditions.checkProvided;
import static net.pinnacle21.oce.utils.CliPreconditions.checkState;
import static net.pinnacle21.oce.utils.InputUtils.getConfigInfo;
import static net.pinnacle21.oce.utils.InputUtils.getDefineSourceFile;
import static net.pinnacle21.oce.utils.OutputUtils.getOutputFileBuilder;
import static net.pinnacle21.oce.utils.OutputUtils.getReportFile;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ValidatorRequestBuilder extends StandardRequestBuilder<ValidatorRequestBuilder, ValidatorRequest> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidatorRequestBuilder.class);

    private static final Set<String> ACCEPTED_FILE_TYPES = new HashSet<>(Arrays.asList(
        "csv", "dlm", "xml", "xpt"
    ));

    private ValidatorRequestBuilder() {
    }

    public static ValidatorRequestBuilder builder() {
        return new ValidatorRequestBuilder();
    }

    @Override
    protected ValidatorRequestBuilder getThis() {
        return this;
    }

    @Override
    public ValidatorRequest build() {
        LOGGER.info("Build validator request");

        ParseResults parseResults = this.getParseResults();

        Config config = parseResults.getConfig();

        validate();

        DataSourceInfo reportData = getReportFile(
            parseResults,
            FormatType.EXCEL.toDefaultFileName("pinnacle21-report")
        );
        DataSourceInfo output;

        boolean hasOutput = parseResults.getCliParameters().getOutput().isProvided();
        if (!hasOutput) {
            LOGGER.info("No output provided. Report URL will be written to standard output");
            output = DataSourceInfo.builder()
                .setDataSource(new StdSource(StandardInputOutput.OUT))
                .build();
        } else {
            output = getOutputFileBuilder(this.getParseResults(),
                FormatType.JSON.toDefaultFileName("url"))
                .build();
            checkState(DataSources.makeDataSource(output.getDataSource()),
                SystemExceptions.FAILED_OUTPUT_DIR,
                String.format("Failed to create output folder=%s",
                    DataSources.toFolder(output.getDataSource()).getAbsolutePath()));
        }

        checkState(reportData.isValid(), ValidationExceptions.REQURED_REPORT_FILE,
            String.format("Report file is required to run validation. args=%s",
                this.getParseResults().getInput()));

        checkState(DataSources.makeDataSource(reportData.getDataSource()),
            SystemExceptions.FAILED_OUTPUT_DIR,
            String.format("Failed creating output folder for report file=%s. args=%s",
                reportData.getDataSource().getAbsolutePath(),
                this.getParseResults().getInput()));

        DataPackage dataPackage = getDataPackageBuilder()
            .setReportSource(reportData)
            .setOutput(output)
            .setDefine(getDefineSourceFile(parseResults).getDataSource())
            .build();

        checkProvided(dataPackage.getSource(),
            ValidationExceptions.REQURED_DATASOURCE,
            String.format("Data source is required for validation but missing. args=%s",
                this.getParseResults().getInput()));

        LocalDataPackage.Builder localBuilder = LocalDataPackage.Builder.builder()
            .setDataPackage(dataPackage)
            .setThreadCount(config.getInt(Constants.THREAD_COUNT))
            .setAutoDisplayDomainKeys(config.getBoolean(Constants.AUTO_DISPLAY_DOMAIN_KEYS))
            .setGenerateRuleMetrics(config.getBoolean(Constants.GENERATE_RULE_METRICS))
            .setUseEnhancedSas(config.getBoolean(Constants.USE_ENHANCED_SAS))
            .setUserId(parseResults.getCliParameters().getUserId().get())
            .setAnonymousId(parseResults.getCliParameters().getAnonymousId().get())
            .setApplicationVersion(parseResults.getCliParameters().getApplicationVersion().get());

        ConfigInfo configInfo = getConfigInfo(parseResults, dataPackage);
        localBuilder.setConfig(configInfo);
        LocalDataPackage localPackage = localBuilder.build();

        ValidatorRequest request = new ValidatorRequest(Lists.newArrayList(localPackage));

        if (dataPackage.getSourceFormatType().get() == FormatType.DEFINE_XML &&
            configInfo.getStandardType().getFamily() != StandardFamily.DEFINE) {
            throw new CLIException(ValidationExceptions.INVALID_DATA_SOURCE,
                String.format("Define XML file %s needs to be validated with Define config but %s was provided",
                    dataPackage.getSource().getAbsolutePath(),
                    configInfo.getConfig().getAbsolutePath()));
        }

        CliParameters cliParams = parseResults.getCliParameters();
        if (cliParams instanceof CommandLine
                && !cliParams.getProcessConfig().isProvided()
                && cliParams.getEngineVersion().isProvided()
                && !cliParams.getStandardVersion().isProvided()) {
            throw new CLIException(ValidationExceptions.REQURED_STANDARD_VERSION);
        }

        if (getSources(cliParams)
                .stream()
                .map(String::toLowerCase)
                .anyMatch(source -> !ACCEPTED_FILE_TYPES.contains(FilenameUtils.getExtension(source)))
        ) {
            throw new CLIException(
                ValidationExceptions.INVALID_DELIMITED_FILE_TYPE,
                "Please use .dlm or .csv file types."
            );
        }

        if (DataSources.exists(dataPackage.getSource())) {
            checkState(DataSources.exists(localPackage.getConfigInfo().getConfig()),
                ConfigInfo.getErrorCode(localPackage.getConfigInfo()),
                ConfigInfo.getErrorMessage(localPackage, parseResults));
        } else {
            throw new CLIException(ValidationExceptions.INVALID_DATA_SOURCE,
                String.format("Cannot run local validation. Data source='%s' does not exist",
                    dataPackage.getSource().getAbsolutePath()));
        }

        return request;
    }

    private static List<String> getSources(CliParameters params) {
        // the list returned from getSources is immutable
        List<String> sources = new ArrayList<>(params.getSources().get());
        
        for (StandardFamily fam : StandardFamily.values()) {
            try {
                List<String> files = params.getSources(fam).get()
                    .stream()
                    .filter(file-> !Files.isDirectory(Paths.get(file)))
                    .collect(Collectors.toList());

                sources.addAll(files);
            } catch (CLIException ignored) {
                // do nothing
                // getSources(fam) will throw a cli exception if there isn't
                // a source mapped to the family, but there is no other
                // way to get all of the sources
            }
        }

        return sources;
    }
}
