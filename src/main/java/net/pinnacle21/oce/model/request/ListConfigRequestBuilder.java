package net.pinnacle21.oce.model.request;

import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.ListInfo;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datasource.StandardInputOutput;
import net.pinnacle21.oce.model.datasource.StdSource;
import net.pinnacle21.oce.model.input.ConfigInfo;
import net.pinnacle21.oce.model.input.DataSourceInfo;
import net.pinnacle21.oce.model.input.ParseResults;
import net.pinnacle21.oce.utils.DataSources;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.pinnacle21.oce.utils.CliPreconditions.checkState;
import static net.pinnacle21.oce.utils.InputUtils.getConfigInfo;
import static net.pinnacle21.oce.utils.OutputUtils.getOutputFileBuilder;

public class ListConfigRequestBuilder extends StandardRequestBuilder<ListConfigRequestBuilder, ListConfigRequest> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ListConfigRequestBuilder.class);

    private ListConfigRequestBuilder() {
    }

    public static ListConfigRequestBuilder builder() {
        return new ListConfigRequestBuilder();
    }

    @Override
    protected ListConfigRequestBuilder getThis() {
        return this;
    }

    @Override
    public DataPackage.Builder getDataPackageBuilder() {
        return this.builder;
    }

    @Override
    public ListConfigRequest build() {
        ParseResults parseResults = this.getParseResults();

        DataPackage dataPackage = getDataPackageBuilder()
            .build();

        DataSourceInfo output;
        boolean hasOutput = parseResults.getCliParameters().getOutput().isProvided();
        if (!hasOutput) {
            LOGGER.info("No output provided. List will output to standard output");
            output = DataSourceInfo.builder()
                .setDataSource(new StdSource(StandardInputOutput.OUT))
                .build();
        } else {
            output = getOutputFileBuilder(this.getParseResults(),
                FormatType.JSON.toDefaultFileName("list"))
                .build();
            checkState(DataSources.makeDataSource(output.getDataSource()),
                SystemExceptions.FAILED_OUTPUT_DIR,
                String.format("Failed to create output folder=%s",
                    DataSources.toFolder(output.getDataSource()).getAbsolutePath()));
        }

        checkState(output.isValid(), ValidationExceptions.REQUIRED_OUTPUT,
            String.format("Output is required to list project configuration. args=%s",
                this.getParseResults().getInput()));

        ConfigInfo configInfo = getConfigInfo(parseResults, dataPackage);

        return new ListConfigRequest(
            ListInfo.Builder.builder()
                .setDataPackage(dataPackage)
                .setOutput(output.getDataSource())
                .setConfig(configInfo.getConfig())
                .setClean(parseResults.getCliParameters().isClean().get())
                .setLoggedIn(!StringUtils.isBlank(parseResults.getCliParameters().getUserId().get()))
                .build()
        );
    }
}
