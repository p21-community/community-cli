package net.pinnacle21.oce.model.request;

import com.google.common.collect.Lists;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.DefineConversionInfo;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datasource.AbstractFileDS;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.input.DataSourceInfo;
import net.pinnacle21.oce.model.input.ParseResults;
import net.pinnacle21.oce.utils.DataSources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.pinnacle21.oce.utils.CliPreconditions.checkProvided;
import static net.pinnacle21.oce.utils.CliPreconditions.checkState;
import static net.pinnacle21.oce.utils.DataSources.isValid;
import static net.pinnacle21.oce.utils.OutputUtils.getOutputFileBuilder;
import static net.pinnacle21.oce.utils.OutputUtils.getReportFile;

public class DefineRequestBuilder extends StandardRequestBuilder<DefineRequestBuilder, DefineRequest> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefineRequestBuilder.class);

    private DefineRequestBuilder() {
    }

    public static DefineRequestBuilder builder() {
        return new DefineRequestBuilder();
    }

    @Override
    protected DefineRequestBuilder getThis() {
        return this;
    }

    @Override
    public DefineRequest build() {
        LOGGER.info("Build define request");

        validate();

        ParseResults parseResults = this.getParseResults();

        DataPackage.Builder dataPackageBuilder = getDataPackageBuilder();

        if (parseResults.getCliParameters().getReport().isProvided()) {
            dataPackageBuilder.setReportSource(getReportFile(parseResults,
                FormatType.EXCEL.toDefaultFileName("pinnacle21-report")));
        }
        DataPackage dataPackage = dataPackageBuilder.build();

        AbstractFileDS source = (AbstractFileDS) dataPackage.getSource();
        checkState(isValid(source) && source.getFile().exists(),
            ValidationExceptions.REQURED_DATASOURCE,
            String.format("Source is required for define Excel spec conversion. input=%s",
                this.getParseResults().getInput()));

        DataSourceInfo output = getOutputFileBuilder(this.getParseResults(),
            FormatType.DEFINE_XML.toDefaultFileName("define"))
            .build();

        checkState(output.getDataSource() != DataSource.EMPTY,
            ValidationExceptions.REQUIRED_OUTPUT,
            String.format("Output is required for define Excel spec conversion. input=%s",
                this.getParseResults().getInput()));

        DataSources.makeDataSource(output.getDataSource());

        DefineConversionInfo info = DefineConversionInfo.Builder.builder()
            .setDataPackage(dataPackage)
            .setExcelFile(source)
            .setXmlFile(output.getDataSource())
            .build();

        checkProvided(dataPackage.getSource(),
            ValidationExceptions.REQURED_DATASOURCE,
            String.format("Data source is required for validation but missing. args=%s",
                this.getParseResults().getInput()));

        return new DefineRequest(Lists.newArrayList(info));
    }
}
