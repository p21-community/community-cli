package net.pinnacle21.oce.model.request;

import net.bootstrap.core.model.FileType;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.ConverterInfo;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.datasource.MultiSource;
import net.pinnacle21.oce.model.datasource.Schema;
import net.pinnacle21.oce.model.input.DataSourceInfo;
import net.pinnacle21.oce.model.input.Input;
import net.pinnacle21.oce.utils.DataSources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static net.pinnacle21.oce.utils.CliPreconditions.checkCollection;
import static net.pinnacle21.oce.utils.CliPreconditions.checkState;
import static net.pinnacle21.oce.utils.DataSources.isDirectory;
import static net.pinnacle21.oce.utils.InputUtils.getConfigInfo;
import static net.pinnacle21.oce.utils.InputUtils.getDefineSourceFile;
import static net.pinnacle21.oce.utils.OutputUtils.getOutputFileOrFolder;

public class ConverterRequestBuilder extends StandardRequestBuilder<ConverterRequestBuilder, ConverterRequest> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConverterRequestBuilder.class);

    private ConverterRequestBuilder() {
    }

    public static ConverterRequestBuilder builder() {
        return new ConverterRequestBuilder();
    }

    @Override
    protected ConverterRequestBuilder getThis() {
        return this;
    }

    @Override
    public ConverterRequest build() {
        LOGGER.info("Build converter request");

        validate();

        DataPackage.Builder builder = getDataPackageBuilder();
        DataPackage tempPackage = builder.build();
        Input<FormatType> sourceFormat = tempPackage.getSourceFormatType();
        Input<FormatType> outputFormat = this.getParseResults()
            .getCliParameters().getOutputFormat();
        List<DataSource> sources = getSources(tempPackage);

        final boolean isConvertDefineToSpec = sourceFormat.get() == FormatType.DEFINE_XML &&
            outputFormat.get() == FormatType.DEFINE_SPEC;

        DataSourceInfo output;
        if (isConvertDefineToSpec || outputFormat.get().getBootstrapType() == FileType.DEFINE) {
            output = getOutputFileOrFolder(this.getParseResults(),
                outputFormat.get().toDefaultFileName("define"));
        } else {
            output = getOutputFileOrFolder(this.getParseResults(), null);
            if (!isDirectory(output.getDataSource()) &&
                sources.size() > 1) {
                if (output.getFormat().get() == FormatType.EXCEL) {
                    outputFormat = outputFormat.withCalculated(FormatType.DEFINE_SPEC);
                } else if (output.getFormat().get().getExtension().equalsIgnoreCase("xml")) {
                    outputFormat = outputFormat.withCalculated(FormatType.DEFINE_XML);
                } else {
                    outputFormat = output.getFormat();
                }

                if (outputFormat.get().getBootstrapType() == FileType.DEFINE) {
                    sourceFormat = sourceFormat.withCalculated(FormatType.XPORT);
                    LOGGER.info("Define generator is determined from multiple sources {} and a single output file {} of format {}",
                        sourceFormat, output.getDataSource().getAbsolutePath(), outputFormat);
                }
            } else {
                LOGGER.info("Convert XPT files in folder {} to {} in folder {}",
                    tempPackage.getSource().getAbsolutePath(), output.getFormat(), output.getDataSource().getAbsolutePath());
                sourceFormat = sourceFormat.withCalculated(FormatType.XPORT);
                outputFormat = output.getFormat();
            }
        }

        DataPackage finalPackage = builder
            .setSourceFormatType(sourceFormat)
            .build();

        ConverterInfo.Builder cBuilder = ConverterInfo.Builder.builder()
            .setDataPackage(finalPackage)
            .setSourceFormatType(sourceFormat)
            .setOutputFormatType(outputFormat)
            .setOutput(output.getDataSource())
            .setSources(sources)
            .setDefine(getDefineSourceFile(this.getParseResults()).getDataSource());

        // Do not set the config if converting from a define.xml to Excel
        if (outputFormat.get() != FormatType.DEFINE_SPEC || sourceFormat.get() != FormatType.DEFINE_XML) {
            cBuilder.setConfig(getConfigInfo(this.getParseResults(), finalPackage));
        }

        return new ConverterRequest(cBuilder.build());
    }

    private List<DataSource> getSources(DataPackage dataPackage) {
        DataSource source = dataPackage.getSource();
        final Input<FormatType> sourceFormat = dataPackage.getSourceFormatType();

        checkState(DataSources.isValid(source),
            ValidationExceptions.REQURED_DATASOURCE,
            String.format("Data source is required but was not provided. Data package=%s",
                dataPackage));

        List<DataSource> sources = null;
        final Predicate<File> fileFilter = file -> sourceFormat.get()
            .isApplicable(file);

        if (source.getSchema() == Schema.FILE) {
            sources = DataSources.from(source, fileFilter);
        } else if (source.getSchema() == Schema.MULTI_SOURCE) {
            sources = ((MultiSource) source).getSources().stream().filter(input -> {
                File file = DataSources.toFile(input);
                return fileFilter.test(file);
            }).collect(Collectors.toList());
        }

        checkCollection(sources, ValidationExceptions.INVALID_DATA_SOURCE,
            String.format("Data source is invalid for converter operation. source=%s. Expected source format=%s",
                source.getAbsolutePath(), sourceFormat.get().getValue()));

        return sources;
    }
}
