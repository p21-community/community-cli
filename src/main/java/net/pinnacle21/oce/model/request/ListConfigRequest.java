package net.pinnacle21.oce.model.request;

import net.pinnacle21.oce.model.ListInfo;

public class ListConfigRequest implements Request {
    private ListInfo listInfo;

    ListConfigRequest(ListInfo listInfo) {
        this.listInfo = listInfo;
    }

    public ListInfo getListInfo() {
        return listInfo;
    }

    @Override
    public RequestType getType() {
        return RequestType.LIST_CONFIG;
    }
}
