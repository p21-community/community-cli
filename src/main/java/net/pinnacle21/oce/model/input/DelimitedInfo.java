package net.pinnacle21.oce.model.input;

public class DelimitedInfo {
    private final Input<String> delimiter;
    private final Input<String> qualifier;

    public DelimitedInfo(Input<String> delimiter, Input<String> qualifier) {
        this.delimiter = delimiter;
        this.qualifier = qualifier;
    }

    public Input<String> getDelimiter() {
        return delimiter;
    }

    public Input<String> getQualifier() {
        return qualifier;
    }
}
