package net.pinnacle21.oce.model.input;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.bootstrap.api.monitor.context.DynamicContext;
import net.bootstrap.core.utils.BootstrapFileUtils;
import net.bootstrap.utils.Libs;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.*;
import net.pinnacle21.oce.model.community.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static net.pinnacle21.oce.utils.CliPreconditions.checkProvided;
import static net.pinnacle21.oce.utils.Constants.COMMUNITY_MODE;
import static net.pinnacle21.oce.utils.Constants.COMMUNITY_MODE_URL;

public class CommunityInput implements CliParameters {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommunityInput.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final String json;
    private final BaseCommunityModel communityInput;
    private final Action action;

    public CommunityInput(String json) {
        this.json = checkProvided(json, ValidationExceptions.REQUIRED_GENERAL,
            "Community input is not provided");
        try {
            this.communityInput = OBJECT_MAPPER.readValue(this.json, BaseCommunityModel.class);
            this.action = this.communityInput.getAction();

            DynamicContext.setAnonymousId(this.communityInput.getAnonymousId());
            DynamicContext.setUserId(this.communityInput.getUserId());
            DynamicContext.setApplicationVersion(this.communityInput.getApplicationVersion());

            checkProvided(this.action, SystemExceptions.FAILED_REQUEST_TYPE,
                String.format("Community input is missing action attribute. Cannot continue. Input=%s", this.json));
        } catch (IOException ex) {
            throw new CLIException(SystemExceptions.FAILED_ARG_PARSE,
                report(), ex);
        }
    }

    @Override
    public Input<String> getProject() {
        return Input.asDefault("community");
    }

    @Override
    public Input<String> getStudy() {
        return Input.asDefault("community");
    }

    @Override
    public Input<String> getDataPackage() {
        return Input.asDefault("community");
    }

    @Override
    public Input<String> getGroup() {
        return Input.asDefault("community");
    }

    @Override
    public Input<Boolean> getSourceCleanup() {
        return Input.asInput(Boolean.FALSE, false);
    }

    @Override
    public Input<File> getProcessConfig() {
        return Input.asInput(this.action.toConfig(this.communityInput));
    }

    @Override
    public Input<List<String>> getSources() {
        return Input.asInput(this.communityInput.getSources().getPaths());
    }

    @Override
    public Input<Boolean> listConfiguration() {
        return Input.asBool(communityInput.getAction() == Action.LIST_CONFIG);
    }

    @Override
    public Input<Boolean> downloadConfig() {
        return Input.asBool(true);
    }

    @Override
    public Input<ReportType> getReportType() {
        CommunityReport report = this.action.toReport(this.communityInput);
        return Input.asInput(report.getReportType(), ReportType.EXCEL);
    }

    @Override
    public Input<List<String>> getSources(StandardFamily family) {
        if (family == null) {
            return getSources();
        }

        Input<StandardType> standardType = this.getStandard();
        if (standardType.isProvided() && standardType.get().getFamily() == family) {
            return getSources();
        }

        if (family == StandardFamily.DEFINE) {
            Input<File> define = getDefineFile();
            return Input.asList(define.isProvided() ? define.get().getAbsolutePath() : null);
        }

        return Input.asEmptyList();
    }

    @Override
    public Input<File> getDefineFile() {
        return Input.asFile(this.action.toDefine(this.communityInput));
    }

    @Override
    public Input<FormatType> getSourceFormat() {
        if (this.communityInput.getGenerate() == GenerateType.DEFINE &&
            this.communityInput.getSources().getType() == FormatType.EXCEL) {
            return Input.asInput(FormatType.DEFINE_SPEC);
        }

        return Input.asInput(this.communityInput.getSources().getType());
    }

    @Override
    public Input<StandardType> getStandard() {
        return Input.asInput(this.action.toStandard(this.communityInput));
    }

    @Override
    public Input<String> getStandardVersion() {
        return Input.emptyString();
    }

    @Override
    public Input<RuleFilter> getRuleFilter() {
        return Input.asDefault(RuleFilter.ALL);
    }

    @Override
    public Input<String> getTerminology(Terminologies terminology) {
        Map<Valuable, String> versionMap = this.action.toVersions(this.communityInput);
        return Input.asString(versionMap.get(terminology));
    }

    @Override
    public Input<String> getDictionary(Dictionaries dictionary) {
        Map<Valuable, String> versionMap = this.action.toVersions(this.communityInput);
        return Input.asString(versionMap.get(dictionary));
    }

    @Override
    public Input<String> getReport() {
        if (this.action == Action.VALIDATE_DATA) {
            CommunityReport report = this.action.toReport(this.communityInput);

            return Input.asString(report.getPath());
        }

        return Input.emptyString();
    }

    @Override
    public Input<Integer> getReportCutoff() {
        return Input.asInteger(this.action.toReport(this.communityInput).getMessageLimit());
    }

    @Override
    public EnterpriseInfo getEnterpriseInfo() {
        return new EnterpriseInfo(COMMUNITY_MODE, COMMUNITY_MODE_URL, null, null);
    }

    @Override
    public Input<String> getOutput() {
        if (this.action == Action.VALIDATE_DATA) {
            return Input.asDefault(null);
        } else if (this.action == Action.LIST_CONFIG) {
            return Input.asString(this.communityInput.getTarget());
        }

        CommunityReport report = this.action.toReport(this.communityInput);
        return Input.asString(report.getPath());
    }

    @Override
    public Input<FormatType> getOutputFormat() {
        Input<FormatType> fmt = Input.asInput(this.communityInput.getOutputFormat());
        if (!fmt.isProvided()) {
            if (this.action == Action.VALIDATE_DATA) {
                // for validator output format is always Excel
                return Input.asInput(FormatType.EXCEL);
            }

            // check generate type
            if (this.communityInput.getGenerate() == GenerateType.SPEC) {
                return Input.asInput(FormatType.DEFINE_SPEC);
            } else if (this.communityInput.getGenerate() == GenerateType.DEFINE) {
                return Input.asInput(FormatType.DEFINE_XML);
            } else if (this.action == Action.LIST_CONFIG) {
                return Input.asCalculated(FormatType.JSON);
            } else if (this.action == Action.MINER) {
                return Input.asCalculated(FormatType.EXCEL);
            }
        }

        return fmt;
    }

    @Override
    public Input<Integer> getNumberOfThreads() {
        return Input.asInteger(this.action.toThreads(this.communityInput));
    }

    @Override
    public Input<String> getEngineVersion() {
        return Input.asString(this.communityInput.getEngineVersion(),
            Libs.getEngineVersion(null).getVersion());
    }

    @Override
    public Input<File> getEngineFolder() {
        File folder = null;
        try {
            if (StringUtils.isNotBlank(this.communityInput.getEngineFolder())) {
                folder = new File(this.communityInput.getEngineFolder());
                if (!folder.exists()) {
                    BootstrapFileUtils.forceMkdir(folder);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Failed to get/create engine folder. Will default to the lib folder next to the CLI jar");
        }

        return Input.asInput(folder);
    }

    @Override
    public Input<Boolean> isGenerateRuleMetrics() {
        return Input.asBool(this.action.toGenerateRuleMetrics(this.communityInput));
    }

    @Override
    public Input<Boolean> isUseEnhancedSas() {
        return Input.asBool(this.action.toUseEnhancedSas(this.communityInput));
    }

    @Override
    public Input<Boolean> isAutoDisplayDomainKeys() {
        return Input.asBool(this.action.toAutoDisplayDomainKeys(this.communityInput));
    }

    @Override
    public DelimitedInfo getDelimitedInfo() {
        CommunityDataSource ds = this.communityInput.getSources();
        if (ds != null) {
            return new DelimitedInfo(Input.asString(ds.getDelimiter()),
                Input.asString(ds.getQualifier()));
        }

        return new DelimitedInfo(Input.emptyString(),
            Input.emptyString());
    }

    @Override
    public Input<Boolean> forceEnterprise() {
        return Input.asBool(false);
    }

    @Override
    public Input<Boolean> isUpload() {
        return Input.asBool(false);
    }

    @Override
    public Input<Boolean> isClean() {
        return Input.asBool(false);
    }

    @Override
    public Input<Boolean> isOnline() {
        return Input.asBool(this.communityInput.getOnline());
    }

    @Override
    public Input<String> getUserId() {
        return Input.asString(this.communityInput.getUserId());
    }

    @Override
    public Input<String> getAnonymousId() {
        return Input.asString(this.communityInput.getAnonymousId());
    }

    @Override
    public Input<String> getApplicationVersion() {
        return Input.asString(this.communityInput.getApplicationVersion());
    }

    @Override
    public String serialize() {
        return json;
    }

    public Action getAction() {
        return action;
    }

    @Override
    public String report() {
        return this.json;
    }

    @Override
    public boolean help() {
        return false;
    }

    @Override
    public void trace() {

    }
}
