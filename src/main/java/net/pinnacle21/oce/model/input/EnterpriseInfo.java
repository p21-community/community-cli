package net.pinnacle21.oce.model.input;

import static net.pinnacle21.oce.utils.Constants.COMMUNITY_MODE;

public class EnterpriseInfo {
    private final String apiKey;
    private final String webHost;
    private final Input<Boolean> createAlways;
    private final Input<Boolean> enableBC;

    EnterpriseInfo(String apiKey,
                   String webHost,
                   String createAlways,
                   String enableBC) {
        this.apiKey = apiKey;
        this.webHost = webHost;
        this.createAlways = Input.asBool(createAlways, true);
        this.enableBC = Input.asBool(enableBC, false);
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getWebHost() {
        return webHost;
    }

    public Input<Boolean> isCreateAlways() {
        return createAlways;
    }

    public Input<Boolean> getEnableBC() {
        return enableBC;
    }

    public boolean isValid() {
        return !COMMUNITY_MODE.equalsIgnoreCase(this.apiKey);
    }
}
