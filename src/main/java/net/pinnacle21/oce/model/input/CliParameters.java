package net.pinnacle21.oce.model.input;

import net.pinnacle21.oce.model.*;

import java.io.File;
import java.util.List;

public interface CliParameters {
    /**
     * Get project name. Used for integration with enterprise. Can be retrieved from config file.
     *
     * @return
     */
    Input<String> getProject();

    /**
     * Get study name. Used for integration with enterprise. Can be retrieved from config.
     *
     * @return
     */
    Input<String> getStudy();

    /**
     * Get data package name. Used for integration with enterprise. Can be retrieved from config.
     *
     * @return
     */
    Input<String> getDataPackage();

    /**
     * Get enterprise user group name. Can be retrieved from config.
     *
     * @return
     */
    Input<String> getGroup();

    /**
     * Source cleanup true/false. Default is false. For remote data sources always true
     *
     * @return
     */
    Input<Boolean> getSourceCleanup();

    /**
     * Local config folder or file. Used for local validation, conversions, and define generation process.
     *
     * @return
     */
    Input<File> getProcessConfig();

    /**
     * lib folder for engine libraries download. Default lib folder is in the CLI installation directory.
     *
     * @return
     */
    Input<File> getEngineFolder();

    /**
     * Get data source info. Can be file, list of files separated by semi-colon or folder
     *
     * @return
     */
    Input<List<String>> getSources();

    Input<List<String>> getSources(StandardFamily family);

    Input<File> getDefineFile();

    /**
     * Format of the source data
     *
     * @return
     */
    Input<FormatType> getSourceFormat();

    /**
     * CDISC standard to be used for CLI processes and conversions
     *
     * @return
     */
    Input<StandardType> getStandard();

    /**
     * Standard version
     *
     * @return
     */
    Input<String> getStandardVersion();

    /**
     * Rule filtr used to build config file name. Default to ALL (no filter)
     *
     * @return
     */
    Input<RuleFilter> getRuleFilter();

    /**
     * Map all terminology versions
     *
     * @return
     */
    Input<String> getTerminology(Terminologies terminology);

    /**
     * Map all terminology versions
     *
     * @return
     */
    Input<String> getDictionary(Dictionaries dictionary);

    /**
     * Get report info.
     *
     * @return
     */
    Input<String> getReport();

    Input<Integer> getReportCutoff();

    /**
     * Get enterprise web host and api key. Available for enterprise setup
     *
     * @return
     */
    EnterpriseInfo getEnterpriseInfo();

    Input<String> getOutput();

    /**
     * Output format type
     *
     * @return
     */
    Input<FormatType> getOutputFormat();

    Input<Integer> getNumberOfThreads();

    Input<String> getEngineVersion();

    Input<Boolean> isGenerateRuleMetrics();

    Input<Boolean> isUseEnhancedSas();

    Input<Boolean> isAutoDisplayDomainKeys();

    DelimitedInfo getDelimitedInfo();

    Input<Boolean> forceEnterprise();

    /**
     * Upload data to enterprise server.
     *
     * @return
     */
    Input<Boolean> isUpload();

    /**
     * Report parameters for logging purposes
     *
     * @return
     */
    String report();

    /**
     * list parameter - list project/study/datapackage options
     * available only for enterprise enabled
     */
    Input<Boolean> listConfiguration();

    /**
     * Set --download.config true/false. Default false. Set to true if CLI should download config
     *
     * @return
     */
    Input<Boolean> downloadConfig();

    /**
     * Report type. Default to EXCEL.
     *
     * @return
     */
    Input<ReportType> getReportType();

    /**
     * Print help. If user is asking for help print help message.
     *
     * @return true if help was needed
     */
    boolean help();

    void trace();

    /**
     * Clean all output before run.
     *
     * @return
     */
    Input<Boolean> isClean();

    /**
     * Online status.
     *
     * @return
     */
    Input<Boolean> isOnline();

    /**
     * Gets the Pinnacle ID of the user running the command.
     */
    Input<String> getUserId();

    /**
     * Gets the anonymous ID of the user running the command.
     */
    Input<String> getAnonymousId();

    /**
     * Gets the application version.
     */
    Input<String> getApplicationVersion();

    String serialize();
}
