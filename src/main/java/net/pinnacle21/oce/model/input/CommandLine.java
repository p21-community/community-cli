package net.pinnacle21.oce.model.input;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import joptsimple.ValueConverter;
import net.bootstrap.core.utils.OSUtils;
import net.bootstrap.utils.Libs;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.DataExceptions;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.Dictionaries;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.ReportType;
import net.pinnacle21.oce.model.RuleFilter;
import net.pinnacle21.oce.model.StandardFamily;
import net.pinnacle21.oce.model.StandardType;
import net.pinnacle21.oce.model.Terminologies;
import net.pinnacle21.oce.model.datasource.Schema;
import net.pinnacle21.oce.utils.Constants;
import net.pinnacle21.oce.utils.HelpPrinter;
import net.pinnacle21.oce.utils.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static net.pinnacle21.oce.utils.CliPreconditions.checkProvided;
import static net.pinnacle21.oce.utils.DataSources.cleanupPath;

public class CommandLine implements CliParameters {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandLine.class);

    private static final OptionParser PARSER = new OptionParser();
    private static final ValueConverter<File> FILE_CONVERTER = new ValueConverter<File>() {

        @Override
        public File convert(String value) {
            return new File(Schema.removeSchema(cleanupPath(value)));
        }

        @Override
        public Class<? extends File> valueType() {
            return File.class;
        }

        @Override
        public String valuePattern() {
            return null;
        }
    };

    //region Option Definitions
    private static final OptionSpec<String> PROJECT = PARSER
        .accepts("project", "Name of project")
        .withRequiredArg();
    private static final OptionSpec<String> STUDY = PARSER
        .accepts("study", "Name of study")
        .withRequiredArg();
    private static final OptionSpec<String> GROUP = PARSER
        .accepts("group", "Group name to assign access to")
        .withRequiredArg();
    private static final OptionSpec<String> DATA_PACKAGE = PARSER
        .accepts("datapackage", "Data package name")
        .withRequiredArg();

    private static final OptionSpec<Boolean> SOURCE_CLEANUP = PARSER
        .accepts("source.cleanup", "true/false flag to keep or cleanup source data whenever possible. " +
            "Default value is false unless data source is remote")
        .withRequiredArg()
        .ofType(Boolean.class);

    // sources
    private static final OptionSpec<File> PROCESS_CONFIG = PARSER
        .accepts("config", "1. Path to XML configuration file. For use in converting to Dataset-XML.\n" +
            "2. Path to rule config folder. Rule config will be determined using standard and version.\n" +
            "3. Path and file name of the rule config XML.\n" +
            "Note: enterprise license allows downloading of the latest/provided config XML")
        .withRequiredArg()
        .ofType(File.class)
        .withValuesConvertedBy(FILE_CONVERTER);

    private static final OptionSpec<String> SOURCE_ADAM = PARSER
        .accepts("source.adam", "Path to source ADaM data files. " +
            "Can contain directories or individual files separated by semicolon.")
        .withRequiredArg()
        .withValuesSeparatedBy(';');
    private static final OptionSpec<String> SOURCE_SDTM = PARSER
        .accepts("source.sdtm", "Path to source SDTM data files. " +
            "Can contain directories or individual files separated by semicolon.")
        .withRequiredArg()
        .withValuesSeparatedBy(';');

    private static final OptionSpec<String> SOURCE_SEND = PARSER
        .accepts("source.send", "Path to source SEND data files. " +
            "Can contain directories or individual files separated by semicolon.")
        .withRequiredArg()
        .withValuesSeparatedBy(';');
    private static final OptionSpec<String> SOURCE_QRS = PARSER
        .accepts("source.qrs", "Path to source QRS data files. " +
            "Can contain directories or individual files separated by semicolon.")
        .withRequiredArg()
        .withValuesSeparatedBy(';');

    private static final OptionSpec<File> SOURCE_DEFINE = PARSER
        .accepts("source.define", "Path to define.xml file. " +
            "If only path provided define.xml file name is assumed.")
        .withRequiredArg()
        .ofType(File.class)
        .withValuesConvertedBy(FILE_CONVERTER);

    // source format descriptor
    private static final OptionSpec<FormatType> SOURCE_FORMAT = PARSER
        .accepts("source.format", "File format. \n" +
            "Data Converter only supports XPORT.")
        .withRequiredArg()
        .ofType(FormatType.class)
        .withValuesConvertedBy(FormatType.converter());

    private static final OptionSpec<String> SOURCE_DELIMITER = PARSER
        .accepts("source.delimiter", "Field delimiter if source.type = Delimited")
        .withRequiredArg();
    private static final OptionSpec<String> SOURCE_QUALIFIER = PARSER
        .accepts("source.qualifier", "Field qualifier if source.type = Delimited")
        .withRequiredArg();

    //standard
    private static final OptionSpec<String> STANDARD = PARSER
        .accepts("standard", "Standard to validate against")
        .withRequiredArg();

    private static final OptionSpec<String> STANDARD_VERSION = PARSER
        .accepts("standard.version", "Version of standard.")
        .withRequiredArg();

    private static final OptionSpec<RuleFilter> FILTER = PARSER
        .accepts("filter", "Filter to apply for a standard. Defaults to ALL")
        .withRequiredArg()
        .ofType(RuleFilter.class)
        .withValuesConvertedBy(RuleFilter.converter());

    private static final OptionSpec<ReportType> REPORT_TYPE = PARSER
        .accepts("report.type", "Report type. Defaults to Excel. Possible values: Excel, Outcomes, and Sites")
        .withRequiredArg()
        .ofType(ReportType.class)
        .withValuesConvertedBy(ReportType.converter());

    // terminology
    private static final OptionSpec<String> CDISC_CT_ADAM_VERSION = PARSER
        .accepts("cdisc.ct.adam.version", "Version of ADaM CT controlled terminology.\n" +
            "Defaults to latest version.")
        .withRequiredArg();
    private static final OptionSpec<String> CDISC_CT_SDTM_VERSION = PARSER
        .accepts("cdisc.ct.sdtm.version", "Version of SDTM CDISC controlled terminology.\n" +
            "Defaults to latest version.")
        .withRequiredArg();
    private static final OptionSpec<String> CDISC_CT_SEND_VERSION = PARSER
        .accepts("cdisc.ct.send.version", "Version of SEND CDISC controlled terminology.\n" +
            "Defaults to latest version.")
        .withRequiredArg();
    private static final OptionSpec<String> MEDDRA_VERSION = PARSER
        .accepts("meddra.version", "Version of MedDRA dictionary.\n" +
            "Defaults to latest version.")
        .withRequiredArg();
    private static final OptionSpec<String> NDF_RT_VERSION = PARSER
        .accepts("ndf-rt.version", "Version of NDF-RT dictionary.\n" +
            "Defaults to latest version.")
        .withRequiredArg();
    private static final OptionSpec<String> SNOMED_VERSION = PARSER
        .accepts("snomed.version", "Version of SNOMED dictionary.\n" +
            "Defaults to latest version.")
        .withRequiredArg();
    private static final OptionSpec<String> UNII_VERSION = PARSER
        .accepts("unii.version", "Version of UNII dictionary.\n" +
            "Defaults to latest version.")
        .withRequiredArg();
    private static final OptionSpec<String> WHODRUG_VERSION = PARSER
        .accepts("whodrug.version", "Version of WHODrug dictionary.\n" +
            "Defaults to latest version.")
        .withRequiredArg();

    // report
    private static final OptionSpec<String> REPORT = PARSER
        .accepts("report", "Path and file name to where the report will be saved.")
        .withRequiredArg();

    private static final OptionSpec<String> REPORT_CUTOFF = PARSER
        .accepts("report.cutoff", "Number of times a record detail is printed per issue. For no limit specify 0.")
        .withRequiredArg();

    // data conversion
    private static final OptionSpec<String> SOURCE = PARSER
        .accepts("source", "Path to source data files. " +
            "Can contain directories or individual files separated by semicolon. " +
            "Path and file name to Excel specification.")
        .withRequiredArg()
        .withValuesSeparatedBy(';');

    private static final OptionSpec<String> OUTPUT = PARSER
        .accepts("output", "Path to output directory.")
        .withRequiredArg();

    private static final OptionSpec<FormatType> OUTPUT_FORMAT = PARSER
        .accepts("output.format", "Output file format.")
        .withRequiredArg()
        .ofType(FormatType.class)
        .withValuesConvertedBy(FormatType.converter());
    // enterprise config
    private static final OptionSpec<String> API_KEY = PARSER
        .accepts(Constants.API_KEY, "Enterprise API key")
        .withRequiredArg();

    private static final OptionSpec<String> WEB_HOST = PARSER
        .accepts(Constants.WEB_HOST, "Enterprise web host")
        .withRequiredArg();

    private static final OptionSpec<String> LIST_CONFIG = PARSER
        .accepts("list.config", "List Enterprise configuration.")
        .withRequiredArg();

    private static final OptionSpec<String> DOWNLOAD_CONFIG = PARSER
        .accepts(Constants.DOWNLOAD_CONFIG,
            "Download config flag. true/false. Default is false. If set to true CLI will attempt to download config")
        .withRequiredArg();

    private static final OptionSpec<String> CREATE_ALWAYS = PARSER
        .accepts(Constants.CREATE_ALWAYS, "Enterprise project/study create new if does not exist (yes/no). Default: yes")
        .withRequiredArg();

    private static final OptionSpec<Void> HELP_OPTION = PARSER
        .accepts("help", "Display help")
        .forHelp();


    private static final OptionSpec<String> NUM_THREADS = PARSER
        .accepts(Constants.THREAD_COUNT, "Number of threads to use by validation engine")
        .withRequiredArg();

    private static final OptionSpec<String> ENGINE_VERSION = PARSER
        .accepts(Constants.ENGINE_VERSION, "Version of the engine")
        .withRequiredArg();

    private static final OptionSpec<File> ENGINE_FOLDER = PARSER
        .accepts(Constants.ENGINE_FOLDER, "Local root folder for all engine libraries")
        .withRequiredArg()
        .ofType(File.class)
        .withValuesConvertedBy(FILE_CONVERTER);

    private static final OptionSpec<String> USE_ENHANCED_SAS = PARSER
        .accepts(Constants.USE_ENHANCED_SAS, "Use enhanced Sas parsing")
        .withRequiredArg();

    private static final OptionSpec<String> GENERATE_RULE_METRICS = PARSER
        .accepts(Constants.GENERATE_RULE_METRICS, "Generate rule metrics")
        .withRequiredArg();

    private static final OptionSpec<String> AUTO_DISPLAY_DOMAIN_KEYS = PARSER
        .accepts(Constants.AUTO_DISPLAY_DOMAIN_KEYS, "Auto display domain keys")
        .withRequiredArg();

    // CLI global configuration
    private static final OptionSpec<String> ENTERPRISE = PARSER
        .accepts("enterprise", "Run process on enterprise server. Need enterprise license")
        .withRequiredArg();

    private static final OptionSpec<String> ENABLE_BC = PARSER
        .accepts(Constants.ENABLE_BC, "Enable Bouncy Castle Crypto Extension (JCE) (Yes/No)")
        .withRequiredArg();

    private static final OptionSpec<String> UPLOAD = PARSER
        .accepts("upload", "Upload data and run process on enterprise server. Need enterprise license")
        .withRequiredArg();

    private static final OptionSpec<String> CLEAN = PARSER
        .accepts("clean", "Clean all output before the run")
        .withRequiredArg();
    private static final Map<StandardFamily, SourceGetter> SOURCE_GETTER_MAP;
    private static final Map<Terminologies, StringGetter> TERMINOLOGY_GETTER_MAP;
    private static final Map<Dictionaries, StringGetter> DICTIONARY_GETTER_MAP;

    static {
        ImmutableMap.Builder<StandardFamily, SourceGetter> builder = new ImmutableMap.Builder<>();
        SOURCE_GETTER_MAP = builder
            .put(StandardFamily.SDTM, SOURCE_SDTM::values)
            .put(StandardFamily.ADaM, SOURCE_ADAM::values)
            .put(StandardFamily.SEND, SOURCE_SEND::values)
            .put(StandardFamily.DEFINE, arguments -> {
                File defineFile = SOURCE_DEFINE.value(arguments);
                return defineFile == null
                    ? ImmutableList.of()
                    : ImmutableList.of(defineFile.getAbsolutePath());
            })
            .put(StandardFamily.QRS, SOURCE_QRS::values)
            .build();
    }

    static {
        ImmutableMap.Builder<Terminologies, StringGetter> builder = new ImmutableMap.Builder<>();
        TERMINOLOGY_GETTER_MAP = builder
            .put(
                Terminologies.SDTM_CT,
                arguments -> Strings.nullToEmpty(CDISC_CT_SDTM_VERSION.value(arguments)).trim()
            )
            .put(
                Terminologies.ADaM_CT,
                arguments -> Strings.nullToEmpty(CDISC_CT_ADAM_VERSION.value(arguments)).trim()
            )
            .put(
                Terminologies.SEND_CT,
                arguments -> Strings.nullToEmpty(CDISC_CT_SEND_VERSION.value(arguments)).trim()
            )
            .build();
    }

    static {
        ImmutableMap.Builder<Dictionaries, StringGetter> builder = new ImmutableMap.Builder<>();
        DICTIONARY_GETTER_MAP = builder
            .put(Dictionaries.MedDRA, arguments -> Strings.nullToEmpty(MEDDRA_VERSION.value(arguments)).trim())
            .put(Dictionaries.SNOMED, arguments -> Strings.nullToEmpty(SNOMED_VERSION.value(arguments)).trim())
            .put(Dictionaries.NDF_RT, arguments -> Strings.nullToEmpty(NDF_RT_VERSION.value(arguments)).trim())
            .put(Dictionaries.UNII, arguments -> Strings.nullToEmpty(UNII_VERSION.value(arguments)).trim())
            .put(Dictionaries.WHODRUG, arguments -> Strings.nullToEmpty(WHODRUG_VERSION.value(arguments)).trim())
            .build();
    }

    private final String[] args;
    private OptionSet arguments;

    public CommandLine(String[] args) {
        this.args = checkProvided(args,
            ValidationExceptions.REQUIRED_GENERAL, "Command line arguments are not provided");
        try {
            this.arguments = PARSER.parse(args);
        } catch (OptionException ex) {
            throw new CLIException(SystemExceptions.FAILED_ARG_PARSE,
                report(), ex);
        }
    }

    @Override
    public void trace() {
        if (LOGGER.isTraceEnabled()) {
            for (Map.Entry<OptionSpec<?>, List<?>> entry : arguments.asMap().entrySet()) {
                LOGGER.trace("Argument {} has value(s) {}", entry.getKey().options(), entry.getValue());
            }
        }
    }

    @Override
    public Input<String> getProject() {
        return Input.asString(PROJECT.value(this.arguments));
    }

    @Override
    public Input<String> getStudy() {
        return Input.asString(STUDY.value(this.arguments));
    }

    @Override
    public Input<String> getGroup() {
        return Input.asString(GROUP.value(this.arguments));
    }

    @Override
    public Input<String> getDataPackage() {
        return Input.asString(DATA_PACKAGE.value(this.arguments));
    }

    @Override
    public Input<Boolean> getSourceCleanup() {
        return Input.asInput(SOURCE_CLEANUP.value(this.arguments), Boolean.FALSE);
    }

    @Override
    public Input<File> getProcessConfig() {
        return Input.asInput(PROCESS_CONFIG.value(this.arguments));
    }

    @Override
    public Input<List<String>> getSources() {
        return getSources(null);
    }

    @Override
    public Input<List<String>> getSources(StandardFamily family) {
        if (family == null) {
            return Input.asInput(SOURCE.values(this.arguments));
        }

        SourceGetter getter = SOURCE_GETTER_MAP.get(family);
        checkProvided(getter, DataExceptions.UNKNOWN_VALUE,
            String.format("Source %s is not mapped", Objects.toString(family)));

        return Input.asInput(getter.get(this.arguments));
    }

    @Override
    public Input<File> getDefineFile() {
        return Input.asInput(SOURCE_DEFINE.value(this.arguments));
    }

    @Override
    public Input<FormatType> getSourceFormat() {
        return Input.asInput(SOURCE_FORMAT.value(this.arguments),
            FormatType.UNKNOWN_FORMAT);
    }

    @Override
    public Input<StandardType> getStandard() {
        Input<String> sStandard = Input.asInput(STANDARD.value(this.arguments));
        if (!sStandard.isProvided()) {
            return Input.asDefault(StandardType.UNDEFINED);
        }
        return Input.asInput(StandardType.fromValue(sStandard.get()));
    }

    @Override
    public Input<String> getStandardVersion() {
        return Input.asString(STANDARD_VERSION.value(this.arguments));
    }

    @Override
    public Input<RuleFilter> getRuleFilter() {
        return Input.asInput(FILTER.value(this.arguments), RuleFilter.ALL);
    }

    @Override
    public Input<String> getTerminology(Terminologies terminology) {
        StringGetter getter = TERMINOLOGY_GETTER_MAP.get(terminology);
        checkProvided(getter, DataExceptions.NOTFOUND_TERMINOLOGY,
            String.format("Terminology %s is not mapped", Objects.toString(terminology)));

        return Input.asString(getter.get(this.arguments));
    }

    @Override
    public Input<String> getDictionary(Dictionaries dictionary) {
        StringGetter getter = DICTIONARY_GETTER_MAP.get(dictionary);
        checkProvided(getter, DataExceptions.NOTFOUND_DICTIONARY,
            String.format("Dictionary %s is not mapped", Objects.toString(dictionary)));

        return Input.asString(getter.get(this.arguments));
    }

    @Override
    public Input<String> getReport() {
        return Input.asString(REPORT.value(this.arguments));
    }

    @Override
    public Input<Integer> getReportCutoff() {
        return Input.asInteger(REPORT_CUTOFF.value(this.arguments), Constants.DEFAULT_CUTOFF);
    }

    @Override
    public EnterpriseInfo getEnterpriseInfo() {
        return new EnterpriseInfo(Strings.nullToEmpty(API_KEY.value(this.arguments)).trim(),
            Strings.nullToEmpty(WEB_HOST.value(this.arguments)).trim(),
            Strings.nullToEmpty(CREATE_ALWAYS.value(this.arguments)).trim(),
            Strings.nullToEmpty(ENABLE_BC.value(this.arguments)).trim());
    }

    @Override
    public Input<Boolean> listConfiguration() {
        return Input.asBool(Strings.nullToEmpty(LIST_CONFIG.value(this.arguments)).trim(), false);
    }

    @Override
    public Input<Boolean> downloadConfig() {
        // This is not allowed from the command line per CCLI-60
        return Input.asBool(false);
    }

    @Override
    public Input<ReportType> getReportType() {
        return Input.asInput(REPORT_TYPE.value(this.arguments), ReportType.EXCEL);
    }

    @Override
    public Input<String> getOutput() {
        return Input.asString(OUTPUT.value(this.arguments));
    }

    @Override
    public Input<FormatType> getOutputFormat() {
        Input<FormatType> fmtType = Input.asInput(OUTPUT_FORMAT.value(this.arguments));
        if (!fmtType.isProvided()) {
            // determine from output parameter
            Input<String> output = getOutput();
            return FormatType.formatForOutput(output, Input.asDefault(FormatType.UNKNOWN_FORMAT));
        }

        return fmtType;
    }

    @Override
    public Input<Integer> getNumberOfThreads() {
        return Input.asInteger(NUM_THREADS.value(this.arguments),
            max(1, min(OSUtils.getProcessorCount() / 2, Constants.THREAD_COUNT_DEFAULT)));
    }

    @Override
    public Input<String> getEngineVersion() {
        return Input.asString(ENGINE_VERSION.value(this.arguments),
            Libs.getValidatorVersion(null).getEngine());
    }

    @Override
    public Input<File> getEngineFolder() {
        return Input.asInput(ENGINE_FOLDER.value(this.arguments));
    }

    @Override
    public Input<Boolean> isGenerateRuleMetrics() {
        return Input.asBool(GENERATE_RULE_METRICS.value(this.arguments),
            Constants.DEFAULT_GENERATE_RULE_METRICS);
    }

    @Override
    public Input<Boolean> isUseEnhancedSas() {
        return Input.asBool(USE_ENHANCED_SAS.value(this.arguments),
            Constants.DEFAULT_USE_ENHANCED_SAS);
    }

    @Override
    public Input<Boolean> isAutoDisplayDomainKeys() {
        return Input.asBool(AUTO_DISPLAY_DOMAIN_KEYS.value(this.arguments),
            Constants.DEFAULT_AUTO_DISPLAY_DOMAIN_KEYS);
    }

    @Override
    public DelimitedInfo getDelimitedInfo() {
        return new DelimitedInfo(
            Input.asString(SOURCE_DELIMITER.value(this.arguments), FormatType.CSV.getDefaultDelimiter()),
            Input.asString(SOURCE_QUALIFIER.value(this.arguments), FormatType.CSV.getDefaultQualifier()));
    }

    @Override
    public Input<Boolean> forceEnterprise() {
        Input<Boolean> ent = Input.asBool(ENTERPRISE.value(this.arguments), false);
        Input<Boolean> upload = isUpload();
        return Input.asInput(upload.get() || ent.get(),
            (upload.isProvided() || ent.isProvided()));
    }

    @Override
    public Input<Boolean> isUpload() {
        return Input.asBool(UPLOAD.value(this.arguments), false);
    }

    @Override
    public Input<Boolean> isClean() {
        return Input.asBool(CLEAN.value(this.arguments), false);
    }

    @Override
    public Input<Boolean> isOnline() {
        return Input.asDefault(false);
    }

    @Override
    public Input<String> getUserId() { return Input.asString(null); }

    @Override
    public Input<String> getAnonymousId() { return Input.asString(null); }

    @Override
    public Input<String> getApplicationVersion() { return Input.asString(Resources.BUILD_VERSION); }

    @Override
    public String serialize() {
        return String.join(" ", args);
    }

    @Override
    public String report() {
        return "Command line: " + Joiner.on(' ')
            .join(
                Arrays.stream(this.args)
                    .map(input -> Strings.nullToEmpty(input).trim())
                    .collect(Collectors.toList())
            );
    }

    @Override
    public boolean help() {
        if (arguments.has(HELP_OPTION)) {
            // print help and exit
            try {
                PARSER.formatHelpWith(new HelpPrinter());
                PARSER.printHelpOn(System.out);
                return true;
            } catch (IOException e) {
                LOGGER.error("Check for help failed.", e);
            }
        }

        return false;
    }

    interface SourceGetter {
        List<String> get(OptionSet arguments);
    }

    interface StringGetter {
        String get(OptionSet arguments);
    }
}
