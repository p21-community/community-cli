package net.pinnacle21.oce.model.input;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.google.common.base.MoreObjects;
import dagger.internal.Preconditions;
import joptsimple.internal.Strings;
import net.bootstrap.core.utils.config.Parser;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.CLIExceptionCode;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.RuleFilter;
import net.pinnacle21.oce.model.StandardType;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datapackage.LocalDataPackage;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.utils.DataSources;
import net.pinnacle21.oce.utils.ResourceUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import static net.pinnacle21.oce.utils.DataSources.isDirectory;
import static net.pinnacle21.oce.utils.DataSources.toFile;

@JsonFilter("configInfoFilter")
public class ConfigInfo {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigInfo.class);

    private final DataSource config;
    private final StandardType.Standard standard;

    private ConfigInfo(Builder builder) {
        this.config = Preconditions.checkNotNull(builder.config, "Config cannot be null");
        this.standard = Preconditions.checkNotNull(builder.configName, "Config name cannot be null");
    }

    public static Builder builder() {
        return new Builder();
    }

    public static CLIExceptionCode getErrorCode(ConfigInfo info) {
        return !info.hasStandard() ? SystemExceptions.EXISTS_FILE :
            ValidationExceptions.REQUIRED_CONFIG;
    }

    public static String getErrorMessage(LocalDataPackage localPackage, ParseResults parseResults) {
        if (parseResults.getCliParameters().downloadConfig().get()) {
            return String.format("Config is required to run local validation, but cannot be downloaded because config folder is not provided or found. " +
                    "Please provide config folder using --config parameter and try again. input=%s",
                parseResults.getInput());
        } else {
            return String.format("Config is required to run local validation, but was not provided or found. config=%s. input=%s",
                localPackage.getConfigInfo().getConfig().getAbsolutePath(), parseResults.getInput());
        }
    }

    private static StandardType.Standard toConfigName(DataSource config) {
        File configFile = toFile(config);
        if (configFile == null) {
            return new StandardType.Standard(
                StandardType.UNDEFINED, Parser.UNKNOWN_VALUE, RuleFilter.ALL
            );
        }

        if (isDirectory(configFile)) {
            LOGGER.info(
                "Config '{}' is a directory. Cannot parse for standard, version, and filter",
                configFile.getAbsolutePath()
            );
            return new StandardType.Standard(
                StandardType.UNDEFINED, Parser.UNKNOWN_VALUE, RuleFilter.ALL
            );
        }

        return StandardType.fromString(FilenameUtils.getBaseName(configFile.getName()));
    }

    private static StandardType.Standard toConfigName(DataSource config, DataPackage dataPackage) {
        StandardType.Standard name = toConfigName(config);
        StandardType type = name.getType();
        String version = name.getVersion();
        RuleFilter filter = name.getFilter();

        if (dataPackage == null) {
            return new StandardType.Standard(type, version, filter);
        }

        if (type == StandardType.UNDEFINED) {
            if (dataPackage.getSourceFormatType().get() == FormatType.DEFINE_XML) {
                // format type is define. Use Define XML standard config
                type = StandardType.DEFINE;
            } else {
                type = dataPackage.getStandard().get();
            }
        }

        String standardVersion = dataPackage.getStandardVersion().get();
        if (Parser.isUnknown(version) &&
            !Strings.isNullOrEmpty(standardVersion)) {
            if (dataPackage.getStandard().get() == type) {
                // use standard version from data package
                // only if StandardType in data package is matching to StandardType determined
                version = standardVersion;
            }
        }

        RuleFilter standardFilter = dataPackage.getStandardFilter().get();
        if (filter == RuleFilter.ALL && standardFilter != null && standardFilter != filter) {
            filter = standardFilter;
        }

        return new StandardType.Standard(type, version, filter);
    }

    public DataSource getConfig() {
        return this.config;
    }

    public StandardType getStandardType() {
        return this.standard.getType();
    }

    public boolean hasStandard() {
        return this.standard != null &&
            StandardType.isValid(Input.asInput(this.standard.getType()));
    }

    public String getVersion() {
        return this.standard.getVersion();
    }

    public RuleFilter getFilter() {
        return this.standard.getFilter();
    }

    public boolean isValid() {
        return DataSources.isValid(this.config) &&
            !isDirectory(this.config);
    }

    @Override
    public String toString() {
        StandardType type = getStandardType();
        return MoreObjects.toStringHelper(getClass())
            .add("configSource", this.config)
            .add("standard", type == null ? "unknown" : type.getValue())
            .add("version", this.standard.getVersion())
            .toString();
    }

    public static class Builder {
        private DataSource config;
        private DataPackage dataPackage;
        private StandardType.Standard configName = StandardType.fromString(null);

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public Builder setConfig(DataSource config) {
            this.config = config;
            return this;
        }

        public Builder setDataPackage(DataPackage dataPackage) {
            this.dataPackage = dataPackage;
            return this;
        }

        public ConfigInfo build() {
            try {
                this.configName = toConfigName(this.config, this.dataPackage);

                if (this.dataPackage != null
                    && this.configName.getType() != StandardType.UNDEFINED) {
                    this.config = ResourceUtils.getConfigFile(
                        this.config, this.configName, this.dataPackage
                    );
                    if (!DataSources.exists(this.config) && this.config != DataSource.EMPTY) {
                        DataSource versionedConfig = getVersionedConfig(
                            dataPackage.getEngineVersion()
                        );
                        if (DataSources.exists(versionedConfig)) {
                            this.config = versionedConfig;
                        } else {
                            versionedConfig = getVersionedConfig("custom");
                            if (DataSources.exists(versionedConfig)) {
                                this.config = versionedConfig;
                            }
                        }
                    }
                }

                if (isDirectory(this.config)) {
                    if (this.dataPackage == null ||
                        !StandardType.isValid(this.dataPackage.getStandard())) {
                        LOGGER.warn(
                            "Config is a folder '{}', but cannot get file since no standard details were provided or Define XML validation is requested",
                            this.config.getAbsolutePath()
                        );
                        return new ConfigInfo(this);
                    }

                    LOGGER.info(
                        "Config is a folder. Lookup file name based on standard={} and version={}",
                        this.dataPackage.getStandard().get().getValue(),
                        this.dataPackage.getStandardVersion()
                    );
                    return new ConfigInfo(this);
                }

                return new ConfigInfo(this);

            } catch (CLIException cliErr) {
                if (cliErr.getCode() == SystemExceptions.DOWNLOAD_FAILED) {
                    // failed to download config. Cannot proceed. report error
                    throw cliErr;
                }

                // swallow error and proceed
                LOGGER.warn("Local Config is not resolved. CLI Error={}", cliErr.getMessage());
                return new ConfigInfo(this);
            }
        }

        private DataSource getVersionedConfig(String version) {
            DataSource versionedConfig = DataSources.newFileOrFolder(toFile(this.config).getParentFile(),
                version);
            LOGGER.info("Config {} does not exist. Looking up in engine versioned folder {}",
                this.config.getAbsolutePath(), versionedConfig.getAbsolutePath());
            return ResourceUtils.getConfigFile(versionedConfig,
                this.configName, this.dataPackage);
        }

    }
}
