package net.pinnacle21.oce.model.input;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.google.common.base.MoreObjects;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.utils.Constants;

@JsonFilter("dataSourceInfoFilter")
public class DataSourceInfo {
    private final DataSource dataSource;
    private final Input<FormatType> format;
    private final int cutoff;
    private final DelimitedInfo delimitedInfo;
    private final DataSource remoteDataSource;

    private DataSourceInfo(Builder builder) {
        this.dataSource = builder.dataSource;
        this.format = builder.format;
        this.cutoff = builder.cutoff;
        this.delimitedInfo = builder.delimitedInfo;
        this.remoteDataSource = builder.remoteDataSource;
    }

    public static Builder builder() {
        return new Builder();
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public Input<FormatType> getFormat() {
        return format;
    }

    public Input<String> getDelimiter() {
        return this.delimitedInfo.getDelimiter();
    }

    public Input<String> getQualifier() {
        return this.delimitedInfo.getQualifier();
    }

    public int getCutoff() {
        return cutoff;
    }

    public DataSource getRemoteDataSource() {
        return remoteDataSource;
    }

    public boolean isValid() {
        return this.format != null
            && this.dataSource != null
            && this.dataSource != DataSource.EMPTY;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("dataSource", dataSource)
            .add("format", format)
            .add("cutoff", cutoff)
            .add("delimitedInfo", delimitedInfo)
            .add("remoteDataSource", remoteDataSource)
            .toString();
    }

    public static class Builder {
        private DataSource dataSource = DataSource.EMPTY;
        private Input<FormatType> format = Input.asDefault(FormatType.UNKNOWN_FORMAT);
        private int cutoff = Constants.DEFAULT_CUTOFF;
        private DelimitedInfo delimitedInfo = new DelimitedInfo(Input.emptyString(),
            Input.emptyString());
        private DataSource remoteDataSource;

        public Builder setDataSource(DataSource dataSource) {
            this.dataSource = dataSource;
            return this;
        }

        public Builder setFormat(Input<FormatType> format) {
            this.format = format;
            return this;
        }

        public Builder setFormat(FormatType format) {
            this.format = Input.asInput(format, FormatType.UNKNOWN_FORMAT);
            return this;
        }

        public Builder setCutoff(Input<Integer> cutoff) {
            this.cutoff = cutoff.defaultIfNotProvided(Constants.DEFAULT_CUTOFF);
            return this;
        }

        public Builder setCutoff(int cutoff) {
            this.cutoff = cutoff;
            return this;
        }

        public Builder setDelimitedInfo(DelimitedInfo delimitedInfo) {
            this.delimitedInfo = delimitedInfo;
            return this;
        }

        public Builder setRemoteDataSource(DataSource remoteDataSource) {
            this.remoteDataSource = remoteDataSource;
            return this;
        }

        public DataSourceInfo build() {
            return new DataSourceInfo(this);
        }
    }
}
