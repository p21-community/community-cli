package net.pinnacle21.oce.model.input;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.google.common.collect.Maps;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import net.bootstrap.utils.S3Loader;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.ReleaseLevel;
import net.pinnacle21.oce.utils.Constants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import static net.bootstrap.core.utils.jar.JarUtils.getJarFile;
import static net.pinnacle21.oce.utils.CliPreconditions.checkProvided;

@JsonFilter("parseResultsFilter")
public class ParseResults {
    private static final Logger LOGGER = LoggerFactory.getLogger(ParseResults.class);
    private static final Config CONFIG = ConfigFactory.load();
    private final CliParameters cliParameters;
    private final Config config;
    private final Input<String> engineVersion;

    private ParseResults(CliParameters cliParameters, Config config, Input<String> engineVersion) {
        this.cliParameters = cliParameters;
        this.config = config;
        this.engineVersion = engineVersion;
    }

    @SuppressWarnings("unchecked")
    public static ParseResults parseArgs(CliParameters cliParameters) {
        checkProvided(cliParameters, ValidationExceptions.REQUIRED_GENERAL);

        Map<String, Object> configParams = Maps.newHashMap();

        if (cliParameters.getEngineFolder().isProvided()) {
            configParams.put(
                Constants.ENGINE_FOLDER,
                cliParameters.getEngineFolder().get().getAbsolutePath()
            );
        }

        if (cliParameters.isOnline().isProvided()) {
            configParams.put(Constants.SYSTEM_ONLINE, cliParameters.isOnline().get());
        }

        if (cliParameters.downloadConfig().isProvided()) {
            configParams.put(Constants.DOWNLOAD_CONFIG, cliParameters.downloadConfig().get());
        }

        configParams.put(Constants.S3_ROOT, getReleaseLevel().getValue());

        Config config = S3Loader.loadClients(getConfig(configParams, cliParameters));

        if (LOGGER.isTraceEnabled()) {
            cliParameters.trace();
        }

        Input<String> engineVersion = cliParameters.getEngineVersion();

        if (!engineVersion.isProvided() && config.hasPath(Constants.ENGINE_VERSION)) {
            engineVersion.withInput(Input.asString(config.getString(Constants.ENGINE_VERSION)));
        }

        LOGGER.info("Loaded parameters |{}|", cliParameters.report());

        return new ParseResults(cliParameters, config, engineVersion);
    }

    private static String getJarVersion() {
        String jarFile = getJarFile(ParseResults.class);
        if (StringUtils.isBlank(jarFile)) {
            return "0-LOCAL-SNAPSHOT";
        }

        try {
            JarFile file = new JarFile(jarFile);
            Manifest manifest = file.getManifest();
            Attributes attributes = manifest.getMainAttributes();
            return attributes.getValue("Implementation-Version");
        } catch (Exception e) {
            LOGGER.error("Failed to read manifest", e);
            return "0-LOCAL-SNAPSHOT";
        }
    }

    private static ReleaseLevel getReleaseLevel() {
        String version = System.getProperty("pinnacle.software.version");
        if (StringUtils.isNotBlank(version)) {
            return ReleaseLevel.fromValue(version);
        }

        return ReleaseLevel.fromValue(getJarVersion());
    }

    private static Config getConfig(Map<String, Object> configParams, CliParameters cliParameters) {
        Config config = CONFIG;

        // set number o threads for validation engine
        if (!config.hasPath(Constants.THREAD_COUNT)) {
            configParams.put(Constants.THREAD_COUNT, cliParameters.getNumberOfThreads().get());
        }
        // set generate rule metrics parameter
        if (!config.hasPath(Constants.GENERATE_RULE_METRICS)) {
            configParams.put(Constants.GENERATE_RULE_METRICS,
                cliParameters.isGenerateRuleMetrics().get());
        }
        // set use enhanced SAS parsing strategy
        if (!config.hasPath(Constants.USE_ENHANCED_SAS)) {
            configParams.put(Constants.USE_ENHANCED_SAS,
                cliParameters.isUseEnhancedSas().get());
        }
        // set auto display domain keys flag
        if (!config.hasPath(Constants.AUTO_DISPLAY_DOMAIN_KEYS)) {
            configParams.put(Constants.AUTO_DISPLAY_DOMAIN_KEYS,
                cliParameters.isAutoDisplayDomainKeys().get());
        }

        // override from command line
        if (!configParams.isEmpty()) {
            config = ConfigFactory.parseMap(configParams).withFallback(config);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Config: '{}'", config.root().render());
        }

        return config;
    }

    public Config getConfig() {
        return config;
    }

    public CliParameters getCliParameters() {
        return cliParameters;
    }

    public Input<String> getEngineVersion() {
        return engineVersion;
    }

    public String getInput() {
        return this.cliParameters.report();
    }
}
