package net.pinnacle21.oce.model.input;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

public class Input<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(Input.class);
    private static final String EMPTY_STRING = "";

    private final T value;
    private final boolean provided;
    private final boolean empty;

    private Input(T value, boolean provided, boolean empty) {
        this.value = value;
        this.provided = provided;
        this.empty = empty;
    }

    public static StringInput emptyString() {
        return new StringInput(EMPTY_STRING, false);
    }

    public static <T> Input<T> safe(Input<T> src, Input<T> dest) {
        if (dest == null) {
            return src;
        }
        if (src == null) {
            return dest;
        }

        return dest.withInput(src);
    }

    public static <T> Input<T> asInput(T value) {
        return new Input<T>(value, value != null, value == null);
    }

    public static <T> Input<T> asCalculated(T value) {
        return new Input<T>(value, false, value == null);
    }

    public static <T> Input<T> asDefault(T defaultValue) {
        return Input.asInput(defaultValue, false, true);
    }

    private static <T> Input<T> asInput(T value, boolean provided, boolean empty) {
        return new Input<T>(value, provided, empty);
    }

    public static <T> Input<T> asInput(T value, T defaultValue) {
        T newVal = ObjectUtils.defaultIfNull(value, defaultValue);
        return new Input<T>(newVal, value != null, value == null || newVal == defaultValue);
    }

    public static <T> Input<T> asInput(Input<T> value, T defaultValue) {
        T otherValue = value.defaultIfNotProvided(Input.asDefault(defaultValue));
        return new Input<>(otherValue, value.isProvided(), otherValue == null || otherValue == defaultValue);
    }

    public static <T> Input<List<T>> asList(List<T> value) {
        if (value == null || value.isEmpty()) {
            return new Input<>((List<T>) ImmutableList.<T>of(), false, true);
        }
        return new Input<>(value, true, false);
    }

    public static <T> Input<List<T>> asList(T value) {
        if (value == null) {
            return new Input<>((List<T>) ImmutableList.<T>of(), false, true);
        }
        return new Input<>((List<T>) ImmutableList.of(value), true, false);
    }

    public static <T> Input<List<T>> asEmptyList() {
        return new Input<>((List<T>) ImmutableList.<T>of(), false, true);
    }

    public static Input<File> asFile(String path) {
        Input<String> pathInput = Input.asString(path);
        return pathInput.isProvided() ? Input.asInput(new File(pathInput.value)) :
            new Input<File>(null, false, true);
    }

    public static Input<String> asString(String value) {
        String sVal = Strings.emptyToNull(StringUtils.trim(value));
        return new StringInput(Strings.nullToEmpty(sVal), sVal != null);
    }

    public static Input<String> asString(String value, String defaultVal) {
        String sVal = Strings.emptyToNull(StringUtils.trim(value));
        return new StringInput(StringUtils.defaultIfBlank(sVal, defaultVal), sVal != null);
    }

    public static Input<String> asString(String value, String defaultVal, boolean provided) {
        String sVal = Strings.emptyToNull(StringUtils.trim(value));
        return new StringInput(StringUtils.defaultIfBlank(sVal, defaultVal),
            provided);
    }

    public static Input<Boolean> asBool(Boolean value) {
        return new Input<>(BooleanUtils.toBoolean(value), value != null, value == null);
    }

    public static Input<Boolean> or(Input... values) {
        if (ArrayUtils.isEmpty(values)) {
            return Input.asDefault(false);
        }

        boolean result = false;
        boolean provided = false;

        for (Input v : values) {
            result |= (Boolean) v.value;
            provided |= v.provided;
        }

        return Input.asInput(result, provided);
    }

    public static Input<Boolean> and(Input... values) {
        if (ArrayUtils.isEmpty(values)) {
            return Input.asDefault(false);
        }

        boolean result = true;
        boolean provided = true;

        for (Input v : values) {
            result &= (Boolean) v.value;
            provided &= v.provided;
        }

        return Input.asInput(result, provided);
    }

    public static Input<Boolean> asBool(String value, Boolean defaultValue) {
        String sVal = Strings.emptyToNull(StringUtils.trim(value));
        return new Input<>(sVal == null ? defaultValue :
            BooleanUtils.toBoolean(sVal), sVal != null, StringUtils.isBlank(sVal));
    }

    public static Input<Integer> asInteger(Integer value) {
        return new Input<>(value, value != null, value == null);
    }

    public static Input<Integer> asInteger(String value, Integer defaultValue) {
        String sVal = Strings.emptyToNull(StringUtils.trim(value));
        try {
            if (sVal == null) {
                LOGGER.info("Use default value {}", defaultValue);
                return new Input<>(defaultValue, false, false);
            }

            return new Input<>(Integer.parseInt(sVal), true, StringUtils.isBlank(sVal));
        } catch (NumberFormatException e) {
            LOGGER.info("Provided string [{}] cannot be converted to expected integer. Use default value {}",
                value, defaultValue);
            return new Input<>(defaultValue, false, false);
        }
    }

    public T get() {
        return value;
    }

    public T defaultIfNotProvided(T defaultInput) {
        return defaultIfNotProvided(Input.asDefault(defaultInput));
    }

    public T defaultIfNotProvided(Input<T> defaultInput) {
        if (this.isProvided()) {
            return get();
        } else if (defaultInput != null && (defaultInput.isProvided() || !defaultInput.isEmpty())) {
            return defaultInput.get();
        }

        return get();
    }

    public boolean isEmpty() {
        return empty;
    }

    public boolean isProvided() {
        return provided;
    }

    public Input<T> withDefualt(T input) {
        return withInput(Input.asDefault(input));
    }

    public Input<T> withCalculated(T input) {
        return withInput(Input.asCalculated(input));
    }

    public Input<T> withInput(Input<T> input) {
        if (input == null) {
            return this;
        }

        T val = defaultIfNotProvided(input);

        return Input.asInput(val,
            this.isProvided() || input.isProvided(),
            this.isEmpty() && input.isEmpty());
    }

    public String describe() {
        if (this.provided) {
            return "provided";
        }

        return this.empty ? "default" : "determined";
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("value", value)
            .add("userProvided", provided)
            .add("empty", empty)
            .toString();
    }

    private static class StringInput extends Input<String> {
        private StringInput(String value, boolean provided) {
            super(value, provided, StringUtils.isBlank(value));
        }

        @Override
        public StringInput withInput(Input<String> defaultValue) {
            return (StringInput) Input.asString(this.defaultIfNotProvided(defaultValue),
                defaultValue.get(), this.isProvided());
        }
    }
}
