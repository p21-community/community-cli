package net.pinnacle21.oce.model;

public interface Valuable {
    public static final Valuable UNKNOWN = new Valuable() {
        @Override
        public String getValue() {
            return "unknown";
        }

        @Override
        public boolean isValue(String value) {
            return "unknown".equalsIgnoreCase(value);
        }
    };

    String getValue();

    boolean isValue(String value);
}
