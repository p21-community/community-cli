package net.pinnacle21.oce.model;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import joptsimple.ValueConverter;
import net.bootstrap.core.model.FileType;
import net.pinnacle21.oce.exceptions.DataExceptions;
import net.pinnacle21.oce.model.input.Input;
import net.pinnacle21.oce.utils.Valuables;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Set;
import java.util.TreeSet;

import static net.pinnacle21.oce.utils.DataSources.isDirectory;

public enum FormatType implements Valuable {
    XPORT("xpt", FileType.XPORT, "xport", "SasTransport", "Sas-Transport", "Sas Transport"),
    EXCEL("xlsx", FileType.EXCEL, "excel", "xls", "xlsx"),
    CSV("csv", ",", "\"", FileType.CSV, "csv", "CommaDelimited", "Comma Delimited"),
    DATASET_XML("xml", FileType.DATASET_XML, "dataset-xml", "dataset_xml", "datasetxml", "dataset", "dataset xml"),
    DEFINE_XML("xml", FileType.DEFINE, "define-xml", "definexml", "define_xml", "define xml", "define", "validate"),
    DEFINE_SPEC("xlsx", FileType.DEFINE, "define-excel", "define-xls", "define-xlsx", "defineexcel", "define_excel",
        "define-spec", "definexls", "definexlsx"),
    DELIMITED(null, FileType.DELIMITED, "delimited"),
    JSON("json", FileType.JSON, "json"),
    URL("url", null, "url"),
    BROWSER("browser", null, "browser"),
    UNKNOWN_FORMAT(null, null, "unknown_format");

    private static final Logger LOGGER = LoggerFactory.getLogger(FormatType.class);
    private static final DateTimeFormatter DEFAULT_FORMAT = DateTimeFormat.forPattern("y-MM-dd'T'HH-mm");

    private final String value;
    private final String extension;
    private final String defaultDelimiter;
    private final String defaultQualifier;
    private final FileType bootstrapType;
    private final Set<String> allValues = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

    FormatType(String extension, FileType opencdiscType, String... values) {
        this(extension, null, null, opencdiscType, values);
    }

    FormatType(String extension, String defaultDelimiter, String defaultQualifier, FileType bootstrapType, String... values) {
        this.extension = extension;
        this.defaultDelimiter = defaultDelimiter;
        this.defaultQualifier = defaultQualifier;
        this.bootstrapType = bootstrapType;
        this.value = values[0];
        this.allValues.addAll(ImmutableList.copyOf(values));
    }

    public static FormatType fromValue(String value) {
        return Valuables.fromValue(values(), value,
            DataExceptions.UNKNOWN_FORMAT);
    }

    public static FormatType fromExt(String ext) {
        for (FormatType type : values()) {
            if (type.getExtension() != null &&
                type.getExtension().equalsIgnoreCase(ext)) {
                return type;
            }
        }

        LOGGER.warn("Extension '{}' is unknown. Return unknown format", ext);
        return UNKNOWN_FORMAT;
    }

    public static Input<FormatType> formatForOutput(Input<String> output, Input<FormatType> defaultType) {
        Input<FormatType> fmtType = defaultType;

        if (output.isProvided() && !isDirectory(new File(output.get()))) {
            String outFile = output.get();
            String fName = FilenameUtils.getBaseName(outFile);
            String fExt = FilenameUtils.getExtension(outFile);
            if (StringUtils.containsIgnoreCase(fName, "define")) {
                fName = "define";
            } else if (StringUtils.containsIgnoreCase(fName, "dataset")) {
                fName = "dataset";
            }

            try {
                fmtType = fmtType.withCalculated(FormatType.fromValue(Strings.nullToEmpty(fName) +
                    Strings.nullToEmpty(fExt)));
            } catch (Exception ex) {
                LOGGER.info("Failed to find format type for file '{}' and extension '{}'. Trying default '{}'",
                    Strings.nullToEmpty(fName), Strings.nullToEmpty(fExt), defaultType.get().getValue());
                fmtType = defaultType;
            }

            if (fmtType.get() == FormatType.UNKNOWN_FORMAT) {
                fmtType = Input.asCalculated(FormatType.fromExt(fExt));
            }
        }

        if (fmtType.get() == FormatType.UNKNOWN_FORMAT) {
            // default to EXCEL
            fmtType = fmtType.withCalculated(FormatType.EXCEL);
            LOGGER.info("Cannot determine output format type. Set to default '{}'", fmtType.get().getValue());
            return fmtType;
        } else {
            LOGGER.info("Output format type is {} as '{}'",
                fmtType.describe(), fmtType.get().getValue());
            return fmtType;
        }
    }

    public static ValueConverter<FormatType> converter() {
        return new ValueConverter<FormatType>() {
            @Override
            public FormatType convert(String value) {
                return FormatType.fromValue(Strings.nullToEmpty(value).trim());
            }

            @Override
            public Class<? extends FormatType> valueType() {
                return FormatType.class;
            }

            @Override
            public String valuePattern() {
                return null;
            }
        };
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public boolean isValue(String value) {
        return this.name().equalsIgnoreCase(value) ||
            this.allValues.contains(value);
    }

    public String getExtension() {
        return extension;
    }

    public FileType getBootstrapType() {
        return bootstrapType;
    }

    public boolean isApplicable(File file) {
        return file != null &&
            FilenameUtils.getExtension(file.getName()).equalsIgnoreCase(this.extension);
    }

    public String toDefaultFileName(String base) {
        String dateTime = DEFAULT_FORMAT.print(DateTime.now().toInstant());
        return base + "-" + dateTime + "." + getExtension();
    }

    public String getDefaultDelimiter() {
        return defaultDelimiter;
    }

    public String getDefaultQualifier() {
        return defaultQualifier;
    }
}
