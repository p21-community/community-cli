package net.pinnacle21.oce.model;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.common.base.MoreObjects;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.support.DataSourceDeserializer;

import java.io.File;

@JsonFilter("defineConversionInfoFilter")
@JsonDeserialize(builder = DefineConversionInfo.Builder.class)
public class DefineConversionInfo extends BaseModel<DefineConversionInfo> {
    private final DataPackage dataPackage;
    private final DataSource excelFile;
    private final DataSource xmlFile;

    private DefineConversionInfo(DataPackage dataPackage,
                                 DataSource excelFile,
                                 DataSource xmlFile) {
        this.dataPackage = dataPackage;
        this.excelFile = excelFile;
        this.xmlFile = xmlFile;
    }

    public DataPackage getDataPackage() {
        return dataPackage;
    }

    public DataSource getExcelFile() {
        return excelFile;
    }

    public DataSource getXmlFile() {
        return xmlFile;
    }

    @Override
    public String getEngineVersion() {
        return this.dataPackage.getEngineVersion();
    }

    @Override
    public File getEngineFolder() {
        return this.dataPackage.getEngineFolder();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("dataPackage", dataPackage)
            .add("excelFile", excelFile)
            .add("xmlFile", xmlFile)
            .toString();
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class Builder extends BaseModel.Builder<Builder, DefineConversionInfo> {
        private DataPackage dataPackage;

        @JsonDeserialize(using = DataSourceDeserializer.class)
        private DataSource excelFile;

        @JsonDeserialize(using = DataSourceDeserializer.class)
        private DataSource xmlFile;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public static Builder from(DefineConversionInfo otherInfo) {
            return builder()
                .setExcelFile(otherInfo.getExcelFile())
                .setDataPackage(otherInfo.getDataPackage())
                .setXmlFile(otherInfo.getXmlFile());
        }

        @Override
        protected Builder getThis() {
            return this;
        }

        public Builder setDataPackage(DataPackage dataPackage) {
            this.dataPackage = dataPackage;
            return this;
        }

        public Builder setExcelFile(DataSource excelFile) {
            this.excelFile = excelFile;
            return this;
        }

        public Builder setXmlFile(DataSource xmlFile) {
            this.xmlFile = xmlFile;
            return this;
        }

        public Builder fromConverter(ConverterInfo info) {
            return setDataPackage(info.getDataPackage())
                .setExcelFile(info.getOutput())
                .setXmlFile(info.getDataPackage().getSource());
        }

        @Override
        public DefineConversionInfo build() {
            return new DefineConversionInfo(this.dataPackage,
                this.excelFile, this.xmlFile);
        }
    }
}
