package net.pinnacle21.oce.model;

import java.io.File;

public interface CliModel {
    String getEngineVersion();

    File getEngineFolder();
}
