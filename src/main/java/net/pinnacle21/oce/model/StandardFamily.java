package net.pinnacle21.oce.model;

import net.pinnacle21.oce.utils.Valuables;
import org.apache.commons.lang3.StringUtils;

public enum StandardFamily implements Valuable {
    SDTM("SDTM"),
    SEND("SEND"),
    ADaM("ADaM"),
    DEFINE("DEFINE"),
    QRS("QRS"),
    UNDEFINED("undefined");


    private final String value;

    StandardFamily(String value) {
        this.value = value;
    }

    public static StandardFamily fromValue(String value) {
        return Valuables.fromValue(values(), value, UNDEFINED);
    }

    public static boolean isValid(StandardFamily family) {
        return family != null && family != StandardFamily.UNDEFINED;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public boolean isValue(String value) {
        return StringUtils.isNotBlank(value) && (
            StringUtils.containsIgnoreCase(value, this.value) ||
                this.getValue().equalsIgnoreCase(value) ||
                this.name().equalsIgnoreCase(value)
        );
    }
}
