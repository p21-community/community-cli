package net.pinnacle21.oce.model.datapackage;

import net.pinnacle21.oce.model.input.Input;

public interface DataPackageGetter {
    Input<String> get(DataPackage dataPackage);
}
