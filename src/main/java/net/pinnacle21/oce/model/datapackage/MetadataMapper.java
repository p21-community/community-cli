package net.pinnacle21.oce.model.datapackage;

import com.google.common.collect.ImmutableSet;
import net.pinnacle21.oce.model.Valuable;
import net.pinnacle21.oce.model.input.Input;

import java.util.Set;

public interface MetadataMapper extends Valuable {
    public static final String EMPTY = "";
    MetadataMapper UNDEFINED_MAPPER = new MetadataMapper() {
        private static final String VALUE = "UNDEFINED_MAPPER";
        private static final String FOLDER = "UNDEFINED";

        @Override
        public String getValue() {
            return VALUE;
        }

        @Override
        public boolean isValue(String value) {
            return VALUE.equalsIgnoreCase(value);
        }

        @Override
        public Input<String> getVersion(DataPackage dataPackage) {
            return Input.asString("undefined");
        }

        @Override
        public boolean isNumeric() {
            return false;
        }

        @Override
        public DataPackage.Builder setVersion(Input<String> version,
                                              DataPackage.Builder builder) {
            return builder;
        }

        @Override
        public String toFolderName() {
            return FOLDER;
        }

        @Override
        public String toFileName() {
            return FOLDER;
        }

        @Override
        public Set<String> convertFrom() {
            return ImmutableSet.of();
        }

        @Override
        public boolean canDownload() {
            return false;
        }
    };

    Input<String> getVersion(DataPackage dataPackage);

    boolean isNumeric();

    DataPackage.Builder setVersion(Input<String> version, DataPackage.Builder builder);

    String toFolderName();

    String toFileName();

    Set<String> convertFrom();

    boolean canDownload();
}
