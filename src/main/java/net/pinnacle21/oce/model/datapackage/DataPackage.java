package net.pinnacle21.oce.model.datapackage;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.google.common.base.MoreObjects;
import net.pinnacle21.oce.model.BaseModel;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.RuleFilter;
import net.pinnacle21.oce.model.StandardType;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.input.DataSourceInfo;
import net.pinnacle21.oce.model.input.Input;

import java.io.File;

import static net.pinnacle21.oce.utils.DataSources.lookupDir;
import static net.pinnacle21.oce.utils.DataSources.toFile;

/**
 * collects all user input.
 */

@JsonFilter("dataPackageFilter")
public class DataPackage extends BaseModel<DataPackage> {

    private final Input<StandardType> standard;
    private final Input<String> standardVersion;
    private final Input<String> meddraVersion;
    private final Input<String> sdtmCtVersion;
    private final Input<String> qrsCtVersion;
    private final Input<String> adamCtVersion;
    private final Input<String> sendCtVersion;
    private final Input<String> ndfrt;
    private final Input<String> unii;
    private final Input<String> whodrug;
    private final Input<String> snomed;
    private final Input<String> projectName;
    private final Input<String> studyName;
    private final Input<String> groupName;
    private final Input<String> dataPackageName;
    private final DataSourceInfo reportSource;
    private final DataSourceInfo output;
    private final DataSource source;
    private final Input<RuleFilter> standardFilter;
    private final Input<FormatType> sourceFormatType;
    private final Input<String> sourceDelimiter;
    private final Input<String> sourceQualifier;
    private final DataSource define;
    private final Input<Boolean> createAlways;
    private final Input<String> engineVersion;
    private final Input<File> engineFolder;
    private final Input<Boolean> downloadConfig;

    public DataPackage(Input<StandardType> standard,
                       Input<String> standardVersion,
                       Input<String> meddraVersion,
                       Input<String> sdtmCtVersion,
                       Input<String> qrsCtVersion,
                       Input<String> adamCtVersion,
                       Input<String> sendCtVersion,
                       Input<String> ndfrt,
                       Input<String> unii,
                       Input<String> whodrug,
                       Input<String> snomed, Input<String> projectName,
                       Input<String> studyName, Input<String> groupName,
                       Input<String> dataPackageName, DataSourceInfo reportSource,
                       DataSourceInfo output, DataSource source,
                       Input<RuleFilter> standardFilter,
                       Input<FormatType> sourceFormatType,
                       Input<String> sourceDelimiter,
                       Input<String> sourceQualifier,
                       DataSource define,
                       Input<Boolean> createAlways,
                       Input<String> engineVersion,
                       Input<File> engineFolder, Input<Boolean> downloadConfig) {
        this.standard = standard;
        this.standardVersion = standardVersion;
        this.meddraVersion = meddraVersion;
        this.sdtmCtVersion = sdtmCtVersion;
        this.qrsCtVersion = qrsCtVersion;
        this.adamCtVersion = adamCtVersion;
        this.sendCtVersion = sendCtVersion;
        this.ndfrt = ndfrt;
        this.unii = unii;
        this.whodrug = whodrug;
        this.snomed = snomed;
        this.projectName = projectName;
        this.studyName = studyName;
        this.groupName = groupName;
        this.dataPackageName = dataPackageName;
        this.reportSource = reportSource;
        this.output = output;
        this.source = source;
        this.standardFilter = standardFilter;
        this.sourceFormatType = sourceFormatType;
        this.sourceDelimiter = sourceDelimiter;
        this.sourceQualifier = sourceQualifier;
        this.define = define;
        this.createAlways = createAlways;
        this.engineVersion = engineVersion;
        this.engineFolder = engineFolder;
        this.downloadConfig = downloadConfig;
    }

    public Input<StandardType> getStandard() {
        return standard;
    }

    public Input<String> getStandardVersion() {
        return standardVersion;
    }

    public Input<String> getMeddraVersion() {
        return meddraVersion;
    }

    public Input<String> getSdtmCtVersion() {
        return sdtmCtVersion;
    }

    private Input<String> getQrsCtVersion() {
        return qrsCtVersion;
    }

    public Input<String> getSendCtVersion() {
        return sendCtVersion;
    }

    public Input<String> getAdamCtVersion() {
        return adamCtVersion;
    }

    public Input<String> getWhodrug() {
        return whodrug;
    }

    public Input<String> getProjectName() {
        return projectName;
    }

    public Input<String> getStudyName() {
        return studyName;
    }

    private Input<String> getDataPackageName() {
        return dataPackageName;
    }

    public Input<String> getGroupName() {
        return groupName;
    }

    public DataSource getSource() {
        return this.source;
    }

    public DataSourceInfo getReportSource() {
        return reportSource;
    }

    public DataSourceInfo getOutput() {
        return output;
    }

    public Input<String> getNdfrt() {
        return ndfrt;
    }

    public Input<String> getUnii() {
        return unii;
    }

    public Input<String> getSnomed() {
        return snomed;
    }

    public Input<RuleFilter> getStandardFilter() {
        return standardFilter;
    }

    public Input<FormatType> getSourceFormatType() {
        return sourceFormatType;
    }

    public Input<String> getSourceDelimiter() {
        return sourceDelimiter;
    }

    public Input<String> getSourceQualifier() {
        return sourceQualifier;
    }

    public DataSource getDefine() {
        return define;
    }

    private Input<Boolean> isCreateAlways() {
        return createAlways;
    }

    @Override
    public String getEngineVersion() {
        return this.engineVersion.get();
    }

    @Override
    public File getEngineFolder() {
        return this.engineFolder.get();
    }

    public Input<Boolean> getDownloadConfig() {
        return downloadConfig;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("standard", standard)
                .add("standardVersion", standardVersion)
                .add("meddraVersion", meddraVersion)
                .add("sdtmCtVersion", sdtmCtVersion)
                .add("qrsCtVersion", qrsCtVersion)
                .add("adamCtVersion", adamCtVersion)
                .add("sendCtVersion", sendCtVersion)
                .add("ndfrt", ndfrt)
                .add("unii", unii)
                .add("whodrug", whodrug)
                .add("snomed", snomed)
                .add("projectName", projectName)
                .add("studyName", studyName)
                .add("groupName", groupName)
                .add("dataPackageName", dataPackageName)
                .add("reportSource", reportSource)
                .add("source", source)
                .add("standardFilter", standardFilter)
                .add("sourceFormatType", sourceFormatType)
                .add("sourceDelimiter", sourceDelimiter)
                .add("sourceQualifier", sourceQualifier)
                .add("define", define)
                .add("createAlways", this.createAlways)
                .add("engineVersion", this.engineVersion)
                .add("engineFolder", this.engineFolder.describe())
                .add("downloadConfig", this.downloadConfig)
                .omitNullValues()
                .toString();
    }

    public static class Builder extends BaseModel.Builder<Builder, DataPackage> {
        private Input<StandardType> standard;
        private Input<String> standardVersion;
        private Input<String> meddraVersion;
        private Input<String> sdtmCtVersion;
        private Input<String> sendCtVersion;
        private Input<String> qrsCtVersion;
        private Input<String> adamCtVersion;
        private Input<String> ndfrt;
        private Input<String> unii;
        private Input<String> snomed;
        private Input<String> whodrug;
        private Input<String> projectName;
        private Input<String> studyName;
        private Input<String> groupName;
        private Input<String> dataPackageName;
        private Input<RuleFilter> standardFilter;
        private DataSourceInfo reportSource;
        private DataSource source;
        private DataSourceInfo output;

        private Input<FormatType> sourceFormatType;
        private Input<String> sourceDelimiter;
        private Input<String> sourceQualifier;
        private DataSource define;
        private Input<Boolean> createAlways;
        private Input<String> engineVersion;
        private Input<File> engineFolder;
        private Input<Boolean> downloadConfig;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public static Builder from(DataPackage otherPackage) {
            return builder()
                    .setSendCtVersion(otherPackage.getSendCtVersion())
                    .setAdamCtVersion(otherPackage.getAdamCtVersion())
                    .setDefine(otherPackage.getDefine())
                    .setGroupName(otherPackage.getGroupName())
                    .setDataPackageName(otherPackage.getDataPackageName())
                    .setMeddraVersion(otherPackage.getMeddraVersion())
                    .setNdfrt(otherPackage.getNdfrt())
                    .setProjectName(otherPackage.getProjectName())
                    .setQrsCtVersion(otherPackage.getQrsCtVersion())
                    .setReportSource(otherPackage.getReportSource())
                    .setSdtmCtVersion(otherPackage.getSdtmCtVersion())
                    .setSnomed(otherPackage.getSnomed())
                    .setSource(otherPackage.getSource())
                    .setOutput(otherPackage.getOutput())
                    .setSourceDelimiter(otherPackage.getSourceDelimiter())
                    .setSourceFormatType(otherPackage.getSourceFormatType())
                    .setSourceQualifier(otherPackage.getSourceQualifier())
                    .setStandard(otherPackage.getStandard())
                    .setStandardFilter(otherPackage.getStandardFilter())
                    .setStandardVersion(otherPackage.getStandardVersion())
                    .setStudyName(otherPackage.getStudyName())
                    .setUnii(otherPackage.getUnii())
                    .setWhodrug(otherPackage.getWhodrug())
                    .setCreateAlways(otherPackage.isCreateAlways())
                    .setDownloadConfig(otherPackage.getDownloadConfig())
                    .setEngineVersion(otherPackage.engineVersion)
                    .setEngineFolder(otherPackage.engineFolder);
        }

        @Override
        protected Builder getThis() {
            return this;
        }

        public Builder setStandard(Input<StandardType> standard) {
            this.standard = Input.safe(standard, this.standard);
            return this;
        }

        public Builder setStandardVersion(Input<String> standardVersion) {
            this.standardVersion = Input.safe(standardVersion, this.standardVersion);
            return this;
        }

        public Builder setProjectName(Input<String> projectName) {
            this.projectName = Input.safe(projectName, this.projectName);
            return this;
        }

        Builder setDataPackageName(Input<String> dataPackageName) {
            this.dataPackageName = Input.safe(dataPackageName, this.dataPackageName);
            return this;
        }

        public Builder setMeddraVersion(Input<String> meddraVersion) {
            this.meddraVersion = Input.safe(meddraVersion, this.meddraVersion);
            return this;
        }

        public Builder setSdtmCtVersion(Input<String> sdtmCtVersion) {
            this.sdtmCtVersion = Input.safe(sdtmCtVersion, this.sdtmCtVersion);
            return this;
        }

        public Builder setNdfrt(Input<String> ndfrt) {
            this.ndfrt = Input.safe(ndfrt, this.ndfrt);
            return this;
        }

        public Builder setUnii(Input<String> unii) {
            this.unii = Input.safe(unii, this.unii);
            return this;
        }

        public Builder setSnomed(Input<String> snomed) {
            this.snomed = Input.safe(snomed, this.snomed);
            return this;
        }

        public Builder setWhodrug(Input<String> whodrug) {
            this.whodrug = Input.safe(whodrug, this.whodrug);
            return this;
        }

        Builder setQrsCtVersion(Input<String> qrsCtVersion) {
            this.qrsCtVersion = Input.safe(qrsCtVersion, this.qrsCtVersion);
            return this;
        }

        public Builder setAdamCtVersion(Input<String> adamCtVersion) {
            this.adamCtVersion = Input.safe(adamCtVersion, this.adamCtVersion);
            return this;
        }

        public Builder setSendCtVersion(Input<String> sendCtVersion) {
            this.sendCtVersion = Input.safe(sendCtVersion, this.sendCtVersion);
            return this;
        }

        public Builder setReportSource(DataSourceInfo reportSource) {
            this.reportSource = reportSource;
            return this;
        }

        public Builder setOutput(DataSourceInfo output) {
            this.output = output;
            return this;
        }

        public Builder setStandardFilter(Input<RuleFilter> standardFilter) {
            this.standardFilter = Input.safe(standardFilter, this.standardFilter);
            return this;
        }

        public Builder setStudyName(Input<String> studyName) {
            this.studyName = Input.safe(studyName, this.studyName);
            return this;
        }

        public Builder setDownloadConfig(Input<Boolean> downloadConfig) {
            this.downloadConfig = downloadConfig;
            return this;
        }

        public Builder setGroupName(Input<String> groupName) {
            this.groupName = Input.safe(groupName, this.groupName);
            return this;
        }

        public Builder setSource(DataSource source) {
            this.source = source;
            return this;
        }

        public Builder setSourceFormatType(Input<FormatType> sourceFormatType) {
            this.sourceFormatType = Input.safe(sourceFormatType, this.sourceFormatType);
            return this;
        }

        public Builder setSourceDelimiter(Input<String> sourceDelimiter) {
            this.sourceDelimiter = Input.safe(sourceDelimiter, this.sourceDelimiter);
            return this;
        }

        public Builder setSourceQualifier(Input<String> sourceQualifier) {
            this.sourceQualifier = Input.safe(sourceQualifier, this.sourceQualifier);
            return this;
        }

        public Builder setDefine(DataSource define) {
            this.define = define;
            return this;
        }

        Builder setCreateAlways(Input<Boolean> createAlways) {
            this.createAlways = Input.safe(createAlways, this.createAlways);
            return this;
        }

        public Builder setEngineVersion(Input<String> engineVersion) {
            this.engineVersion = engineVersion;
            return this;
        }

        public Builder setEngineFolder(Input<File> engineFolder) {
            this.engineFolder = engineFolder;
            return this;
        }

        @Override
        public DataPackage build() {
            return buildProject();
        }

        private DataPackage buildProject() {
            if (engineFolder == null || !engineFolder.isProvided()) {
                engineFolder = Input.asCalculated(toFile(lookupDir("lib", true)));
            }

            return new DataPackage(
                    Input.safe(Input.asDefault(StandardType.UNDEFINED), this.standard),
                    Input.safe(Input.emptyString(), this.standardVersion),
                    Input.safe(Input.emptyString(), this.meddraVersion),
                    Input.safe(Input.emptyString(), this.sdtmCtVersion),
                    Input.safe(Input.emptyString(), this.qrsCtVersion),
                    Input.safe(Input.emptyString(), this.adamCtVersion),
                    Input.safe(Input.emptyString(), this.sendCtVersion),
                    Input.safe(Input.emptyString(), this.ndfrt),
                    Input.safe(Input.emptyString(), this.unii),
                    Input.safe(Input.emptyString(), this.whodrug),
                    Input.safe(Input.emptyString(), this.snomed),
                    Input.safe(Input.emptyString(), this.projectName),
                    Input.safe(Input.emptyString(), studyName),
                    Input.safe(Input.emptyString(), groupName),
                    Input.safe(Input.emptyString(), this.dataPackageName),
                    reportSource,
                    output, source,
                    Input.safe(Input.asDefault(RuleFilter.ALL), standardFilter),
                    Input.safe(Input.asDefault(FormatType.UNKNOWN_FORMAT), sourceFormatType),
                    Input.safe(Input.emptyString(), sourceDelimiter),
                    Input.safe(Input.emptyString(), sourceQualifier),
                    define,
                    Input.safe(Input.asDefault(true), createAlways),
                    Input.safe(Input.emptyString(), engineVersion),
                    engineFolder, Input.safe(Input.asDefault(false), downloadConfig)
            );
        }
    }
}
