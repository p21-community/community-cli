package net.pinnacle21.oce.model.datapackage;

import com.google.common.base.MoreObjects;
import net.pinnacle21.oce.model.BaseModel;
import net.pinnacle21.oce.model.input.ConfigInfo;

import java.io.File;

public class LocalDataPackage extends BaseModel<LocalDataPackage> {
    private final DataPackage dataPackage;
    private final ConfigInfo config;
    private final int threadCount;
    private final boolean generateRuleMetrics;
    private final boolean useEnhancedSas;
    private final boolean autoDisplayDomainKeys;
    private final String userId;
    private final String anonymousId;
    private final String applicationVersion;

    private LocalDataPackage(Builder builder) {
        this.dataPackage = builder.dataPackage;
        this.config = builder.config;
        this.threadCount = builder.threadCount;
        this.generateRuleMetrics = builder.generateRuleMetrics;
        this.useEnhancedSas = builder.useEnhancedSas;
        this.autoDisplayDomainKeys = builder.autoDisplayDomainKeys;
        this.userId = builder.userId;
        this.anonymousId = builder.anonymousId;
        this.applicationVersion = builder.applicationVersion;
    }

    public DataPackage getDataPackage() {
        return dataPackage;
    }

    public ConfigInfo getConfigInfo() {
        return config;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public boolean isGenerateRuleMetrics() {
        return generateRuleMetrics;
    }

    public boolean isUseEnhancedSas() {
        return useEnhancedSas;
    }

    public boolean isAutoDisplayDomainKeys() {
        return autoDisplayDomainKeys;
    }

    public String getUserId() {
        return userId;
    }

    public String getAnonymousId() {
        return anonymousId;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    @Override
    public String getEngineVersion() {
        return this.dataPackage.getEngineVersion();
    }

    @Override
    public File getEngineFolder() {
        return this.dataPackage.getEngineFolder();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("dataPackage", dataPackage)
            .add("config", config)
            .add("threadCount", threadCount)
            .toString();
    }

    public static class Builder extends BaseModel.Builder<LocalDataPackage.Builder, LocalDataPackage> {
        private DataPackage dataPackage;
        private ConfigInfo config;
        private int threadCount;
        private boolean generateRuleMetrics;
        private boolean useEnhancedSas;
        private boolean autoDisplayDomainKeys;
        private String userId;
        private String anonymousId;
        private String applicationVersion;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public LocalDataPackage.Builder setDataPackage(DataPackage dataPackage) {
            this.dataPackage = dataPackage;
            return getThis();
        }

        public LocalDataPackage.Builder setConfig(ConfigInfo config) {
            this.config = config;
            return getThis();
        }

        public LocalDataPackage.Builder setThreadCount(int threadCount) {
            this.threadCount = threadCount;
            return this;
        }

        public Builder setGenerateRuleMetrics(boolean generateRuleMetrics) {
            this.generateRuleMetrics = generateRuleMetrics;
            return this;
        }

        public Builder setUseEnhancedSas(boolean useEnhancedSas) {
            this.useEnhancedSas = useEnhancedSas;
            return this;
        }

        public Builder setAutoDisplayDomainKeys(boolean autoDisplayDomainKeys) {
            this.autoDisplayDomainKeys = autoDisplayDomainKeys;
            return this;
        }

        public Builder setUserId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder setAnonymousId(String anonymousId) {
            this.anonymousId = anonymousId;
            return this;
        }

        public Builder setApplicationVersion(String version) {
            this.applicationVersion = version;
            return this;
        }

        @Override
        protected LocalDataPackage.Builder getThis() {
            return this;
        }

        @Override
        public LocalDataPackage build() {
            return new LocalDataPackage(this);
        }
    }
}
