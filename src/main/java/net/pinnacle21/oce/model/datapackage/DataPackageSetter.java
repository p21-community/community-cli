package net.pinnacle21.oce.model.datapackage;

import net.pinnacle21.oce.model.input.Input;

public interface DataPackageSetter {
    DataPackage.Builder set(Input<String> value, DataPackage.Builder builder);
}
