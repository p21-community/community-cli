package net.pinnacle21.oce.model;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import joptsimple.ValueConverter;
import net.bootstrap.core.model.FileType;
import net.pinnacle21.oce.utils.Valuables;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.TreeSet;

public enum ReportType implements Valuable {
    EXCEL(FileType.EXCEL, "excel", "xls", "xlsx", "default"),
    OUTCOMES(FileType.EXCEL, "outcomes"),
    SITES(FileType.EXCEL, "sites"),
    UNKNOWN_REPORT(null, "unknown_report");

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportType.class);
    private static final DateTimeFormatter DEFAULT_FORMAT = DateTimeFormat.forPattern("y-MM-dd'T'HH-mm");

    private final String value;
    private final FileType bootstrapType;
    private final Set<String> allValues = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

    ReportType(FileType bootstrapType, String... values) {
        this.bootstrapType = bootstrapType;
        this.value = values[0];
        this.allValues.addAll(ImmutableList.copyOf(values));
    }

    public static ReportType fromValue(String value) {
        return Valuables.fromValue(values(), value,
            ReportType.EXCEL);
    }

    public static ValueConverter<ReportType> converter() {
        return new ValueConverter<ReportType>() {
            @Override
            public ReportType convert(String value) {
                return ReportType.fromValue(Strings.nullToEmpty(value).trim());
            }

            @Override
            public Class<? extends ReportType> valueType() {
                return ReportType.class;
            }

            @Override
            public String valuePattern() {
                return null;
            }
        };
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public boolean isValue(String value) {
        return this.name().equalsIgnoreCase(value) ||
            this.allValues.contains(value);
    }

    public FileType getBootstrapType() {
        return bootstrapType;
    }

    public String toDefaultFileName(String base) {
        String dateTime = DEFAULT_FORMAT.print(DateTime.now().toInstant());
        return base + "-" + dateTime + ".xlsx";
    }
}
