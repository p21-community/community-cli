package net.pinnacle21.oce.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.common.base.MoreObjects;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datasource.DataSource;

import java.io.File;

@JsonDeserialize(builder = InstallInfo.Builder.class)
public class InstallInfo extends BaseModel<InstallInfo> {
    private final DataPackage dataPackage;
    private final DataSource config;
    private final boolean clean;

    InstallInfo(Builder builder) {
        this.dataPackage = builder.dataPackage;
        this.config = builder.config;
        this.clean = builder.clean;
    }

    public DataPackage getDataPackage() {
        return dataPackage;
    }

    public DataSource getConfig() {
        return config;
    }

    public boolean isClean() {
        return clean;
    }

    @Override
    public String getEngineVersion() {
        return this.dataPackage.getEngineVersion();
    }

    @Override
    public File getEngineFolder() {
        return this.dataPackage.getEngineFolder();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("dataPackage", dataPackage)
            .add("config", config)
            .toString();
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class Builder extends BaseModel.Builder<Builder, InstallInfo> {
        private DataPackage dataPackage;
        private DataSource config;
        private boolean clean;

        protected Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public static Builder from(InstallInfo otherInfo) {
            return builder()
                .setDataPackage(otherInfo.getDataPackage())
                .setConfig(otherInfo.config);
        }

        @Override
        protected Builder getThis() {
            return this;
        }

        public Builder setDataPackage(DataPackage dataPackage) {
            this.dataPackage = dataPackage;
            return getThis();
        }

        public Builder setConfig(DataSource config) {
            this.config = config;
            return getThis();
        }

        public Builder setClean(boolean clean) {
            this.clean = clean;
            return this;
        }

        @Override
        public InstallInfo build() {
            return new InstallInfo(getThis());
        }
    }
}
