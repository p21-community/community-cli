package net.pinnacle21.oce.model;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum ReleaseLevel implements Valuable {
    SNAPSHOT("snapshot"),
    PRE_RELEASE("pre-release"),
    RELEASE("release");

    private final static Logger LOGGER = LoggerFactory.getLogger(ReleaseLevel.class);
    private final static Pattern UNSUPPORTED_PATTERN = Pattern.compile("^\\d+\\.\\d+(?:\\.\\d+)?(?:-(.+))?$");

    private final String value;

    ReleaseLevel(String value) {
        this.value = value;
    }

    public static ReleaseLevel fromValue(String value) {
        Matcher matcher = UNSUPPORTED_PATTERN.matcher(value);
        if (matcher.find() && matcher.groupCount() > 0) {
            String suffix = matcher.group(1);
            if (StringUtils.containsIgnoreCase(suffix, "pre-release")) {
                return PRE_RELEASE;
            } else if (StringUtils.containsIgnoreCase(suffix, "snapshot")) {
                return SNAPSHOT;
            } else if (StringUtils.isEmpty(suffix)) {
                return RELEASE;
            } else {
                LOGGER.error(
                    "Unsupported version suffix '{}' found in string '{}'. Defaulting to SNAPSHOT",
                    suffix,
                    value
                );
                return SNAPSHOT;
            }
        } else {
            LOGGER.error("Unable to parse release type from string '{}'. Defaulting to SNAPSHOT.", value);
            return SNAPSHOT;
        }
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public boolean isValue(String value) {
        return this.name().equalsIgnoreCase(value) ||
            this.value.equalsIgnoreCase(value) ||
            StringUtils.containsIgnoreCase(value, this.value);
    }
}
