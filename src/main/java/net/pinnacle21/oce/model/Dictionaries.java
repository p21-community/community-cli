package net.pinnacle21.oce.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.DataExceptions;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datapackage.DataPackageGetter;
import net.pinnacle21.oce.model.datapackage.DataPackageSetter;
import net.pinnacle21.oce.model.datapackage.MetadataMapper;
import net.pinnacle21.oce.model.input.Input;
import net.pinnacle21.oce.utils.Valuables;

import java.util.EnumSet;
import java.util.Set;
import java.util.TreeSet;

public enum Dictionaries implements MetadataMapper {
    MedDRA("MedDRA", new DataPackageGetter() {
        @Override
        public Input<String> get(DataPackage dataPackage) {
            return dataPackage.getMeddraVersion();
        }
    }, new DataPackageSetter() {
        @Override
        public DataPackage.Builder set(Input<String> value, DataPackage.Builder builder) {
            return builder.setMeddraVersion(value);
        }
    }, true, EnumSet.of(StandardFamily.ADaM, StandardFamily.SDTM),
        ImmutableSet.<String>of(".*\\.zip", "^soc\\.asc", "^pt\\.asc", "^llt\\.asc", "^hlt\\.asc", "^hlgt\\.asc"), false),
    SNOMED("SNOMED", new DataPackageGetter() {
        @Override
        public Input<String> get(DataPackage dataPackage) {
            return dataPackage.getSnomed();
        }
    }, new DataPackageSetter() {
        @Override
        public DataPackage.Builder set(Input<String> value, DataPackage.Builder builder) {
            return builder.setSnomed(value);
        }
    }, false, EnumSet.of(StandardFamily.SDTM),
        ImmutableSet.of("(?i)(sct2_Description_Full-en)(.*)(\\.txt)",
            ".*\\.zip"), false),
    UNII("UNII", new DataPackageGetter() {
        @Override
        public Input<String> get(DataPackage dataPackage) {
            return dataPackage.getUnii();
        }
    }, new DataPackageSetter() {
        @Override
        public DataPackage.Builder set(Input<String> value, DataPackage.Builder builder) {
            return builder.setUnii(value);
        }
    }, EnumSet.of(StandardFamily.SDTM)),
    NDF_RT("NDF-RT", new DataPackageGetter() {
        @Override
        public Input<String> get(DataPackage dataPackage) {
            return dataPackage.getNdfrt();
        }
    }, new DataPackageSetter() {
        @Override
        public DataPackage.Builder set(Input<String> value, DataPackage.Builder builder) {
            return builder.setNdfrt(value);
        }
    }, EnumSet.of(StandardFamily.SDTM), "NDFRT", "NDF RT"),
    WHODRUG("WHODRUG", new DataPackageGetter() {
        @Override
        public Input<String> get(DataPackage dataPackage) {
            return dataPackage.getWhodrug();
        }
    }, new DataPackageSetter() {
        @Override
        public DataPackage.Builder set(Input<String> value, DataPackage.Builder builder) {
            return builder.setWhodrug(value);
        }
    }, EnumSet.of(StandardFamily.SDTM));

    private final String value;
    private final DataPackageGetter versionGetter;
    private final DataPackageSetter versionSetter;
    private final boolean numeric;
    private final Set<String> allValues = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
    private final Set<StandardFamily> standards;
    private final Set<String> convertFrom;
    private final boolean canDownload;

    Dictionaries(String value,
                 DataPackageGetter versionGetter,
                 DataPackageSetter versionSetter,
                 Set<StandardFamily> standards,
                 String... values) {
        this(value, versionGetter, versionSetter, false, standards,
            ImmutableSet.<String>of(), true, values);
    }

    Dictionaries(String value,
                 DataPackageGetter versionGetter,
                 DataPackageSetter versionSetter,
                 boolean numeric,
                 Set<StandardFamily> standards,
                 Set<String> convertFrom,
                 boolean canDownload,
                 String... values) {
        this.value = value;
        this.versionGetter = versionGetter;
        this.versionSetter = versionSetter;
        this.numeric = numeric;
        this.standards = standards;
        this.convertFrom = convertFrom;
        this.canDownload = canDownload;
        if (values != null) {
            this.allValues.addAll(ImmutableList.copyOf(values));
        }
    }

    @JsonCreator
    public static Dictionaries fromValue(String value) {
        return Valuables.fromValue(values(), value,
            DataExceptions.NOTFOUND_DICTIONARY);
    }

    public static MetadataMapper toMapper(String value) {
        try {
            return fromValue(value);
        } catch (CLIException ex) {
            if (ex.getCode() == DataExceptions.NOTFOUND_DICTIONARY) {
                return null;
            }
            throw ex;
        }
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public boolean isValue(String value) {
        return this.name().equalsIgnoreCase(value) ||
            Strings.nullToEmpty(this.value).equalsIgnoreCase(value) ||
            this.allValues.contains(value);
    }

    @Override
    public Set<String> convertFrom() {
        return this.convertFrom;
    }

    @Override
    public boolean canDownload() {
        return this.canDownload;
    }

    @Override
    public Input<String> getVersion(DataPackage dataPackage) {
        return this.versionGetter.get(dataPackage);
    }

    @Override
    public DataPackage.Builder setVersion(Input<String> version, DataPackage.Builder builder) {
        return this.versionSetter.set(version, builder);
    }

    @Override
    public boolean isNumeric() {
        return numeric;
    }

    @Override
    public String toFileName() {
        return this.getValue();
    }

    @Override
    public String toFolderName() {
        return "";
    }

    public boolean containStandard(StandardFamily family) {
        return this.standards.contains(family);
    }

}
