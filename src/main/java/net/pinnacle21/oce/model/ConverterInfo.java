package net.pinnacle21.oce.model;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import net.bootstrap.core.model.FileType;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.input.ConfigInfo;
import net.pinnacle21.oce.model.input.Input;
import net.pinnacle21.oce.utils.DataSources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Collection;
import java.util.List;

import static net.pinnacle21.oce.utils.CliPreconditions.*;
import static net.pinnacle21.oce.utils.DataPackageUtils.getSourcePath;

@JsonFilter("converterInfoFilter")
public class ConverterInfo extends BaseModel<ConverterInfo> {
    private final Collection<DataSource> sources;
    private final DataSource define;
    private final ConfigInfo config;
    private final DataSource output;

    private final FormatType outputFormatType;
    private final FormatType sourceFormatType;

    private final DataPackage dataPackage;

    private ConverterInfo(Collection<DataSource> sources,
                          DataSource define,
                          ConfigInfo config,
                          DataSource output,
                          FormatType outputFormatType,
                          FormatType sourceFormatType, DataPackage dataPackage) {
        this.sources = sources;
        this.define = define;
        this.config = config;
        this.output = output;
        this.outputFormatType = outputFormatType;
        this.sourceFormatType = sourceFormatType;
        this.dataPackage = dataPackage;
    }

    public Collection<DataSource> getSources() {
        return sources;
    }

    public DataSource getDefine() {
        return define;
    }

    public ConfigInfo getConfig() {
        return config;
    }

    public DataSource getOutput() {
        return output;
    }

    public FormatType getOutputFormatType() {
        return outputFormatType;
    }

    public FormatType getSourceFormatType() {
        return sourceFormatType;
    }

    public DataPackage getDataPackage() {
        return dataPackage;
    }

    @Override
    public String getEngineVersion() {
        return this.dataPackage.getEngineVersion();
    }

    @Override
    public File getEngineFolder() {
        return this.dataPackage.getEngineFolder();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
            .add("sources", Joiner.on('\n').join(this.sources))
            .add("define", this.define)
            .add("config", this.config)
            .add("output", this.output)
            .add("outputFormat", this.outputFormatType.getValue())
            .add("dataPackage", this.dataPackage)
            .toString();
    }

    public static class Builder extends BaseModel.Builder<ConverterInfo.Builder, ConverterInfo> {
        private static final Logger LOGGER = LoggerFactory.getLogger(ConverterInfo.class);

        private List<DataSource> sources;
        private DataSource define;
        private ConfigInfo config;
        private DataSource output;
        private Input<FormatType> outputFormatType;
        private Input<FormatType> sourceFormatType;
        private DataPackage dataPackage;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        @Override
        protected ConverterInfo.Builder getThis() {
            return this;
        }

        public Builder setSources(List<DataSource> sources) {
            this.sources = sources;
            return getThis();
        }

        public Builder setDefine(DataSource define) {
            this.define = define;
            return getThis();
        }

        public Builder setConfig(ConfigInfo config) {
            this.config = config;
            return getThis();
        }

        public Builder setOutput(DataSource output) {
            this.output = output;
            return getThis();
        }

        public Builder setOutputFormatType(Input<FormatType> outputFormatType) {
            this.outputFormatType = outputFormatType;
            return getThis();
        }

        public Builder setDataPackage(DataPackage dataPackage) {
            this.dataPackage = dataPackage;
            return getThis();
        }

        public Builder setSourceFormatType(Input<FormatType> sourceFormatType) {
            this.sourceFormatType = sourceFormatType;
            return getThis();
        }

        @Override
        public ConverterInfo build() {
            this.config = checkConfig();
            this.outputFormatType = checkOutput();
            checkProvided(this.output, ValidationExceptions.REQUIRED_OUTPUT,
                "Output must be provided for converter or define generator");

            checkState(DataSources.makeDataSource(this.output),
                SystemExceptions.FAILED_OUTPUT_DIR,
                String.format("Failed to create output folder=%s",
                    DataSources.toFolder(this.output).getAbsolutePath()));

            if (this.outputFormatType.get().getBootstrapType() == FileType.DEFINE &&
                DataSources.isDirectory(this.output)) {
                // need to generate define xml or define spec but output is a folder.
                // default output to define file
                this.output = DataSources.newFileOrFolder(this.output,
                    this.outputFormatType.get().toDefaultFileName("define"));
            }

            return new ConverterInfo(
                checkCollection(this.sources, ValidationExceptions.REQURED_DATASOURCE,
                    String.format("Data converter or define generator request is missing data source or no files found in path=%s",
                        getSourcePath(this.dataPackage))),
                this.define,
                this.config,
                this.output,
                this.outputFormatType.get(),
                sourceFormatType.get(), this.dataPackage);
        }

        private Input<FormatType> checkOutput() {
            if (this.outputFormatType.isEmpty()) {
                return Input.asCalculated(FormatType.EXCEL);
            }

            return this.outputFormatType;
        }

        private ConfigInfo checkConfig() {
            if (this.outputFormatType.get() == FormatType.DEFINE_SPEC) {
                if (this.sourceFormatType.get() == FormatType.XPORT) {
                    // generate define. must have config
                    exists(
                            DataSources.toFile(this.config.getConfig()),
                            ValidationExceptions.REQUIRED_CONFIG,
                            "Config is required and must exist"
                    );
                }
            } else if (this.outputFormatType.get() == FormatType.DATASET_XML) {
                // need config to generate Dataset-XML
                if (this.config == null || !this.config.isValid()) {
                    if (DataSources.isValid(this.define)) {
                        // no config but define is provided. Will use provided define
                        return null;
                    }

                    DataSource configDS = this.config == null ? DataSource.EMPTY : this.config.getConfig();
                    throw new CLIException(ValidationExceptions.REQUIRED_GENERAL,
                        String.format("Data converter cannot proceed conversion to '%s' without valid config data or define file. Config='%s' Define='%s'",
                            FormatType.DATASET_XML.getValue(),
                            configDS.getAbsolutePath(),
                            Optional.fromNullable(this.define).or(DataSource.EMPTY).getAbsolutePath()));
                }
            }

            return this.config;
        }
    }
}
