package net.pinnacle21.oce.model.community;

public class CommunityMiner extends CommunityGenerator {
    private String reportType;

    public String getReportType() {
        return reportType;
    }

    public CommunityMiner setReportType(String reportType) {
        this.reportType = reportType;
        return this;
    }
}
