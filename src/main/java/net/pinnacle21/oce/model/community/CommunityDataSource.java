package net.pinnacle21.oce.model.community;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.support.FormatTypeDeserializer;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CommunityDataSource {
    private List<String> paths;

    @JsonDeserialize(using = FormatTypeDeserializer.class)
    private FormatType type;

    private String delimiter;
    private String qualifier;

    public List<String> getPaths() {
        return paths;
    }

    public void setPaths(List<String> paths) {
        this.paths = paths;
    }

    public FormatType getType() {
        return type;
    }

    public void setType(FormatType type) {
        this.type = type;
    }

    public String getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    public String getQualifier() {
        return qualifier;
    }

    public void setQualifier(String qualifier) {
        this.qualifier = qualifier;
    }
}
