package net.pinnacle21.oce.model.community;

public class CommunityGenerator extends BaseCommunityModel {
    private String config;

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }
}
