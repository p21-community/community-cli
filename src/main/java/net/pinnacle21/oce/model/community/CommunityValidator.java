package net.pinnacle21.oce.model.community;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CommunityValidator extends BaseCommunityModel {
    private CommunityConfig config;
    private CommunityReport report;
    @JsonProperty("isDefine")
    private boolean isDefine;
    private int threads;

    @JsonProperty("generateRuleMetrics")
    private boolean generateRuleMetrics;

    @JsonProperty("useEnhancedSas")
    private boolean useEnhancedSas;

    @JsonProperty("autoDisplayDomainKeys")
    private boolean autoDisplayDomainKeys;

    public CommunityConfig getConfig() {
        return config;
    }

    public void setConfig(CommunityConfig config) {
        this.config = config;
    }

    public CommunityReport getReport() {
        return report;
    }

    public void setReport(CommunityReport report) {
        this.report = report;
    }

    public boolean isDefine() {
        return isDefine;
    }

    public void setDefine(boolean define) {
        isDefine = define;
    }

    public int getThreads() {
        return threads;
    }

    public void setThreads(int threads) {
        this.threads = threads;
    }

    public boolean isGenerateRuleMetrics() {
        return generateRuleMetrics;
    }

    public CommunityValidator setGenerateRuleMetrics(boolean generateRuleMetrics) {
        this.generateRuleMetrics = generateRuleMetrics;
        return this;
    }

    public boolean isUseEnhancedSas() {
        return useEnhancedSas;
    }

    public CommunityValidator setUseEnhancedSas(boolean useEnhancedSas) {
        this.useEnhancedSas = useEnhancedSas;
        return this;
    }

    public boolean isAutoDisplayDomainKeys() {
        return autoDisplayDomainKeys;
    }

    public CommunityValidator setAutoDisplayDomainKeys(boolean autoDisplayDomainKeys) {
        this.autoDisplayDomainKeys = autoDisplayDomainKeys;
        return this;
    }
}
