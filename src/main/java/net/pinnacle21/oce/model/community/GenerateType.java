package net.pinnacle21.oce.model.community;

import net.pinnacle21.oce.model.Valuable;
import net.pinnacle21.oce.utils.Valuables;

public enum GenerateType implements Valuable {
    SPEC,
    DEFINE,
    NA;

    public static GenerateType fromValue(String value) {
        return Valuables.fromValue(values(), value, NA);
    }

    @Override
    public String getValue() {
        return this.name();
    }

    @Override
    public boolean isValue(String value) {
        return getValue().equalsIgnoreCase(value);
    }
}
