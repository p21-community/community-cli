package net.pinnacle21.oce.model.community;

public class CommunityConverter extends BaseCommunityModel {
    private String define;

    public String getDefine() {
        return define;
    }

    public void setDefine(String define) {
        this.define = define;
    }
}
