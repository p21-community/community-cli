package net.pinnacle21.oce.model.community;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.support.ActionDeserializer;
import net.pinnacle21.oce.model.support.FormatTypeDeserializer;
import net.pinnacle21.oce.model.support.GenerateTypeDeserializer;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "action",
    include = JsonTypeInfo.As.EXISTING_PROPERTY, visible = true)
@JsonSubTypes(value = {
    @JsonSubTypes.Type(value = CommunityConverter.class, name = "CONVERT_DATA"),
    @JsonSubTypes.Type(value = CommunityListConfig.class, name = "LIST_CONFIG"),
    @JsonSubTypes.Type(value = CommunityGenerator.class, name = "CREATE_SPEC"),
    @JsonSubTypes.Type(value = CommunityGenerator.class, name = "GENERATE_DEFINE"),
    @JsonSubTypes.Type(value = CommunityMiner.class, name = "MINER"),
    @JsonSubTypes.Type(value = CommunityValidator.class, name = "VALIDATE_DATA")
})
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseCommunityModel {
    @JsonDeserialize(using = ActionDeserializer.class)
    private Action action;

    @JsonDeserialize(using = GenerateTypeDeserializer.class)
    private GenerateType generate;

    private String target;

    @JsonDeserialize(using = FormatTypeDeserializer.class)
    @JsonProperty("type")
    private FormatType outputFormat;

    private CommunityDataSource sources;

    private String engineVersion;

    private String engineFolder;

    private Boolean online;

    private String userId;

    private String anonymousId;

    private String applicationVersion;

    protected BaseCommunityModel() {
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public FormatType getOutputFormat() {
        return outputFormat;
    }

    public void setOutputFormat(FormatType outputFormat) {
        this.outputFormat = outputFormat;
    }

    public CommunityDataSource getSources() {
        return sources;
    }

    public void setSources(CommunityDataSource sources) {
        this.sources = sources;
    }

    public GenerateType getGenerate() {
        return generate;
    }

    public void setGenerate(GenerateType generate) {
        this.generate = generate;
    }

    public String getEngineVersion() {
        return engineVersion;
    }

    public BaseCommunityModel setEngineVersion(String engineVersion) {
        this.engineVersion = engineVersion;
        return this;
    }

    public String getEngineFolder() {
        return engineFolder;
    }

    public BaseCommunityModel setEngineFolder(String engineFolder) {
        this.engineFolder = engineFolder;
        return this;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String id) {
        userId = id;
    }

    public String getAnonymousId() {
        return anonymousId;
    }

    public void setAnonymousId(String id) {
        anonymousId = id;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public void setApplicationVersion(String version) {
        applicationVersion = version;
    }
}
