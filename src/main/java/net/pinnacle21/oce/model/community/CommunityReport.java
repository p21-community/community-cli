package net.pinnacle21.oce.model.community;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import net.pinnacle21.oce.model.ReportType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CommunityReport {
    private String path;
    private int messageLimit;
    private ReportType reportType;

    public CommunityReport() {
    }

    public CommunityReport(String path, int messageLimit, ReportType reportType) {
        this.path = path;
        this.messageLimit = messageLimit;
        this.reportType = reportType;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getMessageLimit() {
        return messageLimit;
    }

    public void setMessageLimit(int messageLimit) {
        this.messageLimit = messageLimit;
    }

    public ReportType getReportType() {
        return reportType;
    }

    public CommunityReport setReportType(ReportType reportType) {
        this.reportType = reportType;
        return this;
    }
}
