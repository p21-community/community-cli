package net.pinnacle21.oce.model.community;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import net.pinnacle21.oce.model.ReportType;
import net.pinnacle21.oce.model.StandardType;
import net.pinnacle21.oce.model.Valuable;
import net.pinnacle21.oce.utils.Constants;
import net.pinnacle21.oce.utils.Valuables;

import java.io.File;
import java.util.Map;

import static net.pinnacle21.oce.utils.DataSources.lookupDir;
import static net.pinnacle21.oce.utils.DataSources.toFile;

public enum Action implements Valuable {
    CONVERT_DATA("CONVERT_DATA", new Getter<File>() {
        @Override
        public File get(BaseCommunityModel model) {
            return toFile(lookupDir("config", false));
        }
    }, Shared.UNDEFINED_STANDARD, new Getter<String>() {
        @Override
        public String get(BaseCommunityModel model) {
            return ((CommunityConverter) model).getDefine();
        }
    }, Shared.EMPTY_VERSION_MAP, Shared.TARGET_REPORT,
        Shared.DEFAULT_THREADS,
        Shared.DEFAULT_GENERATE_RULE_METRICS,
        Shared.DEFAULT_USE_ENHANCED_SAS,
        Shared.DEFAULT_AUTO_DISPLAY_DOMAIN_KEYS),

    CREATE_SPEC("CREATE_SPEC", Shared.GENERATOR_CONFIG,
        Shared.UNDEFINED_STANDARD,
        Shared.EMPTY_STRING, Shared.EMPTY_VERSION_MAP, Shared.TARGET_REPORT,
        Shared.DEFAULT_THREADS,
        Shared.DEFAULT_GENERATE_RULE_METRICS,
        Shared.DEFAULT_USE_ENHANCED_SAS,
        Shared.DEFAULT_AUTO_DISPLAY_DOMAIN_KEYS),

    GENERATE_DEFINE("GENERATE_DEFINE", Shared.GENERATOR_CONFIG,
        Shared.UNDEFINED_STANDARD,
        Shared.EMPTY_STRING, Shared.EMPTY_VERSION_MAP, Shared.TARGET_REPORT,
        Shared.DEFAULT_THREADS,
        Shared.DEFAULT_GENERATE_RULE_METRICS,
        Shared.DEFAULT_USE_ENHANCED_SAS,
        Shared.DEFAULT_AUTO_DISPLAY_DOMAIN_KEYS),

    MINER("MINER", Shared.GENERATOR_CONFIG,
        Shared.UNDEFINED_STANDARD,
        Shared.EMPTY_STRING, Shared.EMPTY_VERSION_MAP,
        new Getter<CommunityReport>() {
            @Override
            public CommunityReport get(BaseCommunityModel model) {
                CommunityMiner miner = (CommunityMiner) model;
                return new CommunityReport(miner.getTarget(), Constants.DEFAULT_CUTOFF,
                    ReportType.fromValue(miner.getReportType()));
            }
        },
        Shared.DEFAULT_THREADS,
        Shared.DEFAULT_GENERATE_RULE_METRICS,
        Shared.DEFAULT_USE_ENHANCED_SAS,
        Shared.DEFAULT_AUTO_DISPLAY_DOMAIN_KEYS),

    VALIDATE_DATA("VALIDATE_DATA", new Getter<File>() {
        @Override
        public File get(BaseCommunityModel model) {
            return Shared.getConfigFile(((CommunityValidator) model).getConfig().getPath());
        }
    }, new Getter<StandardType>() {
        @Override
        public StandardType get(BaseCommunityModel model) {
            return ((CommunityValidator) model).getConfig().getStandard();
        }
    }, new Getter<String>() {
        @Override
        public String get(BaseCommunityModel model) {
            return ((CommunityValidator) model).getConfig().getDefine();
        }
    }, new Getter<Map<Valuable, String>>() {
        @Override
        public Map<Valuable, String> get(BaseCommunityModel model) {
            CommunityValidator validator = (CommunityValidator) model;
            return validator.getConfig().getTerminologies();
        }
    }, new Getter<CommunityReport>() {
        @Override
        public CommunityReport get(BaseCommunityModel model) {
            return ((CommunityValidator) model).getReport();
        }
    }, new Getter<Integer>() {
        @Override
        public Integer get(BaseCommunityModel model) {
            return ((CommunityValidator) model).getThreads();
        }
    }, new Getter<Boolean>() {
        @Override
        public Boolean get(BaseCommunityModel model) {
            return ((CommunityValidator) model).isGenerateRuleMetrics();
        }
    }, new Getter<Boolean>() {
        @Override
        public Boolean get(BaseCommunityModel model) {
            return ((CommunityValidator) model).isUseEnhancedSas();
        }
    }, new Getter<Boolean>() {
        @Override
        public Boolean get(BaseCommunityModel model) {
            return ((CommunityValidator) model).isAutoDisplayDomainKeys();
        }
    }),

    LIST_CONFIG("LIST_CONFIG",
        Shared.GENERATOR_CONFIG,
        Shared.UNDEFINED_STANDARD,
        Shared.EMPTY_STRING,
        Shared.EMPTY_VERSION_MAP,
        Shared.TARGET_REPORT,
        Shared.DEFAULT_THREADS,
        Shared.DEFAULT_GENERATE_RULE_METRICS,
        Shared.DEFAULT_USE_ENHANCED_SAS,
        Shared.DEFAULT_AUTO_DISPLAY_DOMAIN_KEYS),

    UNKNOWN("UNKNOWN", null,
        Shared.UNDEFINED_STANDARD,
        Shared.EMPTY_STRING,
        Shared.EMPTY_VERSION_MAP,
        Shared.TARGET_REPORT,
        Shared.DEFAULT_THREADS,
        Shared.DEFAULT_GENERATE_RULE_METRICS,
        Shared.DEFAULT_USE_ENHANCED_SAS,
        Shared.DEFAULT_AUTO_DISPLAY_DOMAIN_KEYS);

    private final String value;
    private final Getter<File> configGetter;
    private final Getter<StandardType> standardGetter;
    private final Getter<String> defineGetter;
    private final Getter<Map<Valuable, String>> versionMap;
    private final Getter<CommunityReport> reportGetter;
    private final Getter<Integer> numOfThreads;
    private final Getter<Boolean> generateRuleMetrics;
    private final Getter<Boolean> useEnhancedSas;
    private final Getter<Boolean> autoDisplayDomainKeys;

    Action(String value,
           Getter<File> configGetter,
           Getter<StandardType> standardGetter,
           Getter<String> defineGetter,
           Getter<Map<Valuable, String>> versionMap,
           Getter<CommunityReport> reportGetter,
           Getter<Integer> numOfThreads,
           Getter<Boolean> generateRuleMetrics,
           Getter<Boolean> useEnhancedSas,
           Getter<Boolean> autoDisplayDomainKeys) {
        this.value = value;
        this.configGetter = configGetter;
        this.standardGetter = standardGetter;
        this.defineGetter = defineGetter;
        this.versionMap = versionMap;
        this.reportGetter = reportGetter;
        this.numOfThreads = numOfThreads;
        this.generateRuleMetrics = generateRuleMetrics;
        this.useEnhancedSas = useEnhancedSas;
        this.autoDisplayDomainKeys = autoDisplayDomainKeys;
    }

    public static Action fromValue(String value) {
        return Valuables.fromValue(values(), value, UNKNOWN);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public boolean isValue(String value) {
        return this.name().equalsIgnoreCase(value) ||
            Strings.nullToEmpty(this.value).equalsIgnoreCase(value);
    }

    public File toConfig(BaseCommunityModel model) {
        return this.configGetter == null ? null : this.configGetter.get(model);
    }

    public StandardType toStandard(BaseCommunityModel model) {
        return this.standardGetter.get(model);
    }

    public String toDefine(BaseCommunityModel model) {
        return this.defineGetter.get(model);
    }

    public Map<Valuable, String> toVersions(BaseCommunityModel model) {
        return this.versionMap.get(model);
    }

    public CommunityReport toReport(BaseCommunityModel model) {
        return this.reportGetter.get(model);
    }

    public int toThreads(BaseCommunityModel model) {
        return this.numOfThreads.get(model);
    }

    public boolean toGenerateRuleMetrics(BaseCommunityModel model) {
        return this.generateRuleMetrics.get(model);
    }

    public boolean toUseEnhancedSas(BaseCommunityModel model) {
        return this.useEnhancedSas.get(model);
    }

    public boolean toAutoDisplayDomainKeys(BaseCommunityModel model) {
        return this.autoDisplayDomainKeys.get(model);
    }

    interface Getter<T> {
        T get(BaseCommunityModel model);
    }

    public static final class Shared {
        static final Action.Getter<File> GENERATOR_CONFIG = new Action.Getter<File>() {
            @Override
            public File get(BaseCommunityModel model) {
                return getConfigFile(((CommunityGenerator) model).getConfig());
            }
        };
        static final Action.Getter<StandardType> UNDEFINED_STANDARD = new Action.Getter<StandardType>() {
            @Override
            public StandardType get(BaseCommunityModel model) {
                return StandardType.UNDEFINED;
            }
        };
        static final Action.Getter<String> EMPTY_STRING = new Action.Getter<String>() {
            @Override
            public String get(BaseCommunityModel model) {
                return "";
            }
        };
        static final Action.Getter<Map<Valuable, String>> EMPTY_VERSION_MAP = new Action.Getter<Map<Valuable, String>>() {
            @Override
            public Map<Valuable, String> get(BaseCommunityModel model) {
                return ImmutableMap.of();
            }
        };
        static final Action.Getter<CommunityReport> TARGET_REPORT = new Action.Getter<CommunityReport>() {
            @Override
            public CommunityReport get(BaseCommunityModel model) {
                return new CommunityReport(model.getTarget(), Constants.DEFAULT_CUTOFF, ReportType.EXCEL);
            }
        };
        static final Action.Getter<Integer> DEFAULT_THREADS = new Action.Getter<Integer>() {
            @Override
            public Integer get(BaseCommunityModel model) {
                return 1;
            }
        };
        static final Action.Getter<Boolean> DEFAULT_GENERATE_RULE_METRICS = new Action.Getter<Boolean>() {
            @Override
            public Boolean get(BaseCommunityModel model) {
                return Boolean.TRUE;
            }
        };
        static final Action.Getter<Boolean> DEFAULT_USE_ENHANCED_SAS = new Action.Getter<Boolean>() {
            @Override
            public Boolean get(BaseCommunityModel model) {
                return Boolean.TRUE;
            }
        };
        static final Action.Getter<Boolean> DEFAULT_AUTO_DISPLAY_DOMAIN_KEYS = new Action.Getter<Boolean>() {
            @Override
            public Boolean get(BaseCommunityModel model) {
                return Boolean.TRUE;
            }
        };

        private Shared() {
        }

        private static File getConfigFile(String config) {
            if (Strings.isNullOrEmpty(config)) {
                return toFile(lookupDir("config", false));
            }

            return new File(config);
        }
    }
}
