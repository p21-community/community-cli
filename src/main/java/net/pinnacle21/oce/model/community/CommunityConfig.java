package net.pinnacle21.oce.model.community;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import net.pinnacle21.oce.model.StandardType;
import net.pinnacle21.oce.model.Valuable;
import net.pinnacle21.oce.model.support.StandardTypeDeserializer;
import net.pinnacle21.oce.model.support.TerminologiesDeserializer;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CommunityConfig {
    @JsonDeserialize(using = StandardTypeDeserializer.class)
    private StandardType standard;
    private String path;
    private String define;
    private StandardType defineStandard;

    @JsonDeserialize(using = TerminologiesDeserializer.class)
    private Map<Valuable, String> terminologies;

    public StandardType getStandard() {
        return standard;
    }

    public void setStandard(StandardType standard) {
        this.standard = standard;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDefine() {
        return define;
    }

    public void setDefine(String define) {
        this.define = define;
    }

    public Map<Valuable, String> getTerminologies() {
        return terminologies;
    }

    public void setTerminologies(Map<Valuable, String> terminologies) {
        this.terminologies = terminologies;
    }
}
