package net.pinnacle21.oce;

import net.bootstrap.api.monitor.context.DynamicContext;
import net.pinnacle21.oce.utils.IPC;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static net.pinnacle21.oce.utils.ProcessUtils.startFromCommandLine;
import static net.pinnacle21.oce.utils.ProcessUtils.startFromJson;

public final class Main {
    public static void main(String[] args) throws IOException {
        if (System.getProperty("json") == null) {
            DynamicContext.setStartTime();
            startFromCommandLine(args);
        } else {
            IPC.initialize(); // This makes sure we capture System.out for IPC and disable other library chatter.
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8))) {
                for (String json = reader.readLine(); json != null; json = reader.readLine()) {
                    try {
                        DynamicContext.setStartTime();
                        startFromJson(json);
                    } catch (Throwable error) {
                        System.err.println(error.getMessage());
                        error.printStackTrace();
                    }
                }
            }
        }
    }
}
