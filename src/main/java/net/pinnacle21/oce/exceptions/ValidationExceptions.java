package net.pinnacle21.oce.exceptions;

public enum ValidationExceptions implements CLIExceptionCode {
    INVALID_DATA_SOURCE(41, "Data source is invalid", "CLI.4.1"),
    MISSING_ARG_PARSE_RESULTS(42, "Argument parsing results must be provided", "CLI.4.2"),
    REQURED_STANDARD(43, "Standard type is required", "CLI.4.3"),
    REQURED_DATASOURCE(44, "Data source is required", "CLI.4.4"),
    REQURED_STANDARD_VERSION(45, "Standard version is required", "CLI.4.5"),
    REQURED_REPORT_FILE(46, "Report file is required", "CLI.4.6"),
    REQUIRED_CONFIG(47, "Config is required", "CLI.4.7"),
    REQUIRED_OUTPUT(48, "Output is required", "CLI.4.8"),
    REQUIRED_OUTPUT_FORMAT(49, "Output format type is required", "CLI.4.9"),
    MISSING_DATA_SOURCE(410, "Data source is missing", "CLI.4.10"),
    INVALID_COMMAND_LINE_OPTION(411, "Command line option is invalid or unknown", "CLI.4.11"),
    REQUIRED_WEB_HOST(412, "Web host is required for Enterprise mode", "CLI.4.12"),
    REQUIRED_DELIMITER(413, "Source delimiter is required for DELIMITED data", "CLI.4.13"),
    REQUIRED_QUALIFIER(414, "Source text qualifier is required for DELIMITED data", "CLI.4.14"),
    REQUIRED_GENERAL(415, "Missing required input", "CLI.4.15"),
    REQUIRED_TERMINOLOGY_VERSION(416, "Missing CDISC CT Version", "CLI.4.16"),
    INVALID_REPORT_FILE(417, "Report file is invalid", "CLI.4.17"),
    INVALID_ENGINE_VERSION(418, "Invalid engine version parameter", "CLI.4.18"),
    INVALID_DELIMITED_FILE_TYPE(419, "Invalid delimited file type", "CLI.4.19");


    private final byte code;
    private final String message;
    private final String ref;

    ValidationExceptions(int code, String message, String ref) {
        this.code = (byte) code;
        this.message = message;
        this.ref = ref;
    }

    @Override
    public byte getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String getRef() {
        return this.ref;
    }
}
