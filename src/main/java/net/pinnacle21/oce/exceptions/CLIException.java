package net.pinnacle21.oce.exceptions;

import net.pinnacle21.oce.utils.CliErrorUtils;

import static net.pinnacle21.oce.exceptions.CLIExceptionCode.GENERAL_EXCEPTION;

public class CLIException extends RuntimeException {
    private final CLIExceptionCode code;

    public CLIException() {
        this(GENERAL_EXCEPTION);
    }

    public CLIException(CLIExceptionCode code) {
        this(code, code.getMessage());
    }

    public CLIException(CLIExceptionCode code, String msg) {
        super(msg);
        this.code = code;
    }

    public CLIException(CLIExceptionCode code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public CLIException(CLIExceptionCode code, String msg, Throwable cause) {
        super(msg, cause);
        this.code = code;
    }

    public CLIException(String msg) {
        this(GENERAL_EXCEPTION, msg);
    }

    public CLIException(String msg, Throwable cause) {
        this(CliErrorUtils.getErrorCode(cause), msg, cause);
    }

    public CLIExceptionCode getCode() {
        return code;
    }
}
