package net.pinnacle21.oce.exceptions;

public interface CLIExceptionCode {
    CLIExceptionCode GENERAL_EXCEPTION = new CLIExceptionCode() {
        @Override
        public byte getCode() {
            return 1;
        }

        @Override
        public String getMessage() {
            return "General CLI exception";
        }

        @Override
        public String getRef() {
            return "CLI.1.1";
        }
    };

    byte getCode();

    String getMessage();

    String getRef();
}
