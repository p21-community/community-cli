package net.pinnacle21.oce.exceptions;

public enum SystemExceptions implements CLIExceptionCode {
    FAILED_REQUEST_TYPE(31, "Request type cannot be determined", "CLI.3.1"),
    FAILED_COPY_SOURCE(32, "Failed to copy data source", "CLI.3.2"),
    EXISTS_DEFINE_FILE(33, "Define file does not exists", "CLI.3.3"),
    FAILED_COPY_DEFINE_XML(34, "Failed to copy define XML", "CLI.3.4"),
    FAILED_ARG_PARSE(35, "Failed parsing input argument list", "CLI.3.5"),
    EXISTS_FILE(36, "File or folder does not exists", "CLI.3.6"),
    FAILED_CLIENT_CALL(37, "Client call failed", "CLI.3.7"),
    FAILED_OUTPUT_DIR(38, "Failed creating output directory", "CLI.3.8"),
    EXISTS_CT_TERMINOLOGY(39, "Control terminology does not exists", "CLI.3.9"),
    FAILED_OUTPUT_FILE(310, "Failed creating output file", "CLI.3.10"),
    FAILED_CLIENT_TASK(311, "Failed processing client task", "CLI.3.11"),
    FAILED_CORE(312, "Failed core process.", "CLI.3.12"),
    OPENED_FILE(313, "File is opened and cannot be written to. Please close the file and restart process", "CLI.3.13"),
    NOTFOUND_INTEGRATION_LIB(314, "Config for integration library is not found", "CLI.3.14"),
    NOTFOUND_GROUP(315, "Enterprise user group is not found", "CLI.3.15"),
    FAILED_SYSTEM_SETUP_CHECK(316, "System setup and configuration error. Please update your system", "CLI.3.16"),
    FAILED_SYSTEM_IQ_CHECK(317, "System installation qualification error", "CLI.3.17"),
    FAILED_JSON(318, "Failed json serialization", "CLI.3.18"),
    PERMISSION_DENIED(319, "Permission denied", "CLI.3.19"),
    DOWNLOAD_FAILED(320, "Download failed", "CLI.3.20");


    private final byte code;
    private final String message;
    private final String ref;

    SystemExceptions(int code, String message, String ref) {
        this.code = (byte) code;
        this.message = message;
        this.ref = ref;
    }

    @Override
    public byte getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String getRef() {
        return this.ref;
    }
}
