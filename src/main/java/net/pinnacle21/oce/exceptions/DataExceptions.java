package net.pinnacle21.oce.exceptions;

public enum DataExceptions implements CLIExceptionCode {
    UNKNOWN_STANDARD(22, "Standard is unknown", "CLI.2.2"),
    UNKNOWN_FORMAT(23, "Format type is unknown", "CLI.2.3"),
    NOTFOUND_TERMINOLOGY(24, "Terminology value is invalid or not defined", "CLI.2.4"),
    NOTFOUND_DICTIONARY(25, "Dictionary value is invalid or not defined", "CLI.2.5"),
    UNKNOWN_ENTERPRISE_TYPE(26, "Enterprise data type is unknown", "CLI.2.6"),
    UNDEFINED_RELEASE_VERSION(27, "Release version is undefined", "CLI.2.7"),
    PARSE_DATA_SOURCE(28, "Parsing failed. Cannot read data source", "CLI.2.8"),
    UNKNOWN_VALUE(29, "Provided Value is unknown", "CLI.2.9"),
    UNKNOWN_REQUEST_TYPE(210, "Request type value is unknown", "CLI.2.10");


    private final byte code;
    private final String message;
    private final String ref;

    DataExceptions(int code, String message, String ref) {
        this.code = (byte) code;
        this.message = message;
        this.ref = ref;
    }

    @Override
    public byte getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String getRef() {
        return this.ref;
    }
}
