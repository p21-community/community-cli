package net.pinnacle21.oce.services;

import dagger.Component;
import net.pinnacle21.oce.clients.BootstrapCli;
import net.pinnacle21.oce.modules.AppModule;
import net.pinnacle21.oce.modules.ServicesModule;

import javax.inject.Singleton;

/**
 * Main component that provides all services.
 */

@Component(modules = {ServicesModule.class, AppModule.class})
@Singleton
public interface Clients {
    BootstrapCli bootstrapClient();
}
