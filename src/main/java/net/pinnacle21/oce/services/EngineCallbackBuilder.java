package net.pinnacle21.oce.services;

import net.bootstrap.core.callback.EngineCallback;
import net.pinnacle21.oce.model.CliModel;

public class EngineCallbackBuilder extends EngineCallback.Builder<EngineCallback, EngineCallbackBuilder> {
    private CliModel cliModel;

    public static EngineCallbackBuilder builder() {
        return new EngineCallbackBuilder();
    }

    public EngineCallbackBuilder setCliModel(CliModel cliModel) {
        this.cliModel = cliModel;
        if (this.cliModel != null) {
            return getThis()
                .setEngineFolder(this.cliModel.getEngineFolder())
                .setEngineVersion(this.cliModel.getEngineVersion());
        }
        return getThis();
    }

    @Override
    protected EngineCallbackBuilder getThis() {
        return this;
    }

    @Override
    public EngineCallback build() {
        return null;
    }
}
