package net.pinnacle21.oce.services;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import net.pinnacle21.oce.model.community.Action;
import net.pinnacle21.oce.model.input.CliParameters;
import net.pinnacle21.oce.model.input.CommunityInput;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.Arrays;

public class RemoteAppender extends AppenderBase<ILoggingEvent> {
    private Config conf = ConfigFactory.load();

    // FIXME: I should be able to get the parameters and action somewhere nicely
    private static CliParameters input;

    public static void setInput(CliParameters parameters) {
        input = parameters;
    }

    @Override
    protected void append(ILoggingEvent eventObject) {
        // Only disable logging if the variable is explicitly set to false
        String enabled = System.getProperty("pinnacle.software.logging.remote");
        if (StringUtils.isNotBlank(enabled) && !Boolean.valueOf(enabled)) {
            return;
        }
        CliParameters parameters = input;
        String errorId = getErrorId(eventObject.getCallerData());
        JsonNode message = getMessage(eventObject, parameters);
        String action = getAction();
        String tool = getTool(parameters);
        String pinnacleId = getPinnacleId(parameters);
        String appId = getAppId(parameters);
        String version = getVersion();
        String service = getService();

        LogData payload = new LogData(errorId, message, action, tool, pinnacleId, appId, version, service);

        post(payload);
    }

    private String getErrorId(StackTraceElement[] error) {
        if (ArrayUtils.isEmpty(error)) {
            return null;
        }
        return DigestUtils.md5Hex(Arrays.toString(error));
    }

    private JsonNode getMessage(ILoggingEvent event, CliParameters parameters) {
        ObjectNode message = new ObjectMapper().createObjectNode();
        message.put("message", event.getMessage());
        if (event.getThrowableProxy() != null) {
            message.put("exception-message", event.getThrowableProxy().getMessage());
            if (event.getThrowableProxy().getCause() != null) {
                message.put("caused-by-message", event.getThrowableProxy().getCause().getMessage());
                message.put(
                        "caused-by-stacktrace",
                        Arrays.toString(event.getThrowableProxy().getCause().getStackTraceElementProxyArray())
                );
            }
        }
        message.put("parameters", parameters.serialize());
        message.put("stacktrace", Arrays.toString(event.getCallerData()));
        message.put("level", event.getLevel().toString().toLowerCase());
        return message;
    }

    private String getAction() {
        // Action only exists when being called from the GUI as far as I can tell
        if (input instanceof CommunityInput) {
            Action action = ((CommunityInput) input).getAction();
            if (action != null) {
                return StringUtils.stripToNull(action.getValue());
            }
        }
        return null;
    }

    private String getTool(CliParameters args) {
        // Tool is a GUI only thing, doesn't make sense for CLI
        return null;
    }

    private String getPinnacleId(CliParameters args) {
        return StringUtils.stripToNull(args.getUserId().get());
    }

    private String getAppId(CliParameters args) {
        return StringUtils.stripToNull(args.getAnonymousId().get());
    }

    private String getVersion() {
        return StringUtils.stripToNull(this.getClass().getPackage().getImplementationVersion());
    }

    private String getService() {
        if (StringUtils.containsIgnoreCase(getVersion(), "snapshot")) {
            return "community-cli-snapshot";
        }
        return "community-cli";
    }

    private void post(LogData payload) {
        try {
            String json = new ObjectMapper().writeValueAsString(payload);
            StringEntity requestEntity = new StringEntity(json, ContentType.APPLICATION_JSON);
            HttpPost post = new HttpPost(getLoggingUrl(payload.getVersion()));
            post.setEntity(requestEntity);

            RequestConfig.Builder builder = RequestConfig.custom();
            int timeout = conf.getInt("ws.timeout");
            builder.setConnectTimeout(timeout);
            builder.setConnectionRequestTimeout(timeout);
            builder.setSocketTimeout(timeout);

            HttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(builder.build()).build();

            client.execute(post);
        } catch (IOException ignore) {
            // This gets thrown in the case of no internet connection, etc
            // I guess we just wont append this error ¯\_(ツ)_/¯
        }
    }

    private String getLoggingUrl(String version) {
        String url;
        if (StringUtils.containsAny(StringUtils.lowerCase(version), "pre", "snap")) {
            url = this.conf.getString("ws.test.url");
        } else {
            url = this.conf.getString("ws.prod.url");
        }
        return url + this.conf.getString("ws.endpoints.log");
    }

    @SuppressWarnings("unused") // This class is serialized
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final static class LogData {
        private String errorId;
        private JsonNode message;
        private String action;
        private String tool;
        private String pinnacleId;
        private String appId;
        private String version;
        private String service;

        LogData(String errorId,
                JsonNode message,
                String action,
                String tool,
                String pinnacleId,
                String appId,
                String version,
                String service) {
            this.errorId = errorId;
            this.message = message;
            this.action = action;
            this.tool = tool;
            this.pinnacleId = pinnacleId;
            this.appId = appId;
            this.version = version;
            this.service = service;
        }

        public String getErrorId() {
            return this.errorId;
        }

        public JsonNode getMessage() {
            return this.message;
        }

        public String getAction() {
            return this.action;
        }

        public String getTool() {
            return this.tool;
        }

        public String getPinnacleId() {
            return this.pinnacleId;
        }

        public String getAppId() {
            return this.appId;
        }

        public String getVersion() {
            return this.version;
        }

        public String getService() {
            return this.service;
        }
    }
}
