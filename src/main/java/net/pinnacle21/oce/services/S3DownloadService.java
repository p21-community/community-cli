package net.pinnacle21.oce.services;

import com.google.common.collect.ImmutableList;
import net.bootstrap.core.utils.jar.Artifact;
import net.bootstrap.utils.S3Loader;
import rx.Observable;

import java.io.File;
import java.util.List;

import static net.bootstrap.utils.S3Loader.getS3Bucket;

public class S3DownloadService {
    public Observable<Boolean> downloadEngine(final File destination, final String name) {
        class LibArtifact extends Artifact {
            private LibArtifact() {
                super(
                    getS3Bucket(),
                    name,
                    "jar",
                    S3DownloadService.class,
                    destination,
                    s -> true
                );
            }

            @Override
            public boolean isTemp() {
                return false;
            }

            @Override
            public SourceType getSourceType() {
                return SourceType.S3;
            }

            @Override
            public List<String> forClasses() {
                return ImmutableList.<String>of();
            }

            @Override
            public boolean isUseSystemLoader() {
                return false;
            }
        }

        LibArtifact artifact = new LibArtifact();
        return S3Loader.loadArtifactFromS3Async(artifact, artifact.getPredicate())
            .toList()
            .map(urls -> !S3Loader.isEmpty(urls));
    }
}
