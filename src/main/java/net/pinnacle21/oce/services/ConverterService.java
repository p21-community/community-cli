package net.pinnacle21.oce.services;

import net.bootstrap.api.converter.ConverterResult;
import net.bootstrap.api.converter.ConverterUtils;
import net.bootstrap.api.model.*;
import net.bootstrap.core.callback.EngineCallback;
import net.bootstrap.core.model.FileType;
import net.bootstrap.core.utils.BootstrapFileUtils;
import net.pinnacle21.oce.events.Listener;
import net.pinnacle21.oce.events.ProcessEvent;
import net.pinnacle21.oce.events.StateEvent;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.ConverterInfo;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.ProcessState;
import net.pinnacle21.oce.utils.DataSources;
import rx.Observable;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static net.pinnacle21.oce.utils.CliPreconditions.checkCollection;
import static net.pinnacle21.oce.utils.CliPreconditions.checkProvided;
import static net.pinnacle21.oce.utils.ProcessUtils.toLabel;

public class ConverterService {
    private final DefineService defineService;

    public ConverterService(DefineService defineService) {
        this.defineService = defineService;
    }

    public Observable<ConverterResult> convert(final ConverterInfo converterInfo,
                                               final Listener<StateEvent> listener) {
        return ConverterUtils.convert(toSource(converterInfo, listener),
            toDest(converterInfo), toOutputType(converterInfo),
            new EngineCallback(EngineCallbackBuilder
                .builder()
                .setCliModel(converterInfo)) {
                @Override
                public void tell(String origin, String destination, long pos) {
                    ProcessEvent.Builder<ConverterInfo> builder =
                        ProcessEvent.Builder.<ConverterInfo>builder()
                            .setLabel(toLabel(origin, destination))
                            .setState(ProcessState.TASK_PROCESSING)
                            .setCurrent(pos)
                            .setTotal(getLength())
                            .setTarget(converterInfo);

                    listener
                        .listen(builder.build());
                }
            });
    }

    private FileType toOutputType(ConverterInfo converterInfo) {
        return checkProvided(converterInfo.getOutputFormatType(),
            ValidationExceptions.REQUIRED_OUTPUT_FORMAT).getBootstrapType();
    }

    private File toDest(ConverterInfo converterInfo) {
        final File destination = DataSources.toFile(converterInfo.getOutput());
        try {
            BootstrapFileUtils.forceMkdir(checkProvided(destination, ValidationExceptions.REQUIRED_OUTPUT,
                "Output directory is required but not provided"));
        } catch (IOException e) {
            throw new CLIException(SystemExceptions.FAILED_OUTPUT_DIR,
                "Failed to create converter output directory", e);
        }

        return destination;
    }

    private Source toSource(ConverterInfo converterInfo, Listener<StateEvent> listener) {
        List<File> files = DataSources.toFiles(converterInfo.getSources());

        checkCollection(files, ValidationExceptions.MISSING_DATA_SOURCE,
            "Cannot create converter source. Sources are invalid");

        if (files.size() == 1) {
            return new FileSource(getDefineDocument(converterInfo, listener),
                files.get(0));
        } else {
            return new MultiFileSource(getDefineDocument(converterInfo, listener),
                files);
        }
    }

    private DefineDocument getDefineDocument(ConverterInfo converterInfo,
                                             Listener<StateEvent> listener) {
        if (converterInfo.getOutputFormatType() == FormatType.DATASET_XML) {
            return this.defineService.getOrCreateDefineDocument(converterInfo, listener)
                .toBlocking().single();
        } else {
            return null;
        }

    }
}
