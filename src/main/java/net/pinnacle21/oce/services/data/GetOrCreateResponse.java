package net.pinnacle21.oce.services.data;

/**
 * Created by aleksey on 6/9/17.
 */
public class GetOrCreateResponse<T> {
    private final T response;
    private final boolean created;
    private final Long parentId;

    public GetOrCreateResponse(T response, boolean created, Long parentId) {
        this.response = response;
        this.created = created;
        this.parentId = parentId;
    }

    public T getResponse() {
        return response;
    }

    public boolean isCreated() {
        return created;
    }

    public Long getParentId() {
        return parentId;
    }
}
