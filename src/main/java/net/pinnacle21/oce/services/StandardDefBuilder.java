package net.pinnacle21.oce.services;

import com.google.common.base.Optional;
import net.bootstrap.api.model.StandardDef;
import net.pinnacle21.oce.model.datapackage.MetadataMapper;
import net.pinnacle21.oce.model.input.ConfigInfo;
import net.pinnacle21.oce.utils.CliPreconditions;
import net.pinnacle21.oce.utils.ResourceUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

import static net.pinnacle21.oce.utils.DataSources.toFile;

class StandardDefBuilder {
    private MetadataMapper mapper;
    private String filter = StandardDef.ALL_FILTERS;
    private ResourceUtils.MetadataResource resource;
    private String standard;
    private File path;
    private ConfigInfo info;

    private StandardDefBuilder() {
    }

    static StandardDefBuilder builder() {
        return new StandardDefBuilder();
    }

    StandardDefBuilder setResource(ResourceUtils.MetadataResource resource) {
        this.resource = resource;
        return this;
    }

    StandardDefBuilder setMapper(MetadataMapper mapper) {
        this.mapper = mapper;
        return this;
    }

    StandardDefBuilder setFilter(String filter) {
        this.filter = filter;
        return this;
    }

    StandardDefBuilder setStandard(String standard) {
        this.standard = standard;
        return this;
    }

    StandardDefBuilder setPath(File path) {
        this.path = path;
        return this;
    }

    StandardDefBuilder setInfo(ConfigInfo info) {
        this.info = info;
        return this;
    }

    StandardDef build() {
        if (StringUtils.isNotBlank(this.standard) &&
            this.path != null &&
            this.resource != null) {
            return StandardDef.builder()
                .withStandard(this.standard)
                .withVersion(resource.version.get())
                .withPath(this.path)
                .build();
        }

        if (StringUtils.isNotBlank(this.standard) &&
            this.resource != null) {
            return StandardDef.builder()
                .withStandard(this.standard)
                .withVersion(resource.version.get())
                .withPath(toFile(resource.dataSource))
                .build();
        }

        if (this.info != null &&
            this.path != null) {
            MetadataMapper resolvedMapper = Optional.fromNullable(this.mapper).or(info.getStandardType());
            String resolvedVersion = StringUtils.defaultIfBlank(info.getVersion(),
                FilenameUtils.getBaseName(this.path.getName()));

            return StandardDef.builder()
                .withStandard(StringUtils.defaultString(this.standard, resolvedMapper.getValue()))
                .withVersion(resolvedVersion)
                .withPath(this.path)
                .withFilter(info.getFilter().getValue())
                .withConvertFrom(resolvedMapper.convertFrom())
                .build();
        }

        CliPreconditions.checkProvided(this.mapper, "Cannot create Stadard definition. Mapper must be provided");
        CliPreconditions.checkProvided(this.resource,
            String.format("Cannot create Stadard definition for %s. Metadata resource",
                this.mapper.getValue()));

        return StandardDef.builder()
            .withStandard(mapper.getValue())
            .withVersion(resource.version.get())
            .withPath(toFile(resource.dataSource))
            .withFilter(filter)
            .withConvertFrom(mapper.convertFrom())
            .build();
    }
}
