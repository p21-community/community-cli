package net.pinnacle21.oce.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.datasource.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;

public class JsonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonService.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final String STD_START_MARKER = "=CLI:" + System.lineSeparator();
    private static final String STD_END_MARKER = System.lineSeparator();

    private static <T> void writeObjectToOutput(T object, DataSource output) throws IOException {
        OutputStream stream = output.getOutputStream();
        if (output.getSchema() == Schema.STD) {
            // standard output. create a marker
            stream.write(STD_START_MARKER.getBytes());
        }

        OBJECT_MAPPER.writeValue(stream, object);
        if (output.getSchema() == Schema.STD) {
            // standard output. create a marker
            stream.write(STD_END_MARKER.getBytes());
        }

        stream.close();
    }

    public <T> void writeObject(T object, DataSource output) {
        try {
            writeObjectToOutput(object, output);
        } catch (IOException e) {
            LOGGER.error("Failed to write response {} to the output {}", object, output);
            throw new CLIException(SystemExceptions.FAILED_OUTPUT_FILE,
                "Failed to write response to output.", e);
        }
    }
}
