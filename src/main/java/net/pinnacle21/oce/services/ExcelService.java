package net.pinnacle21.oce.services;

import com.google.common.collect.Lists;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.utils.ProcessUtils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Arrays;
import java.util.List;

import static net.pinnacle21.oce.utils.DataSources.toFile;

public class ExcelService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelService.class);
    private static final List<String> SEVERITIES = Arrays.asList("Reject", "Error", "Warning", "Notice");
    private static final int SEVERITY_CODE = SEVERITIES.size();

    private static void closeFinally(Closeable closable) {
        if (closable != null) {
            try {
                closable.close();
            } catch (IOException e) {
                LOGGER.error("Failed to close", e);
            }
        }
    }

    /**
     * get validation results from the data input stream
     *
     * @param stream server response
     */
    int getValidationResults(DataPackage dataPackage, InputStream stream) {
        if (ProcessUtils.isGui()) {
            // running from community UI. No need to read report
            return 0;
        }

        try (FileOutputStream output =
                 FileUtils.openOutputStream(toFile(dataPackage.getReportSource()))) {
            // write the file
            IOUtils.copy(stream, output);
            output.close();
            return getValidationResults(dataPackage);
        } catch (Exception e) {
            throw new CLIException("Failed to get validation result",
                e);
        }
    }

    int getValidationResults(DataPackage dataPackage) {
        if (ProcessUtils.isGui()) {
            // running from community UI. No need to read report
            return 0;
        }

        Workbook workbook = null;
        File exlFile = toFile(dataPackage.getReportSource());

        try {
            LOGGER.info("Open report '{}' for read and analysis",
                exlFile.getAbsolutePath());
            workbook = WorkbookFactory.create(exlFile);
            return readWorkbook(workbook);
        } catch (Exception e) {
            throw new CLIException(String.format("Failed to get validation result. Reading file='%s'",
                exlFile.getAbsolutePath()), e);
        } finally {
            closeFinally(workbook);
        }
    }

    private int readWorkbook(Workbook workbook) {
        int min = SEVERITY_CODE; // if none of the SEVERITIES is found return code will be SEVERITIES.size() + 1
        List<Integer> cols = Lists.newArrayList();

        for (Row row : workbook.getSheet("Issue Summary")) {
            if (cols.isEmpty()) {
                cols = findcolomns(row);
            } else {
                min = readRow(row, cols, min);
                if (min == 0) {
                    break;
                }
            }
        }

        return min + 1;
    }

    private int readRow(Row row, List<Integer> cols, int min) {
        for (Integer col : cols) {
            String severity = row.getCell(col, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
            int index = SEVERITIES.indexOf(severity);

            if (index != -1 && index < min) {
                min = index;

                if (min == 0) {
                    break;
                }
            }
        }

        return min;
    }

    private List<Integer> findcolomns(Row header) {
        List<Integer> cols = Lists.newArrayList();
        for (int cell = 0; cell < header.getLastCellNum(); ++cell) {
            String headerText = header.getCell(cell, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
            if ("fda".equalsIgnoreCase(headerText) ||
                "pmda".equalsIgnoreCase(headerText) ||
                "severity".equalsIgnoreCase(headerText)) {
                cols.add(cell);
            }
        }

        return cols;
    }
}
