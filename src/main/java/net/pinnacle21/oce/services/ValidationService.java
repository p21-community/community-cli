package net.pinnacle21.oce.services;

import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import net.bootstrap.api.model.*;
import net.bootstrap.api.model.data.ResourcesData;
import net.bootstrap.api.validator.ValidatorMetrics;
import net.bootstrap.api.validator.ValidatorOptions;
import net.bootstrap.api.validator.ValidatorSource;
import net.bootstrap.api.validator.ValidatorUtils;
import net.bootstrap.core.callback.EngineCallback;
import net.bootstrap.core.model.CoreNames;
import net.bootstrap.core.model.FileType;
import net.bootstrap.core.utils.jar.JarUtils;
import net.bootstrap.utils.S3Loader;
import net.pinnacle21.oce.events.Listener;
import net.pinnacle21.oce.events.ProcessEvent;
import net.pinnacle21.oce.events.StateEvent;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.*;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datapackage.LocalDataPackage;
import net.pinnacle21.oce.model.datapackage.MetadataMapper;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.datasource.FolderDS;
import net.pinnacle21.oce.model.input.ConfigInfo;
import net.pinnacle21.oce.model.input.Input;
import net.pinnacle21.oce.model.response.ValidatorResponse;
import net.pinnacle21.oce.utils.ResourceUtils;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;

import javax.inject.Inject;
import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Math.max;
import static net.pinnacle21.oce.utils.DataSources.*;
import static net.pinnacle21.oce.utils.ProcessUtils.toLabel;

/**
 * Validator service.
 */
public class ValidationService {
    private static final Logger LOG = LoggerFactory.getLogger(ValidationService.class);
    private static final String ENGINE_MODULES = "engine/modules/";
    private static final String SLASH = "/";
    private static final String MODULES = "modules";
    private static final String PINNACLE_21 = "pinnacle21";
    private static final String VALIDATION = "validation";

    private final ExcelService excelService;

    @Inject
    public ValidationService(ExcelService excelService) {
        this.excelService = excelService;
    }

    private static StandardDef toStandardDef(LocalDataPackage dataPackage) {
        final DataPackage dp = dataPackage.getDataPackage();
        ConfigInfo info = dataPackage.getConfigInfo();

        if (dp.getSourceFormatType().get() == FormatType.DEFINE_XML) {
            return StandardDefBuilder.builder()
                .setMapper(StandardType.DEFINE)
                .setPath(toFile(info.getConfig()))
                .setInfo(info)
                .build();
        }

        return StandardDefBuilder.builder()
            .setStandard(dp.getStandard().get().getValue())
            .setInfo(info)
            .setPath(toFile(info.getConfig()))
            .build();
    }

    private static ResourceUtils.MetadataResource getMetadataPath(final LocalDataPackage dataPackage,
                                                                  MetadataMapper metadataMapper,
                                                                  boolean quiet) {
        DataSource configFolderDataSource =
            toFolder(dataPackage.getConfigInfo().getConfig());

        File configFolder = new File(configFolderDataSource.getAbsolutePath());
        if (configFolder.getName().equals("custom")) {
            configFolderDataSource = new FolderDS(
                configFolder.toPath().resolve("..").normalize().toFile(),
                configFolderDataSource.isCleanup()
            );
        }

        quiet &= !metadataMapper.getVersion(dataPackage.getDataPackage()).isProvided();

        try {
            return ResourceUtils.getMetadataFolder(
                configFolderDataSource,
                metadataMapper,
                dataPackage.getDataPackage());
        } catch (Exception ex) {
            if (quiet) {
                LOG.warn("Folder for metadata {} is not found under root {} for dataPackage {}",
                    metadataMapper.getValue(), configFolderDataSource.getAbsolutePath(), dataPackage);
                return new ResourceUtils.MetadataResource();
            }
            throw ex;
        }
    }

    private static FileSource findDefine(DataPackage dataPackage) {
        if (isValid(dataPackage.getDefine())) {
            return new FileSource(toFile(dataPackage.getDefine()), FileType.DEFINE);
        }

        if (dataPackage.getStandard().get() == StandardType.DEFINE) {
            return new FileSource(toFile(dataPackage.getSource()), FileType.DEFINE);
        }

        Collection<File> defineFile = toFiles(
            dataPackage.getSource(), FormatType.DEFINE_XML::isApplicable
        );

        if (defineFile == null || defineFile.isEmpty()) {
            return null;
        }

        return new FileSource(defineFile.iterator().next(), FileType.DEFINE);
    }

    private static ValidatorSource toValidatorSource(DataPackage dataPackage) {
        return new ValidatorSource(toCdiscSource(dataPackage),
            findDefine(dataPackage), dataPackage.getSourceFormatType().get() == FormatType.DEFINE_XML);
    }

    private static AbstractSource toCdiscSource(DataPackage dataPackage) {
        Input<FormatType> sourceFormat = dataPackage.getSourceFormatType();
        Input<String> delimiter = dataPackage.getSourceDelimiter();
        Input<String> qualifier = dataPackage.getSourceQualifier();
        Collection<File> files = toFiles(dataPackage.getSource());

        if (files == null || files.isEmpty()) {
            throw new CLIException(ValidationExceptions.INVALID_DATA_SOURCE,
                String.format("Data source is invalid. No files found in %s", dataPackage.getSource()));
        }

        if (files.size() > 1) {
            return new MultiFileSource("validator",
                files, sourceFormat.get().getBootstrapType(), delimiter.get(), qualifier.get());
        } else {
            return new FileSource(files.iterator().next(), sourceFormat.get().getBootstrapType(),
                delimiter.get(), qualifier.get());
        }
    }

    /**
     * Submit data package for local validation
     */
    public Observable<ValidatorResponse> submit(final LocalDataPackage localPackage,
                                                final Listener<StateEvent> listener) {
        if (localPackage == null || localPackage.getDataPackage() == null ||
            localPackage.getDataPackage().getSource() == null) {
            LOG.info("data package is not provided return Integer.MAX_VALUE");
            return Observable.just(new ValidatorResponse(Integer.MAX_VALUE, ""));
        }

        final EngineCallback callback = new EngineCallback(
            EngineCallbackBuilder.builder().setCliModel(localPackage)
        ) {
            private long maxCurrent = Long.MIN_VALUE;
            private long saveCurrent = Long.MIN_VALUE;

            @Override
            public void start() {
                this.saveCurrent = this.maxCurrent;
                super.start();
            }

            @Override
            public void stop() {
                this.maxCurrent = this.saveCurrent;
                super.stop();
            }

            @Override
            public synchronized void tell(String origin, String destination, long pos) {
                maxCurrent = max(maxCurrent, pos);

                // HACK: There's not a cleaner way of indicating a sub-task vs a task.
                // For now, let's prefix all sub-tasks with 'Sub-task: '. This allows
                // the message to appear below the primary status message on the progress
                // bar. There's also some hacky logic in the UI to search for parentheses
                // for showing how many records have been processed within the datasets.
                ProcessState state = ProcessState.TASK_PROCESSING;
                if (StringUtils.startsWith(origin, EngineCallback.SUB_TASK_PREFIX)) {
                    state = ProcessState.TASK_SUBPROCESSING;
                    origin = StringUtils.removeStart(origin, EngineCallback.SUB_TASK_PREFIX);
                }
                listener.listen(
                    ProcessEvent.Builder.<LocalDataPackage>builder()
                        .setTarget(localPackage)
                        .setCurrent(maxCurrent)
                        .setTotal(getLength())
                        .setState(state)
                        .setLabel(toLabel(origin, destination))
                        .build()
                );
            }
        };

        // TODO: Move to bootstrap-harness project
        File engineFolder = new File(
            Paths.get(
                localPackage.getEngineFolder().getAbsolutePath(),
                MODULES,
                PINNACLE_21,
                VALIDATION,
                localPackage.getEngineVersion(),
                SLASH 
            ).toString()
        );
        
        StringBuilder jarPath = new StringBuilder(ENGINE_MODULES);
        jarPath.append(localPackage.getEngineVersion());
        jarPath.append(SLASH);
        Predicate<String> jarFilter = JarUtils.jarPredicate(null, jarPath.toString());
        S3Loader.getResources()
            .map(resources -> resources.orElseGet(ResourcesData::new))
            .map(ResourcesData::getEngines)
            .map(ListUtils::emptyIfNull)
            .map(engines -> {
                return engines.stream()
                    .filter(engine -> engine.getName().equals(localPackage.getEngineVersion()))
                    .map(ResourcesData.EngineResource::getLibs)
                    .map(ListUtils::emptyIfNull)
                    .flatMap(List::stream)
                    .map(ResourcesData.FileResource::getUri)
                    .collect(Collectors.toList());
            })
            .toObservable()
            .flatMap(Observable::from)
            .filter(uri -> jarFilter.apply(uri))
            .doOnNext(uri -> S3Loader.loadResourceFromS3(uri, engineFolder.getPath()).await())
            .toCompletable()
            .await();

        ValidatorSource validatorSource = toValidatorSource(localPackage.getDataPackage());
        ValidatorOptions validatorOptions = toValidatorOptions(localPackage);
        return ValidatorUtils.validate(
            validatorSource,
            validatorOptions,
            callback
        )
            .map(validatorMetrics -> {
                long len = callback.getLength();
                String reportFile = localPackage.getDataPackage()
                    .getReportSource()
                    .getDataSource().getAbsolutePath();
                listener.listen(ProcessEvent.Builder.<ValidatorMetrics>builder()
                    .setTarget(validatorMetrics)
                    .setState(ProcessState.TASK_SUBPROCESSING)
                    .setTotal(len)
                    .setCurrent(len)
                    .setLabel(toLabel("Generating Report",
                        FilenameUtils.getName(reportFile)))
                    .build());
                ValidatorUtils.generateReport(validatorMetrics);
                ValidatorUtils.uploadMetrics(validatorSource);
                return new ValidatorResponse(
                    excelService.getValidationResults(localPackage.getDataPackage()),
                    reportFile
                );
            });
    }

    private ValidatorOptions toValidatorOptions(final LocalDataPackage dataPackage) {
        final DataPackage dp = dataPackage.getDataPackage();

        List<StandardDef> standards = Arrays.stream(Terminologies.values())
            .filter(t -> t.getVersion(dp).isProvided())
            .filter(terminologies -> terminologies
                .containStandard(dp.getStandard().get().getFamily())
            )
            .map(terminology -> {
                ResourceUtils.MetadataResource metadata = getMetadataPath(dataPackage, terminology, true);
                String stdName;
                StandardFamily family = terminology.toStandardFamily();
                if (StandardFamily.isValid(family)) {
                    stdName = String.format(CoreNames.CDISC_NAME, family.getValue());
                } else {
                    stdName = CoreNames.CDISC_COMMON_NAME.getValue();
                }
                return StandardDefBuilder.builder()
                    .setStandard(stdName)
                    .setResource(metadata)
                    .build();
            })
            .filter(stdDef -> stdDef != null && !Strings.isNullOrEmpty(stdDef.getVersion()))
            .collect(Collectors.toList());

        Terminologies stdTerminology = Terminologies.fromStandardFamily(dp.getStandard().get().getFamily());
        StandardDef stdDef = null;

        if (stdTerminology != null) {
            ResourceUtils.MetadataResource metadata = getMetadataPath(dataPackage, stdTerminology, false);
            stdDef = StandardDefBuilder.builder()
                .setStandard(CoreNames.CDISC_COMMON_NAME.getValue())
                .setResource(metadata)
                .build();
        }

        List<StandardDef> dictionaries = Arrays.stream(Dictionaries.values())
            .filter(dictionary -> dictionary.getVersion(dp).isProvided())
            .filter(dictionary -> dictionary.containStandard(dp.getStandard().get().getFamily()))
            .map(dictionary -> StandardDefBuilder.builder()
                .setMapper(dictionary)
                .setResource(getMetadataPath(dataPackage, dictionary, true))
                .build()
            )
            .filter(stdDef1 -> !Strings.isNullOrEmpty(stdDef1.getVersion()))
            .collect(Collectors.toList());

        ImmutableList.Builder<StandardDef> allStandards = ImmutableList
            .<StandardDef>builder().addAll(standards).addAll(dictionaries);
        if (stdDef != null) {
            allStandards.add(stdDef);
        }

        LOG.info("Engine folder: " + dataPackage.getEngineFolder().getAbsolutePath());
        ValidatorOptions.Builder builder = new ValidatorOptions.Builder(
            toFile(dataPackage.getConfigInfo().getConfig())
        )
            .withStandard(toStandardDef(dataPackage))
            .withStandards(allStandards.build())
            .withDefine(findDefine(dataPackage.getDataPackage()))
            .withThreadCount(dataPackage.getThreadCount())
            .withGenerateRuleMetrics(dataPackage.isGenerateRuleMetrics())
            .withUseEnhancedSas(dataPackage.isUseEnhancedSas())
            .withAutoDisplayDomainKeys(dataPackage.isAutoDisplayDomainKeys())
            .withVersion(dataPackage.getEngineVersion())
            .withModulesPath(
                Paths.get(dataPackage.getEngineFolder().getAbsolutePath(), "modules").toString()
            )
            .withReportFile(toFile(dp.getReportSource().getDataSource()))
            .withReportCutoff(dp.getReportSource().getCutoff())
            .withUserId(dataPackage.getUserId())
            .withAnonymousId(dataPackage.getAnonymousId())
            .withApplicationVersion(dataPackage.getApplicationVersion());
        if (dataPackage.getConfigInfo().getStandardType() == StandardType.DEFINE) {
            File configFile = new File(dataPackage.getConfigInfo().getConfig().getAbsolutePath());
            File parentDir = configFile.getParentFile();
            File defineStandardFile = new File(parentDir, dp.getStandard().get().toFileName());
            if (!defineStandardFile.exists()) {
                StandardType.Standard standard = new StandardType.Standard(
                    dp.getStandard().get(),
                    dp.getStandardVersion().get(),
                    dp.getStandardFilter().get()
                );
                defineStandardFile = new File(parentDir, standard.toName());
            }
            builder.withDefineStandard(defineStandardFile.getPath());
        }
        return builder.build();
    }
}
