package net.pinnacle21.oce.services;

import com.google.common.base.Strings;
import net.bootstrap.api.define.DefineUtils;
import net.bootstrap.api.model.DefineDocument;
import net.bootstrap.core.callback.EngineCallback;
import net.pinnacle21.oce.events.Listener;
import net.pinnacle21.oce.events.ProcessEvent;
import net.pinnacle21.oce.events.StateEvent;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.exceptions.ValidationExceptions;
import net.pinnacle21.oce.model.ConverterInfo;
import net.pinnacle21.oce.model.DefineConversionInfo;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.ProcessState;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datasource.AbstractFileDS;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.input.ConfigInfo;
import net.pinnacle21.oce.utils.DataSources;
import net.pinnacle21.oce.utils.ProcessUtils;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.functions.Func1;

import java.io.*;
import java.util.Collection;
import java.util.function.Predicate;

import static net.pinnacle21.oce.utils.CliPreconditions.*;
import static net.pinnacle21.oce.utils.DataSources.toFile;
import static net.pinnacle21.oce.utils.ProcessUtils.toLabel;

/**
 * Define service provides convert method that will convert Excel spec to define.xml
 */
public class DefineService {
    private static final Logger LOG = LoggerFactory.getLogger(DefineService.class);

    private static void writeToFile(InputStream inputStream, File outFile) {
        try (InputStream sourceStream = inputStream;
                OutputStream outputStream = new FileOutputStream(outFile)) {
            IOUtils.copy(sourceStream, outputStream);
        } catch (IOException e) {
            throw new CLIException(SystemExceptions.FAILED_OUTPUT_FILE,
                String.format("Field to write file='%s'", outFile.getAbsolutePath()), e);
        }
    }

    /**
     * Convert define to excel spec
     */
    public Observable<File> convert(final DefineConversionInfo defineInfo,
                                    final Listener<StateEvent> listener) {
        EngineCallback callback = new EngineCallback(EngineCallbackBuilder
            .builder().setCliModel(defineInfo)) {
            @Override
            public void tell(String origin, String destination, long pos) {
                ProcessEvent.Builder<DefineConversionInfo> builder = ProcessEvent.Builder.<DefineConversionInfo>builder()
                    .setTotal(getLength())
                    .setCurrent(pos)
                    .setLabel(toLabel(origin, destination))
                    .setState(ProcessState.TASK_PROCESSING)
                    .setTarget(defineInfo);

                listener.listen(builder.build());
            }
        };

        return DefineUtils.generateSpec(toFile(defineInfo.getXmlFile()), callback)
            .map(inputStream -> {
                File xlFile = ((AbstractFileDS) defineInfo.getExcelFile()).getFile();
                if (!Strings.isNullOrEmpty(inputStream.getStudyName())) {
                    xlFile = new File(xlFile.getParentFile(),
                        FormatType.DEFINE_SPEC.toDefaultFileName(inputStream.getStudyName()));
                }
                writeToFile(inputStream.getStream(), xlFile);
                return xlFile;
            });
    }

    Observable<DefineDocument> getOrCreateDefineDocument(ConverterInfo converterInfo,
                                                         final Listener<StateEvent> listener) {
        if (converterInfo.getDefine() != DataSource.EMPTY) {
            // define source is provided -> create document
            return DefineUtils.parseDefine(converterInfo.getDefine().getAbsolutePath());
        } else {
            // generate define document
            return createDefine(converterInfo, listener)
                .flatMap((Func1<InputStream, Observable<DefineDocument>>) DefineUtils::parseDefine);
        }
    }

    public Observable<InputStream> createDefine(final ConverterInfo converterInfo,
                                                final Listener<StateEvent> listener) {
        // generate define document
        checkProvided(converterInfo,
            "Cannot generate define xml. converter info cannot be null");

        DataPackage dataPackage = converterInfo.getDataPackage();
        ConfigInfo config = converterInfo.getConfig();

        checkProvided(dataPackage, ValidationExceptions.MISSING_DATA_SOURCE,
            "Cannot generate define xml. Missing data source");

        EngineCallback callback = new EngineCallback(EngineCallbackBuilder
            .builder()
            .setCliModel(converterInfo)) {
            @Override
            public void tell(String origin, String destination, long pos) {
                ProcessEvent.Builder<ConverterInfo> builder = ProcessEvent.Builder.<ConverterInfo>builder()
                    .setTotal(getLength())
                    .setCurrent(pos)
                    .setLabel(toLabel(origin, destination))
                    .setState(ProcessState.TASK_PROCESSING)
                    .setTarget(converterInfo);

                listener.listen(builder.build());
            }
        };
        Predicate<File> fileFilter = FormatType.XPORT::isApplicable;

        Collection<File> files = checkCollection(DataSources.toFiles(dataPackage.getSource(), fileFilter),
            ValidationExceptions.MISSING_DATA_SOURCE);
        File configFile = exists(toFile(config.getConfig()), SystemExceptions.EXISTS_FILE,
            "Failed to generate define. Config file is missing");

        Observable<DefineUtils.SpecStream> oSpec;
        if (converterInfo.getOutputFormatType() == FormatType.DEFINE_SPEC) {
            oSpec = DefineUtils.generateSpec(files, configFile, null, callback);
        } else {
            oSpec = DefineUtils.generateDefine(
                files,
                configFile,
                null,
                callback,
                ProcessUtils.COMMUNITY_VERSION,
                ProcessUtils.SOURCE_SYSTEM
            );
        }

        return oSpec.map(DefineUtils.SpecStream::getStream);
    }


}
