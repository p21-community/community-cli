package net.pinnacle21.oce.modules;

import dagger.Module;
import dagger.Provides;
import net.pinnacle21.oce.services.*;

import javax.inject.Singleton;

/**
 * Module provides all services
 */

@Module
public class ServicesModule {

    @Provides
    @Singleton
    static ValidationService providesValidationService(ExcelService excelService) {
        return new ValidationService(excelService);
    }

    @Provides
    @Singleton
    static DefineService providesDefineService(ExcelService excelService) {
        return new DefineService();
    }

    @Provides
    @Singleton
    static ExcelService provideExcelService() {
        return new ExcelService();
    }

    @Provides
    @Singleton
    static JsonService provideJsonService() {
        return new JsonService();
    }

    @Provides
    @Singleton
    static S3DownloadService provideS3DownloadService() {
        return new S3DownloadService();
    }

    @Provides
    @Singleton
    static ConverterService providesConverterService(DefineService defineService) {
        return new ConverterService(defineService);
    }
}
