package net.pinnacle21.oce.modules;

import dagger.Module;
import dagger.Provides;
import net.pinnacle21.oce.clients.BootstrapCli;
import net.pinnacle21.oce.services.*;

import javax.inject.Singleton;

/**
 * Application module
 */

@Module
public class AppModule {
    @Provides
    @Singleton
    static BootstrapCli providesCommunityCLI(DefineService defineService,
                                             ConverterService converterService,
                                             ValidationService validationService,
                                             JsonService jsonService) {
        return new BootstrapCli(defineService, converterService, validationService, jsonService);
    }
}
