package net.pinnacle21.oce.events;


import net.pinnacle21.oce.model.response.Response;

public interface Listener<T extends StateEvent> {
    void listen(T event);

    Response getResponse();
}
