package net.pinnacle21.oce.events;

import com.google.common.base.Strings;
import net.pinnacle21.oce.model.ProcessState;
import net.pinnacle21.oce.model.response.ExceptionResponse;
import net.pinnacle21.oce.utils.CliErrorUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionEvent extends StateEvent<ExceptionResponse, ProcessState> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionEvent.class);

    private ExceptionEvent(ExceptionResponse target,
                           Throwable error,
                           String label,
                           long total,
                           long current) {
        super(target, ProcessState.EXCEPTION, error, label, total, current);
        LOGGER.debug("Building exception event for target {}",
            target, error);
        LOGGER.error(target.getMessage(), error);
    }

    public static class Builder extends StateEvent.Builder<ExceptionResponse, ProcessState, ExceptionEvent, Builder> {
        public static Builder builder() {
            return new ExceptionEvent.Builder();
        }

        @Override
        protected Builder getThis() {
            return this;
        }

        @Override
        public ExceptionEvent build() {
            if (this.target == null) {
                // check exception and find CLI code
                this.target = new ExceptionResponse(this.error);
            }

            return new ExceptionEvent(this.target,
                this.error,
                getLabel(),
                this.total,
                this.current);
        }

        private String getLabel() {
            String errStr = Strings.nullToEmpty(this.label);

            if (StringUtils.isNotBlank(this.target.getException())) {
                errStr = errStr + "Exception: " + this.target.getException();
            } else if (this.error != null) {
                errStr = errStr + "Exception: " + CliErrorUtils.report(this.error);
            }

            return errStr;
        }
    }

}
