package net.pinnacle21.oce.events;

import net.pinnacle21.oce.model.ProcessState;

public class ProcessEvent<T> extends StateEvent<T, ProcessState> {
    private ProcessEvent(T target, ProcessState state, String label, long total, long current) {
        super(target, state, null, label, total, current);
    }

    public static class Builder<T> extends StateEvent.Builder<T, ProcessState, ProcessEvent<T>, ProcessEvent.Builder<T>> {
        public static <T> Builder<T> builder() {
            return new Builder<T>();
        }

        @Override
        ProcessEvent.Builder<T> getThis() {
            return this;
        }

        @Override
        public ProcessEvent<T> build() {
            return new ProcessEvent<>(this.target,
                this.state, this.label, this.total, this.current);
        }
    }
}
