package net.pinnacle21.oce.events;

public interface State<S extends State> {
    S getState();

    boolean isCompleted();
}
