package net.pinnacle21.oce.events;

import com.google.common.base.Strings;
import net.pinnacle21.oce.model.ProcessState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.bootstrap.core.utils.OSUtils.getOSManufacturer;

public class CommandLineReporter implements Reporter {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandLineReporter.class);
    private static final boolean NEED_ESCAPE = getOSManufacturer().toLowerCase().contains("linux");

    private static void updateProgress(String label,
                                       double progressPercentage) {
        final int width = 10; // progress bar width in chars

        if (NEED_ESCAPE) {
            System.out.print("\33[1A\33[2K");
        }

        System.out.print("\r" + label + " [");
        int i = 0;
        for (; i <= (int) (progressPercentage * width); i++) {
            System.out.print(".");
        }
        for (; i < width; i++) {
            System.out.print(" ");
        }
        System.out.print("]");
        System.out.flush();
    }

    private static String getLabel(StateEvent event) {
        return event == null || Strings.isNullOrEmpty(event.getLabel()) ? "" : event.getLabel() + ": ";
    }

    @Override
    public void report(StateEvent event) {
        if (event.getState() == ProcessState.STARTED) {
            System.out.println(event.getLabel());
        } else if (event.getState() == ProcessState.EXCEPTION) {
            String completeMsg = getLabel(event) + "Process error.";
            LOGGER.error(completeMsg, event.getError());
        } else if (event.getState().isCompleted()) {
            String completeMsg = getLabel(event) + "Process completed.";
            LOGGER.info(completeMsg);
        } else if (event.getTotal() > 0) {
            updateProgress(event.getLabel(),
                (double) event.getCurrent() / event.getTotal());
        }

        LOGGER.debug("Report event [{}] state {} for target {}",
            event.getLabel(), event.getState(), event.getTarget());
    }
}
