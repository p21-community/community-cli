package net.pinnacle21.oce.events;

public class StateEvent<T, S extends State> {
    private final T target;
    private final S state;
    private final Throwable error;
    private final String label;
    private long total;
    private long current;

    StateEvent(T target, S
        state, String label, long total) {
        this(target, state, null, label, total, 0);
    }


    StateEvent(T target,
               S state,
               Throwable error,
               String label,
               long total,
               long current) {
        this.target = target;
        this.state = state;
        this.error = error;
        this.label = label;
        this.total = total;
        this.current = current;
    }

    public T getTarget() {
        return target;
    }

    public S getState() {
        return state;
    }

    public Throwable getError() {
        return error;
    }

    public String getLabel() {
        return label;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getCurrent() {
        return current;
    }

    public void setCurrent(long current) {
        this.current = current;
    }

    public static abstract class Builder<T, S, R extends StateEvent, B> {
        T target;
        S state;
        Throwable error;
        String label;
        long total;
        long current;

        abstract B getThis();

        public B setTarget(T target) {
            this.target = target;
            return getThis();
        }

        public B setState(S state) {
            this.state = state;
            return getThis();
        }

        public B setError(Throwable error) {
            this.error = error;
            return getThis();
        }

        public B setLabel(String label) {
            this.label = label;
            return getThis();
        }

        public B setTotal(long total) {
            this.total = total;
            return getThis();
        }

        public B setCurrent(long current) {
            this.current = current;
            return getThis();
        }

        public abstract R build();
    }
}
