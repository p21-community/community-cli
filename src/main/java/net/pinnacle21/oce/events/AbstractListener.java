package net.pinnacle21.oce.events;

import net.pinnacle21.oce.model.response.Response;

public abstract class AbstractListener<T extends StateEvent> implements Listener<T> {
    private Response response;

    @Override
    public Response getResponse() {
        return response;
    }

    protected void setResponse(T event) {
        if (event.getState().isCompleted()) {
            if (event.getTarget() instanceof Response) {
                this.response = (Response) event.getTarget();
            }
        }
    }
}
