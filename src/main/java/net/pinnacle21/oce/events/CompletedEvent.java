package net.pinnacle21.oce.events;

import net.pinnacle21.oce.model.ProcessState;
import net.pinnacle21.oce.model.response.Response;

public class CompletedEvent<T extends Response> extends StateEvent<T, ProcessState> {
    private CompletedEvent(T target, String label, long total, long current) {
        super(target, ProcessState.COMPLETED, null, label, total, current);
    }

    public static class Builder<T extends Response>
        extends StateEvent.Builder<T, ProcessState, CompletedEvent<T>, CompletedEvent.Builder<T>> {

        public static <T extends Response> CompletedEvent.Builder<T> builder() {
            return new Builder<>();
        }

        @Override
        protected Builder<T> getThis() {
            return this;
        }

        @Override
        public CompletedEvent<T> build() {
            return new CompletedEvent<>(this.target,
                this.label, this.total, this.current);
        }
    }
}
