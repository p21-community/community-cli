package net.pinnacle21.oce.events;

public interface Reporter {
    void report(StateEvent event);
}
