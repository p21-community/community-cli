package net.pinnacle21.oce.starters.miner;

import net.pinnacle21.oce.clients.BootstrapCli;
import net.pinnacle21.oce.events.CompletedEvent;
import net.pinnacle21.oce.events.Listener;
import net.pinnacle21.oce.events.StateEvent;
import net.pinnacle21.oce.model.request.MinerRequest;
import net.pinnacle21.oce.model.request.Request;
import net.pinnacle21.oce.model.response.SuccessResponse;
import net.pinnacle21.oce.starters.RequestStarter;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

public class MinerStarter implements RequestStarter {
    @Override
    public Observable<Void> start(final Request request, final Listener<StateEvent> listener) {
        final MinerRequest minerRequest = (MinerRequest) request;

        return BootstrapCli.getInstance(minerRequest.getEngineVersion())
            .mine(minerRequest, listener)
            .doOnNext(new Action1<Integer>() {
                @Override
                public void call(Integer result) {
                    listener.listen(CompletedEvent.Builder.<SuccessResponse>builder()
                        .setTarget(new SuccessResponse(result,
                            minerRequest.getOutput().getAbsolutePath()))
                        .setLabel("miner")
                        .build());
                }
            })
            .map(new Func1<Integer, Void>() {
                @Override
                public Void call(Integer integer) {
                    return null;
                }
            });
    }
}
