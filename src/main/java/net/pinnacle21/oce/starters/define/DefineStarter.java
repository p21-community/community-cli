package net.pinnacle21.oce.starters.define;

import net.pinnacle21.oce.clients.BootstrapCli;
import net.pinnacle21.oce.events.CompletedEvent;
import net.pinnacle21.oce.events.Listener;
import net.pinnacle21.oce.events.StateEvent;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.model.DefineConversionInfo;
import net.pinnacle21.oce.model.request.DefineRequest;
import net.pinnacle21.oce.model.request.Request;
import net.pinnacle21.oce.model.request.RequestType;
import net.pinnacle21.oce.model.response.SuccessResponse;
import net.pinnacle21.oce.starters.RequestStarter;
import org.apache.commons.io.FilenameUtils;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

import java.util.List;

import static net.pinnacle21.oce.utils.ProcessUtils.buildStartEvent;

public class DefineStarter implements RequestStarter {
    @Override
    public Observable<Void> start(final Request request, final Listener<StateEvent> listener) {
        List<DefineConversionInfo> infos = null;

        if (request.getType() == RequestType.DEFINE) {
            infos = ((DefineRequest) request).getInfos();
        }

        if (infos != null && !infos.isEmpty()) {
            String engineVersion = infos.get(0).getEngineVersion();

            listener.listen(
                buildStartEvent(
                    infos.get(0),
                    String.format(
                        "Convert define from %s to %s",
                        FilenameUtils.getName(infos.get(0).getExcelFile().getAbsolutePath()),
                        FilenameUtils.getName(infos.get(0).getXmlFile().getAbsolutePath())
                    ),
                    infos.size()
                )
            );

            Observable<Integer> oResult = BootstrapCli.getInstance(engineVersion)
                .convertDefine(infos, listener).map(new Func1<Void, Integer>() {
                    @Override
                    public Integer call(Void aVoid) {
                        return 0;
                    }
                });

            return oResult.doOnNext(new Action1<Integer>() {
                @Override
                public void call(Integer result) {
                    List<DefineConversionInfo> processedInfos = ((DefineRequest) request).getInfos();

                    listener.listen(CompletedEvent.Builder.<SuccessResponse>builder()
                        .setTarget(
                            new SuccessResponse(
                                result, processedInfos.get(0).getXmlFile().getAbsolutePath()
                            )
                        )
                        .setTotal(processedInfos.size())
                        .setCurrent(processedInfos.size())
                        .setLabel("convert-define")
                        .build());
                }
            }).map(new Func1<Integer, Void>() {
                @Override
                public Void call(Integer integer) {
                    return null;
                }
            });
        }

        return Observable.error(
            new CLIException(
                String.format("Unexpected request for validation. type=%s", request.getType())
            )
        );
    }
}
