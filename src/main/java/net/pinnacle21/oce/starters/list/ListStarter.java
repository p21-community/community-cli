package net.pinnacle21.oce.starters.list;

import net.pinnacle21.oce.clients.BootstrapCli;
import net.pinnacle21.oce.events.CompletedEvent;
import net.pinnacle21.oce.events.Listener;
import net.pinnacle21.oce.events.StateEvent;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.model.ListInfo;
import net.pinnacle21.oce.model.request.ListConfigRequest;
import net.pinnacle21.oce.model.request.Request;
import net.pinnacle21.oce.model.response.SuccessResponse;
import net.pinnacle21.oce.starters.RequestStarter;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

import static net.pinnacle21.oce.utils.ProcessUtils.buildStartEvent;

public class ListStarter implements RequestStarter {
    @Override
    public Observable<Void> start(final Request request, final Listener<StateEvent> listener) {
        final ListInfo listInfo = ((ListConfigRequest) request).getListInfo();

        if (listInfo != null) {
            listener.listen(buildStartEvent(listInfo,
                "List community info", 1));
            Observable<Integer> oResult = BootstrapCli.getInstance(listInfo.getEngineVersion())
                .listOptions(listInfo, listener);

            return oResult.doOnNext(new Action1<Integer>() {
                @Override
                public void call(Integer result) {
                    listener.listen(CompletedEvent.Builder.<SuccessResponse>builder()
                        .setTarget(new SuccessResponse(result,
                            listInfo.getOutput().getAbsolutePath()))
                        .setTotal(result)
                        .setCurrent(result)
                        .setLabel("list-config")
                        .build());
                }
            }).map(new Func1<Integer, Void>() {
                @Override
                public Void call(Integer integer) {
                    return null;
                }
            });
        }

        return Observable.error(new CLIException(String.format("Unexpected request for list config option. type=%s",
            request.getType())));
    }
}
