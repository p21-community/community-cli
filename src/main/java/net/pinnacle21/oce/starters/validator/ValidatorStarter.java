package net.pinnacle21.oce.starters.validator;

import com.google.common.collect.Lists;
import net.pinnacle21.oce.clients.BootstrapCli;
import net.pinnacle21.oce.events.CompletedEvent;
import net.pinnacle21.oce.events.Listener;
import net.pinnacle21.oce.events.StateEvent;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datapackage.LocalDataPackage;
import net.pinnacle21.oce.model.request.Request;
import net.pinnacle21.oce.model.request.RequestType;
import net.pinnacle21.oce.model.request.ValidatorRequest;
import net.pinnacle21.oce.starters.RequestStarter;
import net.pinnacle21.oce.utils.IPC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;

import javax.annotation.Nullable;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static net.pinnacle21.oce.utils.ProcessUtils.buildStartEvent;

public class ValidatorStarter implements RequestStarter {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidatorStarter.class);

    @Override
    @SuppressWarnings("unchecked")
    public Observable<Void> start(Request request, final Listener<StateEvent> listener) {
        List<LocalDataPackage> localPackages = null;
        final List<DataPackage> packages = Lists.newArrayList();
        String engineVersion;

        if (request.getType() == RequestType.VALIDATOR) {
            localPackages = ((ValidatorRequest) request).getPackages();
            packages.addAll(localPackages.stream().map(new Function<LocalDataPackage, DataPackage>() {
                @Nullable
                @Override
                public DataPackage apply(@Nullable LocalDataPackage input) {
                    return input != null ? input.getDataPackage() : null;
                }
            }).collect(Collectors.toList()));
        }

        if (!packages.isEmpty()) {
            engineVersion = packages.get(0).getEngineVersion();

            listener.listen(
                    buildStartEvent(
                            packages.get(0),
                            String.format(
                                    "Validate %s",
                                    packages.get(0).getSource().getAbsolutePath()
                            ),
                            packages.get(0).getSource().getCount()
                    )
            );


            return BootstrapCli.getInstance(engineVersion)
                    .validate(localPackages, listener)
                    .doOnNext(response ->
                            listener.listen(CompletedEvent.Builder.builder()
                                    .setLabel("Validation")
                                    .setTotal(packages.size())
                                    .setCurrent(packages.size())
                                    .setTarget(response)
                                    .build())
                    )
                    .doOnError(IPC::send)
                    .map(response -> null);
        }

        return Observable.error(new CLIException(String.format("Unexpected request for validation. type=%s",
                request.getType())));
    }
}
