package net.pinnacle21.oce.starters;

import net.pinnacle21.oce.events.Listener;
import net.pinnacle21.oce.events.StateEvent;
import net.pinnacle21.oce.model.request.Request;
import rx.Observable;

public interface RequestStarter {
    Observable<Void> start(Request request, Listener<StateEvent> listener);
}
