package net.pinnacle21.oce.starters.converter;

import net.bootstrap.core.model.FileType;
import net.pinnacle21.oce.clients.BootstrapCli;
import net.pinnacle21.oce.events.Listener;
import net.pinnacle21.oce.events.StateEvent;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.model.ConverterInfo;
import net.pinnacle21.oce.model.request.ConverterRequest;
import net.pinnacle21.oce.model.request.Request;
import net.pinnacle21.oce.model.request.RequestType;
import net.pinnacle21.oce.starters.RequestStarter;
import rx.Observable;
import rx.functions.Func1;

import java.io.File;

import static net.pinnacle21.oce.utils.ProcessUtils.buildStartEvent;

public class ConverterStarter implements RequestStarter {
    @Override
    public Observable<Void> start(Request request, Listener<StateEvent> listener) {
        ConverterInfo converterInfo = null;

        if (request.getType() == RequestType.CONVERTER) {
            converterInfo = ((ConverterRequest) request).getInfo();
        }

        if (converterInfo != null) {
            String engineVersion = converterInfo.getEngineVersion();

            if (isGenerateDefine(converterInfo)) {
                // generate define xml or define spec
                listener.listen(buildStartEvent(converterInfo,
                    String.format("Generate define file %s",
                        converterInfo.getOutput().getAbsolutePath()), 1));
                return BootstrapCli.getInstance(engineVersion)
                    .generateDefine(converterInfo, listener)
                    .map(new Func1<File, Void>() {
                        @Override
                        public Void call(File file) {
                            return null;
                        }
                    });
            } else {
                // convert data
                listener.listen(buildStartEvent(converterInfo,
                    String.format("Convert data to %s into %s",
                        converterInfo.getOutputFormatType(),
                        converterInfo.getOutput().getAbsolutePath()),
                    converterInfo.getSources().size()));

                return BootstrapCli.getInstance(engineVersion).converter(converterInfo, listener);
            }
        }

        return Observable.error(new CLIException(String.format("Unexpected request for conversion. type=%s",
            request.getType())));
    }

    private boolean isGenerateDefine(ConverterInfo converterInfo) {
        return converterInfo.getOutputFormatType().getBootstrapType() == FileType.DEFINE;
    }
}
