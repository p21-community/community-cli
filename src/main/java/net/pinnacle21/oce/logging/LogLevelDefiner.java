package net.pinnacle21.oce.logging;

import ch.qos.logback.core.PropertyDefinerBase;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.File;
import java.util.Set;

import static net.bootstrap.core.utils.jar.JarUtils.getAppPath;
import static net.pinnacle21.oce.utils.Constants.CONF_FILE;
import static net.pinnacle21.oce.utils.Constants.LOG_LEVEL_PROPERTY;

public class LogLevelDefiner extends PropertyDefinerBase {
    private static final Set<String> LEVELS = Sets.newTreeSet(String.CASE_INSENSITIVE_ORDER);

    static {
        LEVELS.addAll(Lists.newArrayList("info", "debug", "trace", "error"));
    }

    private static String checkConfFile() {
        String level = "info";

        File appConf = new File(getAppPath(LogLevelDefiner.class), CONF_FILE);
        if (appConf.exists()) {
            Config config = ConfigFactory.parseFile(appConf);
            level = config.hasPath(LOG_LEVEL_PROPERTY) ? config.getString(LOG_LEVEL_PROPERTY) : level;
        }

        if (LEVELS.contains(level)) {
            return level;
        }

        return "info";
    }

    @Override
    public String getPropertyValue() {
        String level = System.getProperty(LOG_LEVEL_PROPERTY);
        if (Strings.isNullOrEmpty(level)) {
            return checkConfFile();
        }

        if (LEVELS.contains(level)) {
            return level;
        }

        return "info";
    }
}
