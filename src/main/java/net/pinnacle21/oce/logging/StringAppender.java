package net.pinnacle21.oce.logging;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.UnsynchronizedAppenderBase;

public class StringAppender extends UnsynchronizedAppenderBase<ILoggingEvent> {
    private static MessageBuilder MSG_BUILDER = new MessageBuilder();
    private static int exitCode;

    public StringAppender() {
        setName("StringAppender");
    }

    public static String getMessage() {
        return MSG_BUILDER.getMessage();
    }

    public static int getExitCode() {
        return exitCode;
    }

    public static void setExitCode(int code) {
        MSG_BUILDER.append(String.format("exit=%d", code));
        exitCode = code;
    }

    @Override
    protected void append(ILoggingEvent iLoggingEvent) {
        if (iLoggingEvent.getLevel() == Level.ERROR) {
            MSG_BUILDER.append(iLoggingEvent.getFormattedMessage());
        }
    }

    public static class MessageBuilder {
        private static StringBuilder message = new StringBuilder();

        synchronized void append(String msg) {
            message.append(msg);
            message.append('\n');
        }

        public String getMessage() {
            return message.toString();
        }
    }
}
