package net.pinnacle21.oce.logging;

import ch.qos.logback.core.PropertyDefinerBase;

import static net.pinnacle21.oce.utils.DataSources.getLogFile;

public class JarNameDefiner extends PropertyDefinerBase {
    @Override
    public String getPropertyValue() {
        return getLogFile();
    }
}
