package net.pinnacle21.oce.logging;

import ch.qos.logback.core.PropertyDefinerBase;
import org.apache.commons.lang3.StringUtils;

import static net.bootstrap.core.utils.jar.URLClassLoaderUtils.APPEND_LIB_LOG;

public class LogAppendFlagDefiner extends PropertyDefinerBase {
    @Override
    public String getPropertyValue() {
        return StringUtils.defaultIfBlank(System.getProperty(APPEND_LIB_LOG), "false");
    }
}
