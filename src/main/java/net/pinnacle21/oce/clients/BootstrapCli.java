package net.pinnacle21.oce.clients;

import com.google.common.base.Joiner;
import net.bootstrap.api.converter.ConverterResult;
import net.bootstrap.api.converter.DestinationProxy;
import net.bootstrap.api.define.DefineUtils;
import net.bootstrap.api.miner.MinerOptions;
import net.bootstrap.api.miner.MinerUtils;
import net.bootstrap.api.model.data.CommunityConfigData;
import net.bootstrap.config.ConfigContext;
import net.bootstrap.config.ConfigUtils;
import net.bootstrap.core.callback.EngineCallback;
import net.bootstrap.core.utils.jar.JarUtils;
import net.pinnacle21.oce.events.*;
import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.SystemExceptions;
import net.pinnacle21.oce.model.*;
import net.pinnacle21.oce.model.datapackage.LocalDataPackage;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.request.MinerRequest;
import net.pinnacle21.oce.model.response.ConverterResponse;
import net.pinnacle21.oce.model.response.ExceptionResponse;
import net.pinnacle21.oce.model.response.SuccessResponse;
import net.pinnacle21.oce.model.response.ValidatorResponse;
import net.pinnacle21.oce.services.*;
import net.pinnacle21.oce.utils.ProcessUtils;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.functions.Func1;

import java.io.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static net.bootstrap.utils.S3Loader.isHealthy;
import static net.pinnacle21.oce.utils.CliErrorUtils.getProcessException;
import static net.pinnacle21.oce.utils.CliErrorUtils.propagate;
import static net.pinnacle21.oce.utils.DataSources.toFile;
import static net.pinnacle21.oce.utils.ProcessUtils.toLabel;

public class BootstrapCli {
    private static final Logger LOGGER = LoggerFactory.getLogger(BootstrapCli.class);
    private static final int DICTIONARIES_INSTALL_LIMIT = 5;
    private static BootstrapCli instance;
    private final DefineService defineService;
    private final ConverterService converterService;
    private final ValidationService validationService;
    private final JsonService jsonService;

    public BootstrapCli(DefineService defineService,
                        ConverterService converterService,
                        ValidationService validationService,
                        JsonService jsonService) {
        this.defineService = defineService;
        this.converterService = converterService;
        this.validationService = validationService;
        this.jsonService = jsonService;
    }

    public static BootstrapCli getInstance(String engineVersion) {
        if (instance == null) {
            synchronized (BootstrapCli.class) {
                if (instance == null) {
                    instance = DaggerClients.create().bootstrapClient();
                }
            }
        }

        LOGGER.info("Engine Version: {}", engineVersion);
        return instance;
    }

    private static String toTransaction(List<ConverterResult> converterResults) {
        return Joiner.on(", ")
            .join(
                converterResults.stream()
                    .filter(input -> input != null && input.isTruncated())
                    .map(input -> input.getFile().getName())
                    .collect(Collectors.toList())
            );
    }

    public Observable<Void> converter(final ConverterInfo converterInfo,
                                      final Listener<StateEvent> listener) {
        if (converterInfo == null) {
            return Observable.just(null);
        }

        return converterService.convert(converterInfo, listener)
            .toList()
            .map((Func1<List<ConverterResult>, Void>) converterResults -> {

                listener.listen(CompletedEvent.Builder.<ConverterResponse>builder()
                    .setTarget(new ConverterResponse(converterInfo.getOutput().getAbsolutePath(),
                        toTransaction(converterResults)))
                    .setTotal(converterResults.size())
                    .setCurrent(converterResults.size())
                    .setLabel("converter")
                    .build());

                return null;
            })
            .onErrorReturn(e -> {
                if (e instanceof DestinationProxy.DatasetXMLException) {
                    listener.listen(ExceptionEvent.Builder.<ExceptionResponse>builder()
                        .setTarget(new ExceptionResponse(e.getLocalizedMessage()))
                        .setError(e)
                        .build());

                    return null;
                } else {
                    throw propagate(e, new CLIException(getProcessException(e),
                        String.format("Failed converter process. converterInfo=%s",
                            converterInfo.toString()), e));
                }
            });
    }

    public Observable<File> generateDefine(final ConverterInfo converterInfo,
                                           final Listener<StateEvent> listener) {
        if (converterInfo == null) {
            return Observable.just(null);
        }
        if (converterInfo.getSourceFormatType() == FormatType.DEFINE_XML &&
            converterInfo.getOutputFormatType() == FormatType.DEFINE_SPEC) {
            // convert define XML to define SPEC
            return defineService.convert(DefineConversionInfo.Builder.builder()
                .fromConverter(converterInfo)
                .build(), listener)
                .doOnNext(file -> listener.listen(CompletedEvent.Builder.<SuccessResponse>builder()
                    .setTarget(new SuccessResponse(0,
                        file.getAbsolutePath()))
                    .setLabel("define spec")
                    .build()));
        }

        return defineService.createDefine(converterInfo, listener)
            .map(inputStream -> {
                File outputFile = toFile(converterInfo.getOutput());
                try (InputStream sourceStream = inputStream;
                        OutputStream outputStream = new FileOutputStream(outputFile)) {
                    IOUtils.copy(sourceStream, outputStream);
                    listener.listen(CompletedEvent.Builder.<SuccessResponse>builder()
                        .setTarget(new SuccessResponse(0,
                            converterInfo.getOutput().getAbsolutePath()))
                        .setLabel("create-define")
                        .build());
                } catch (Exception e) {
                    throw new CLIException(SystemExceptions.FAILED_OUTPUT_FILE,
                        String.format("Failed to generate define  file. Cannot create file=%s",
                            converterInfo.getOutput().getAbsolutePath()), e);
                }
                return outputFile;
            })
            .onErrorReturn(e -> {
                throw propagate(e, new CLIException(getProcessException(e),
                    String.format("Failed define generation process. converterInfo=%s",
                        converterInfo.toString()), e));
            });
    }

    @SuppressWarnings("unchecked")
    public Observable<Void> convertDefine(final List<DefineConversionInfo> infos,
                                          final Listener<StateEvent> listener) {
        if (infos == null || infos.isEmpty()) {
            return Observable.just(null);
        }

        final DefineConversionInfo converterInfo = infos.get(0);

        return DefineUtils.generateDefine(
            toFile(converterInfo.getExcelFile()),
            new EngineCallback(EngineCallbackBuilder
                .builder()
                .setCliModel(converterInfo)) {
                @Override
                public void tell(String origin, String destination, long pos) {
                    ProcessEvent.Builder<DefineConversionInfo> builder =
                        ProcessEvent.Builder.<DefineConversionInfo>builder()
                            .setCurrent(pos)
                            .setTotal(getLength())
                            .setTarget(converterInfo)
                            .setState(ProcessState.TASK_PROCESSING)
                            .setLabel(toLabel(origin, destination));

                    listener.listen(builder.build());
                }
            },
            ProcessUtils.COMMUNITY_VERSION,
            ProcessUtils.SOURCE_SYSTEM
        )
        .map((Func1<DefineUtils.SpecStream, Void>) spec -> {
                try (InputStream inputStream = spec.getStream();
                        OutputStream outputStream = new FileOutputStream(toFile(converterInfo.getXmlFile()))) {
                    IOUtils.copy(inputStream, outputStream);
                } catch (IOException e) {
                    throw new CLIException(SystemExceptions.FAILED_OUTPUT_FILE,
                        String.format("Failed to generate define  file. Cannot create file=%s",
                            converterInfo.getXmlFile().getAbsolutePath()), e);
                }
                return null;
            })
            .onErrorReturn(e -> {
                throw propagate(e, new CLIException(getProcessException(e),
                    String.format("Failed define converter process. infos=%s",
                        Joiner.on('\n').join(infos)), e));
            });
    }

    public Observable<ValidatorResponse> validate(final List<LocalDataPackage> localPackages,
                                                  final Listener<StateEvent> listener) {
        List<Observable<ValidatorResponse>> allPackages = localPackages.stream()
            .map(localPack -> validationService.submit(localPack, listener))
            .collect(Collectors.toList());

        return Observable.zip(allPackages, results -> {
            Integer iResult = Integer.MAX_VALUE;
            ValidatorResponse response;

            for (Object result : results) {
                iResult = Math.min(iResult, ((ValidatorResponse) result).getSeverity());
            }

            if (iResult == Integer.MAX_VALUE) {
                response = new ValidatorResponse(-1, "");
            } else {
                response = new ValidatorResponse(
                    iResult, ((ValidatorResponse) results[0]).getOutputPath()
                );
            }
            LOGGER.debug("Local Validation process is complete. Sending COMPLETE message wiht result {}",
                iResult);

            return response;
        }).onErrorReturn(e -> {
            throw propagate(
                e,
                new CLIException(
                    getProcessException(e),
                    String.format(
                        "Failed local validation process. packages=%s",
                        Joiner.on('\n').join(localPackages)
                    ),
                    e
                )
            );
        });
    }

    public Observable<Integer> listOptions(final ListInfo listInfo, final Listener<StateEvent> listener) {
        return getCommunityConfigData(listInfo, !isHealthy(), listener)
            .map(response -> {
                jsonService.writeObject(response, listInfo.getOutput());
                return response == null ? 0 : 1;
            });
    }

    private Observable<CommunityConfigData> getCommunityConfigData(final InstallInfo info,
                                                                   boolean loadDir,
                                                                   final Listener<StateEvent> listener) {
        boolean isListConfig = info instanceof ListInfo;
        boolean isLoggedIn = isListConfig && ((ListInfo)info).isLoggedIn();
        boolean skipCTS3Download = isListConfig && !isLoggedIn;
        return ConfigUtils.getCommunityResources(ConfigContext.builder()
            .withLoadFromDir(loadDir)
            .withLoadCustom(isListConfig)
            .withEnginePath(Optional.ofNullable(info.getEngineFolder()).orElse(new File(JarUtils.getAppPath(getClass()), "lib")).getAbsolutePath())
            .withConfigPath(Optional.ofNullable(info.getConfig()).orElse(DataSource.EMPTY).getAbsolutePath())
            .withLimit(isListConfig ? 0 : DICTIONARIES_INSTALL_LIMIT)
            .withCTDownloadSkipped(skipCTS3Download)
            .withCallback(new EngineCallback(EngineCallbackBuilder
                .builder().setCliModel(info)) {
                @Override
                public void tell(String origin, String destination, long pos) {
                    ProcessEvent.Builder<InstallInfo> builder =
                        ProcessEvent.Builder.<InstallInfo>builder()
                            .setCurrent(pos)
                            .setTotal(getLength())
                            .setTarget(info)
                            .setState(ProcessState.TASK_PROCESSING)
                            .setLabel(toLabel(origin, destination));

                    listener.listen(builder.build());
                }
            })
            .build());
    }

    public Observable<Integer> mine(final MinerRequest request, final Listener<StateEvent> listener) {
        return MinerUtils.mine(
            MinerOptions.builder()
                .setFilePath(request.getOutput().getAbsolutePath())
                .setCsvFile(request.getCategories())
                .setQuery(request.getUrl().getAbsolutePath())
                .setReportType(request.getReportType().getValue())
                .build(),
            new EngineCallback(EngineCallbackBuilder
                .builder()
                .setCliModel(request)) {
                @Override
                public void tell(String origin, String destination, long pos) {
                    ProcessEvent.Builder<MinerRequest> builder =
                        ProcessEvent.Builder.<MinerRequest>builder()
                            .setCurrent(pos)
                            .setTotal(getLength())
                            .setTarget(request)
                            .setState(ProcessState.TASK_PROCESSING)
                            .setLabel(toLabel(origin, destination));

                    listener.listen(builder.build());
                }
            }).map(response -> response == null ? 0 : 1);
    }
}
