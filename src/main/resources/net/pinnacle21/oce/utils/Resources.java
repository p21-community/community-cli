package net.pinnacle21.oce.utils;

public final class Resources {
    public static final String BUILD_VERSION = "${project.version}";
    public static final String BUILD_DATE_TIME = "${buildNumber}";
    public static final String BUILD_DATE_TIME_FORMAT = "yyMMdd_HHmm";
}
