package net.pinnacle21.oce.test;

import java.io.File;

public final class TestUtils {
    public static final File RESOURCE_FILES_PATH = new File("src/test/resources/__files");

    private TestUtils() {
    }
}
