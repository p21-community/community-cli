package net.pinnacle21.oce.test;

import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.exceptions.CLIExceptionCode;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

public class CliErrorCodeMatcher extends BaseMatcher<CLIException> {

    private CLIExceptionCode errorCode;
    private CLIExceptionCode errorCodeGot;
    private boolean invalidException = true;

    private CliErrorCodeMatcher(CLIExceptionCode errorCode) {
        this.errorCode = errorCode;
    }

    public static Matcher<CLIException> hasError(CLIExceptionCode errorCode) {
        return new CliErrorCodeMatcher(errorCode);
    }

    @Override
    public boolean matches(Object item) {
        if (item instanceof CLIException) {
            invalidException = false;
            CLIException ex = CLIException.class.cast(item);
            this.errorCodeGot = ex.getCode();
            if (this.errorCodeGot != null) {
                return errorCode.getCode() == errorCodeGot.getCode();
            }
        }
        return false;
    }

    @Override
    public void describeTo(Description description) {
        if (this.invalidException) {
            description.appendText("CLIException with error code '" + this.errorCode + "' but got none.");
        } else {
            description.appendText("exception with error code '" + this.errorCode + "' but got '" + this.errorCodeGot + "'");
        }
    }
}
