package net.pinnacle21.oce.test;

import com.google.common.io.Files;
import net.pinnacle21.oce.model.DefineConversionInfo;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.StandardType;
import net.pinnacle21.oce.model.datapackage.DataPackage;
import net.pinnacle21.oce.model.datasource.FileDS;
import net.pinnacle21.oce.model.datasource.Schema;
import net.pinnacle21.oce.model.input.DataSourceInfo;
import net.pinnacle21.oce.model.input.Input;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public final class ModelTestUtils {
    private ModelTestUtils() {
    }

    public static DefineConversionInfo testDefineConversionInfoTest(int idx) {
        try {
            File tempFile = File.createTempFile("xml_file" + idx, ".xml");
            tempFile.deleteOnExit();
            return DefineConversionInfo.Builder.builder()
                .setXmlFile(new FileDS(tempFile, true))
                .setExcelFile(new FileDS("/path/to/excel" + idx, true))
                .setDataPackage(testDataPackage(idx))
                .build();
        } catch (IOException e) {
            throw new RuntimeException("Failed to create temp file", e);
        }
    }

    public static DataPackage testDataPackage(int idx) {
        return testDataPackage(idx, null, null);
    }

    public static DataPackage testDataPackage(int idx, String meddra, String ct) {
        File tempDir = Files.createTempDir();
        tempDir.deleteOnExit();
        File tempFile = new File(tempDir, "test_" + idx + ".data");

        return DataPackage.Builder.builder()
            .setProjectName(Input.asString("test" + idx))
            .setStudyName(Input.asString("study" + idx))
            .setGroupName(Input.asString("group" + idx))
            .setStandard(Input.asInput(StandardType.SDTMIG))
            .setStandardVersion(Input.asString("2016-03-03"))
            .setMeddraVersion(Input.asString(meddra))
            .setSdtmCtVersion(Input.asString(ct))
            .setSource(Schema.toDataSource(tempFile.getAbsolutePath(), true))
            .setReportSource(DataSourceInfo.builder()
                .setDataSource(Schema
                    .toDataSource(new File(tempDir, "report.xlsx").getAbsolutePath(), true))
                .setFormat(FormatType.EXCEL)
                .build())
            .build();
    }

    static void assertDefineConversionInfo(DefineConversionInfo info, int idx) {
        assertDataPackage(info.getDataPackage(), idx);

        assertThat("Expect XML path", info.getXmlFile().getAbsolutePath(), containsString("xml_file" + idx));
        assertEquals("Expect Excel path", info.getExcelFile().getAbsolutePath(), "/path/to/excel" + idx);
    }

    static void assertDataPackage(DataPackage dataPackage, int idx) {
        assertEquals("Expect project name test", dataPackage.getProjectName(), "test" + idx);
        assertEquals("Expect study name study", dataPackage.getStudyName(), "study" + idx);
        assertEquals("Expect group name group", dataPackage.getGroupName(), "group" + idx);
        assertEquals("Expect FILE schema", dataPackage.getSource().getSchema(), Schema.FILE);
        assertThat("Expect path", dataPackage.getSource().getAbsolutePath(), containsString("source" + idx));
    }

    static void assertDataPackages(List<DataPackage> dataPackages) {
        int idx = 0;
        for (DataPackage dataPackage : dataPackages) {
            assertDataPackage(dataPackage, idx++);
        }
    }

    static void assertDefineConversionInfos(List<DefineConversionInfo> infos) {
        int idx = 0;
        for (DefineConversionInfo info : infos) {
            assertDefineConversionInfo(info, idx++);
        }
    }
}
