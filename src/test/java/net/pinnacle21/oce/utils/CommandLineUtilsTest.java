package net.pinnacle21.oce.utils;

import net.pinnacle21.oce.exceptions.CLIException;
import net.pinnacle21.oce.model.input.CommandLine;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static net.pinnacle21.oce.model.input.ParseResults.parseArgs;
import static org.hamcrest.Matchers.containsString;

public class CommandLineUtilsTest {
    @Rule
    public final ExpectedException expectation = ExpectedException.none();

    @Test
    public void expectParameterParsingError() {
        expectation.expect(CLIException.class);
        expectation.expectMessage(containsString("unknown"));

        String[] args = {"--unknown=1"};

        parseArgs(new CommandLine(args));
    }
}