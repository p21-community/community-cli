package net.pinnacle21.oce.utils;

import net.bootstrap.core.model.ContextMessage;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ContextMessageUtilsTest {
    private static final String MESSAGE = "{\"name\": \"test\"," +
        "\"properties\": {" +
        "\"property1\": \"value1\"," +
        "\"property2\": \"value2\"" +
        "}}";

    @Test
    public void toContextMessage() throws Exception {
        ContextMessage message = ContextMessageUtils.toContextMessage(MESSAGE);
        assertThat(message.getName(), is("test"));
        assertThat(message.getProperties().get("property1"), is("value1"));
        assertThat(message.getProperties().get("property2"), is("value2"));
    }

}