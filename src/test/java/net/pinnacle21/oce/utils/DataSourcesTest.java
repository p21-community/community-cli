package net.pinnacle21.oce.utils;

import com.google.common.collect.Lists;
import com.google.common.io.Files;
import net.bootstrap.core.utils.BootstrapFileUtils;
import net.pinnacle21.oce.model.StandardType;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.model.datasource.FolderDS;
import net.pinnacle21.oce.model.datasource.Schema;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.Is.is;

public class DataSourcesTest {
    @Test
    public void toDataSourceTest() throws Exception {
        File tempFile = File.createTempFile("data", ".xpt");
        tempFile.deleteOnExit();

        DataSources.DataSourceContext cxt = DataSources.DataSourceContext.context()
            .withType(StandardType.SDTMIG)
            .withCleanup(true)
            .withUpload(false);
        DataSource ds = DataSources.toDataSource(Lists.newArrayList(tempFile.getAbsolutePath()), cxt);
        assertThat(ds.getSchema(), is(Schema.FILE));
        assertThat(ds.getAbsolutePath(), is(tempFile.getAbsolutePath()));
    }

    @Test
    public void toDataSourceCombineTest() throws Exception {
        File tempFile = File.createTempFile("data", ".temp");
        File testFile = File.createTempFile("test", ".test");

        tempFile.deleteOnExit();
        testFile.deleteOnExit();
        DataSources.DataSourceContext cxt = DataSources.DataSourceContext.context()
            .withType(StandardType.SDTMIG)
            .withCleanup(true)
            .withUpload(false)
            .withCombine(true);
        DataSource ds = DataSources.toDataSource(Lists.newArrayList(tempFile.getAbsolutePath(),
            testFile.getAbsolutePath()), cxt);

        assertThat(ds.getSchema(), is(Schema.FILE));
        FolderDS fileDS = (FolderDS) ds;
        assertThat(fileDS.isValid(), is(true));
        File[] files = fileDS.getFile().listFiles();
        for (File f : files) {
            assertThat(Files.getFileExtension(f.getName()), anyOf(is("temp"), is("test")));
        }
    }

    @Test
    public void toDataSourceCombineDirsTest() throws Exception {
        File tempFile = File.createTempFile("data", ".temp");
        File testDir = new File(tempFile.getParent(), "myDir");
        BootstrapFileUtils.forceMkdir(testDir);
        File testFile = new File(testDir, "test.test");
        FileUtils.touch(testFile);

        testDir.deleteOnExit();
        tempFile.deleteOnExit();
        testFile.deleteOnExit();

        DataSources.DataSourceContext cxt = DataSources.DataSourceContext.context()
            .withType(StandardType.SDTMIG)
            .withCleanup(true)
            .withUpload(false)
            .withCombine(true);

        DataSource ds = DataSources.toDataSource(Lists.newArrayList(tempFile.getAbsolutePath(),
            testDir.getAbsolutePath()), cxt);

        assertThat(ds.getSchema(), is(Schema.FILE));
        FolderDS fileDS = (FolderDS) ds;
        assertThat(fileDS.isValid(), is(true));
        String[] items = fileDS.getFile().list();
        for (String item : items) {
            assertThat(item, anyOf(containsString("data"), containsString("test")));
        }
    }
}