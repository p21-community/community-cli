package net.pinnacle21.oce.utils;

import net.bootstrap.core.model.Request;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class TestPointcuts {
    @Pointcut("execution(* net.bootstrap.api.iq.filters.OSFilter.qualify(..)) && args(request)")
    public void osQualify(Request request) {
    }
}
