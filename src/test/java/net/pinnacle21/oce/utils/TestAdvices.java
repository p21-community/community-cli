package net.pinnacle21.oce.utils;

import net.bootstrap.core.model.Request;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class TestAdvices {
    @Around("net.pinnacle21.oce.utils.TestPointcuts.osQualify(request)")
    public boolean osqualifyAdvice(final ProceedingJoinPoint thisJoinPoint,
                                   Request request) {
        try {
            thisJoinPoint.proceed(new Object[]{request});
            return true;
        } catch (Throwable throwable) {
            throw new RuntimeException("Failed to call OS IQ check", throwable);
        }
    }
}
