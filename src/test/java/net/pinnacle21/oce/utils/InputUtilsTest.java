package net.pinnacle21.oce.utils;

import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.input.CliParameters;
import net.pinnacle21.oce.model.input.CommunityInput;
import net.pinnacle21.oce.model.request.ConverterRequest;
import net.pinnacle21.oce.model.request.Request;
import net.pinnacle21.oce.model.request.RequestType;
import net.pinnacle21.oce.test.TestUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.FileInputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;

public class InputUtilsTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void converterCommunity() throws Exception {
        CliParameters converter = new CommunityInput(IOUtils
            .toString(new FileInputStream(new File(TestUtils.RESOURCE_FILES_PATH, "input/converter.json"))));

        Request request = InputUtils.from(converter)
            .toBlocking().single();
        assertThat(request, notNullValue());
        assertThat(request.getType(), is(RequestType.CONVERTER));
    }

    @Test
    public void converterCommunityXml() throws Exception {
        CliParameters converter = new CommunityInput(IOUtils
            .toString(new FileInputStream(new File(TestUtils.RESOURCE_FILES_PATH, "input/converterDatasetXml.json"))));

        Request request = InputUtils.from(converter)
            .toBlocking().single();
        assertThat(request, notNullValue());
        assertThat(request.getType(), is(RequestType.CONVERTER));

        ConverterRequest converterRequest = (ConverterRequest) request;
        assertThat(converterRequest.getInfo().getOutputFormatType(), is(FormatType.DATASET_XML));
    }
}