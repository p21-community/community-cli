package net.pinnacle21.oce.model.input;

import net.pinnacle21.oce.model.StandardType;
import net.pinnacle21.oce.model.datasource.DataSource;
import net.pinnacle21.oce.utils.DataSources;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ConfigInfoTest {

    @Test
    public void checkConfigName() {
        DataSource ds = DataSources.newFileOrFolder(new File("sdtm 3.1.1 (FDA).xml"));
        ConfigInfo info = ConfigInfo.Builder.builder().setConfig(ds).build();

        assertThat(info.getStandardType().getValue(), is(StandardType.SDTMIG.getValue()));
        assertThat(info.getVersion(), is("3.1.1"));

        info = ConfigInfo.Builder.builder()
            .setConfig(DataSources.newFileOrFolder(new File("sdtm 3.1.1.xml")))
            .build();

        assertThat(info.getStandardType().getValue(), is(StandardType.SDTMIG.getValue()));
        assertThat(info.getVersion(), is("3.1.1"));

        info = ConfigInfo.Builder.builder()
            .setConfig(DataSources.newFileOrFolder(new File("adam 1.0 (pmda).xml")))
            .build();

        assertThat(info.getStandardType().getValue(), is(StandardType.ADaMIG.getValue()));
        assertThat(info.getVersion(), is("1.0"));

        info = ConfigInfo.Builder.builder()
            .setConfig(DataSources.newFileOrFolder(new File("Define.xml 2.0.xml")))
            .build();

        assertThat(info.getStandardType().getValue(), is(StandardType.DEFINE.getValue()));
        assertThat(info.getVersion(), is("2.0"));

        info = ConfigInfo.Builder.builder()
            .setConfig(DataSources.newFileOrFolder(new File("sdtm md 3.2 (fda).xml")))
            .build();

        assertThat(info.getStandardType().getValue(), is(StandardType.SDTMIG_MD.getValue()));
        assertThat(info.getVersion(), is("3.2"));


        info = ConfigInfo.Builder.builder()
            .setConfig(DataSources.newFileOrFolder(new File("config-r9-v35.xml")))
            .build();

        assertThat(info.getStandardType().getValue(), is("config-r9-v35"));
        assertThat(info.getVersion(), is("unknown"));

        info = ConfigInfo.Builder.builder()
            .setConfig(DataSources.newFileOrFolder(new File("sdtm md-(fda).xml")))
            .build();

        assertThat(info.getStandardType().getValue(), is(StandardType.SDTMIG_MD.getValue()));
        assertThat(info.getVersion(), is("unknown"));

        info = ConfigInfo.Builder.builder()
            .setConfig(DataSources.newFileOrFolder(new File("Custom 1.0 (fda).xml")))
            .build();

        assertThat(info.getStandardType().getValue(), is("Custom 1.0 (fda)"));
        assertThat(info.getVersion(), is("unknown"));

        info = ConfigInfo.Builder.builder()
            .setConfig(DataSources.newFileOrFolder(new File("Define.xml.xml")))
            .build();

        assertThat(info.getStandardType().getValue(), is(StandardType.DEFINE.getValue()));
        assertThat(info.getVersion(), is("unknown"));

        info = ConfigInfo.Builder.builder()
            .setConfig(DataSources.newFileOrFolder(new File("Define.xml (PMDA).xml")))
            .build();

        assertThat(info.getStandardType().getValue(), is(StandardType.DEFINE.getValue()));
        assertThat(info.getVersion(), is("unknown"));

        StandardType.Standard standard = StandardType.fromString("sdtm 3.2");
        assertThat(standard.getType(), is(StandardType.SDTMIG));
        assertThat(standard.getVersion(), is("3.2"));
    }

}