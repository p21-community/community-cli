package net.pinnacle21.oce.model.input;

import net.pinnacle21.oce.model.ReportType;
import net.pinnacle21.oce.test.TestUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class CommunityInputTest {
    @Test
    public void converterRequestTest() throws Exception {
        String json = IOUtils.toString(new FileInputStream(new File(TestUtils.RESOURCE_FILES_PATH, "input/converter.json")));

        CliParameters params = new CommunityInput(json);
    }

    @Test
    public void minerRequestTest() throws Exception {
        String json = IOUtils.toString(new FileInputStream(new File(TestUtils.RESOURCE_FILES_PATH, "input/minerSites.json")));

        CliParameters params = new CommunityInput(json);
        Input<List<String>> sources = params.getSources();
        assertThat(sources, notNullValue());
        assertThat(sources.get().get(0), containsString("clinicaltrials.gov"));

        Input<ReportType> reportType = params.getReportType();
        assertThat(reportType.get(), is(ReportType.OUTCOMES));

        Input<String> output = params.getOutput();
        assertThat(output.get(), containsString("reports/folder"));
    }
}