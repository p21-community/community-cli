package net.pinnacle21.oce.model;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ReleaseLevelTest {

    @Test
    public void fromStringTest() {
        ReleaseLevel level = ReleaseLevel.fromValue("3.0.7");
        assertThat(level, is(ReleaseLevel.RELEASE));

        level = ReleaseLevel.fromValue("3.0");
        assertThat(level, is(ReleaseLevel.RELEASE));

        level = ReleaseLevel.fromValue("3.0.7-PRE-RELEASE");
        assertThat(level, is(ReleaseLevel.PRE_RELEASE));

        level = ReleaseLevel.fromValue("3.0.7-SNAPSHOT");
        assertThat(level, is(ReleaseLevel.SNAPSHOT));

        level = ReleaseLevel.fromValue("xxx");
        assertThat(level, is(ReleaseLevel.SNAPSHOT));

        level = ReleaseLevel.fromValue("1.0.8-ddfr");
        assertThat(level, is(ReleaseLevel.SNAPSHOT));

        level = ReleaseLevel.fromValue("1.0.8-");
        assertThat(level, is(ReleaseLevel.SNAPSHOT));

        level = ReleaseLevel.fromValue("1-");
        assertThat(level, is(ReleaseLevel.SNAPSHOT));

        level = ReleaseLevel.fromValue("1.-");
        assertThat(level, is(ReleaseLevel.SNAPSHOT));
    }

}