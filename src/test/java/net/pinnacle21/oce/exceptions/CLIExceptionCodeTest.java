package net.pinnacle21.oce.exceptions;

import com.google.common.base.Splitter;
import org.junit.Test;
import org.reflections.Reflections;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.lang.Math.max;
import static net.pinnacle21.oce.utils.CliErrorUtils.toCode;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class CLIExceptionCodeTest {
    private static int parseErrorRef(String ref) {
        List<String> parts = Splitter.on('.').splitToList(ref);
        assertThat("code " + ref + " must have 3 parts separated by '.'",
            parts.size(), is(3));
        return toCode(Integer.valueOf(parts.get(1) + parts.get(2)).byteValue());
    }

    @Test
    public void validateErrorCodes() {
        Reflections reflect = new Reflections("net.pinnacle21.oce");

        Set<Class<? extends CLIExceptionCode>> subTypes = reflect.getSubTypesOf(CLIExceptionCode.class);
        Set<String> errorCodes = new HashSet<>();
        Set<String> duplicsates = new HashSet<>();

        for (Class<? extends CLIExceptionCode> errCodeCls : subTypes) {
            CLIExceptionCode[] values = errCodeCls.getEnumConstants();
            if (values != null) {
                for (CLIExceptionCode code : values) {
                    if (errorCodes.contains(code.getRef())) {
                        duplicsates.add(code.getRef());
                    } else {
                        errorCodes.add(code.getRef());
                    }
                    int clientCode = parseErrorRef(code.getRef());
                    assertThat(clientCode, is(toCode(code.getCode())));
                }
            }
        }

        for (String dupCode : duplicsates) {
            System.out.println("Duplicate error code: " + dupCode);
        }

        assertThat(duplicsates.isEmpty(), is(true));

        int nextNum = 0;
        for (String errorCode : errorCodes) {
            int clientCode = parseErrorRef(errorCode);
            nextNum = max(nextNum, clientCode);
        }

        nextNum++;
        System.out.println("Next client error code is " + nextNum);
    }
}