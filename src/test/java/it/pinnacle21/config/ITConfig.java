package it.pinnacle21.config;

import com.google.common.collect.ImmutableList;
import it.pinnacle21.IntegrationTests;
import net.bootstrap.core.proxy.MethodDesriptor;
import net.bootstrap.core.proxy.ObjectBuilder;
import net.bootstrap.core.proxy.ProxyCallerInterface;
import net.bootstrap.core.proxy.ProxyUtils;
import net.bootstrap.core.proxy.ProxyVersionedInterface;
import net.bootstrap.core.utils.jar.ArtifactInterface;
import net.bootstrap.core.utils.jar.ArtifactLoader;
import net.bootstrap.core.utils.jar.LibArtifact;
import org.apache.commons.lang3.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import static it.pinnacle21.utils.TestUtils.*;
import static net.bootstrap.core.utils.jar.URLClassLoaderUtils.DIR_LOADER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.startsWith;

@Category(IntegrationTests.class)
public class ITConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(ITConfig.class);

    private static String getResponse(BufferedReader stdInput) throws IOException {
        String s;
        boolean response = false;
        while ((s = stdInput.readLine()) != null) {
            if (response || LOGGER.isDebugEnabled()) {
                System.out.println(s);
                if (response) {
                    break;
                }
            }
            if (s.equalsIgnoreCase("=cli:")) {
                response = true;
            }
        }

        return s;
    }

    @Test
    public void getListOfProjects() throws IOException {
        String[] cliCommand = {"java", "-jar", getJar(),
            "--list.config=true",
            "--api.key=" + getApiKey(),
            "--web.host=" + getWebHost()};
        Process proc = Runtime.getRuntime().exec(cliCommand);

        BufferedReader stdInput = new BufferedReader(new
            InputStreamReader(proc.getInputStream()));

        String s = getResponse(stdInput);

        assertThat(s, notNullValue());
        assertThat(s, startsWith("[{"));
    }

    @Test
    public void getListOfStudies() throws IOException {
        String[] cliCommand = {"java", "-jar", getJar(),
            "--list.config=true",
            "--project=av340 project",
            "--api.key=" + getApiKey(),
            "--web.host=" + getWebHost()};
        Process proc = Runtime.getRuntime().exec(cliCommand);

        BufferedReader stdInput = new BufferedReader(new
            InputStreamReader(proc.getInputStream()));

        String s = getResponse(stdInput);

        assertThat(s, notNullValue());
        assertThat(s, startsWith("[{"));
    }

    @Test
    public void getListOfPackages() throws IOException {
        String[] cliCommand = {"java", "-jar", getJar(),
            "--list.config=true",
            "--project=av340 project",
            "--study=v340 study 1",
            "--api.key=" + getApiKey(),
            "--web.host=" + getWebHost()};
        Process proc = Runtime.getRuntime().exec(cliCommand);

        BufferedReader stdInput = new BufferedReader(new
            InputStreamReader(proc.getInputStream()));

        String s = getResponse(stdInput);

        assertThat(s, notNullValue());
        assertThat(s, startsWith("[{"));
    }

    @Test
    public void getPackage() throws IOException {
        String[] cliCommand = {"java", "-jar", getJar(),
            "--list.config=true",
            "--project=av340 project",
            "--study=v340 study 1",
            "--datapackage=v340 s1 dp1",
            "--api.key=" + getApiKey(),
            "--web.host=" + getWebHost()};
        Process proc = Runtime.getRuntime().exec(cliCommand);

        BufferedReader stdInput = new BufferedReader(new
            InputStreamReader(proc.getInputStream()));

        String s = getResponse(stdInput);

        assertThat(s, notNullValue());
        assertThat(s, startsWith("[{"));
    }

    @Test
    @Ignore
    public void communityListConfig() throws IOException, InterruptedException {
        List<ArtifactInterface> artifacts = ImmutableList.of(LibArtifact.builder()
                .withSourceType(ArtifactInterface.SourceType.DIR)
                .withRoot("target")
                .build());

        ObjectBuilder builder = ObjectBuilder
            .builder()
            .setFallbackOrder(artifacts)
            .setStaticObject(true)
            .setClassName("net.pinnacle21.oce.utils.ProcessUtils");

        ProxyCallerInterface caller = builder.build(ImmutableList.of(DIR_LOADER));
        final CountDownLatch latch = new CountDownLatch(2);

        ProxyCallerInterface listener = ObjectBuilder.from(builder)
            .setInterfaceName("net.pinnacle21.oce.events.Listener")
            .setHandler(new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    if (ProxyUtils.isMethod(method, "listen")) {
                        LOGGER.info("report: ", args[0]);
                        latch.countDown();
                    }
                    return null;
                }
            }).build(ImmutableList.of(DIR_LOADER));

        caller.call(MethodDesriptor.builder()
            .setStatic(true)
            .setSignature(String.class, listener.myClass())
            .setParams("{\n" +
                "\"action\":\"LIST_CONFIG\",\n" +
                "\"target\":\"/Users/aleksey/MyFiles/p21-client/config.json\",\n" +
                "\"config\":\"/Users/aleksey/MyFiles/p21-client/config\"\n" +
                "}", listener)
            .setName("startFromJson")
            .build());
        latch.await();
    }
}
