package it.pinnacle21.utils;

import org.apache.commons.io.FilenameUtils;

import java.io.File;

public final class TestUtils {
    private TestUtils() {
    }

    public static String getJar() {
        return FilenameUtils.normalize(new File(TestUtils.class.getResource("../../").getFile() +
            ".." + File.separator + ".." + File.separator + "p21-client-0-LOCAL-SNAPSHOT.jar")
            .getAbsolutePath());
    }

    public static String getApiKey() {
        return "tUKyiTQq4fEYCH2KkyjxG8KiqlAmvnG399HdFWe6o3ZKMkoEaI1otADvQx3iuK7RgWa0XpQmOXitCaPTAA58hPABiAN3m0KTnjpQ";
    }

    public static String getWebHost() {
        return "https://qa-dev.pinnacle21.net";
    }
}
