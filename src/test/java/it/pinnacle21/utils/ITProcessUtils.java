package it.pinnacle21.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import it.pinnacle21.IntegrationTests;
import net.bootstrap.api.model.data.CommunityConfigData;
import net.pinnacle21.oce.model.FormatType;
import net.pinnacle21.oce.model.community.Action;
import net.pinnacle21.oce.model.community.CommunityListConfig;
import net.pinnacle21.oce.utils.ProcessUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@Category(IntegrationTests.class)
public class ITProcessUtils {
    private static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Ignore
    @Test
    public void communityList() throws Exception {
        File tempDir = Files.createTempDir();
        File resultFile = new File(tempDir, "config.json");
        tempDir.deleteOnExit();
        resultFile.deleteOnExit();

        CommunityListConfig list = new CommunityListConfig();
        list.setAction(Action.LIST_CONFIG);
        list.setTarget(resultFile.getAbsolutePath());
        list.setOutputFormat(FormatType.JSON);

        final CountDownLatch latch = new CountDownLatch(2); // coundown from 3 to 0

        ProcessUtils.startFromJson(OBJECT_MAPPER.writeValueAsString(list));

        try {
            latch.await(10, TimeUnit.SECONDS);  // wait until latch counted down to 0
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertThat(resultFile.exists(), is(true));

        CommunityConfigData data = OBJECT_MAPPER.readValue(resultFile, CommunityConfigData.class);
        assertThat(data, notNullValue());
        assertThat(data.getEngines(), notNullValue());
    }
}
